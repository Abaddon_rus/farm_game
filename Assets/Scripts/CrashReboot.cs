﻿using UnityEngine;
using UnityEngine.EventSystems;
using Game;
using UnityEngine.Rendering;
using System;

public class CrashReboot : MonoBehaviour, IPointerClickHandler
{
    [SerializeField]
    SlotFarm _farm;
    [SerializeField]
    SortingGroup _sortingGroup;
    [SerializeField]
    SpriteRenderer _spriteRendererThis;

    public void OnPointerClick(PointerEventData eventData)
    {
        NetworkCore.Send("Reboot", new DataPacket()
        {
            v1 = _farm.ID
        }, (data) =>
        {
            if (Convert.ToBoolean(data))
            {
                _farm.CRASH = false;
                gameObject.SetActive(false);
            }
        });
    }

    private void Start()
    {
        _spriteRendererThis.sortingOrder = _sortingGroup.sortingOrder + 3;
    }
}
