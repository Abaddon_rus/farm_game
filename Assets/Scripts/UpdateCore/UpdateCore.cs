﻿using Game.CoreSupport;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class UpdateCore
    {
        #region Constructor

        public UpdateCore()
        {
            _updatablesSet = new HashSet<IUpdatable>();

            var obj = new GameObject("UpdateCore");
            _context = obj.AddComponent<UpdateCoreContext>();
            _context.OnUpdateEvent += OnUpdate;
            Object.DontDestroyOnLoad(_context);
        }

        #endregion Constructor
        #region Fields

        UpdateCoreContext _context;

        HashSet<IUpdatable> _updatablesSet;

        #endregion Fields
        #region Methods

        public void Add(IUpdatable updatable)
        {
            _updatablesSet.Add(updatable);
        }

        public void Remove(IUpdatable updatable)
        {
            _updatablesSet.Remove(updatable);
        }

        void OnUpdate()
        {
            using (var tempSet = CashSet<IUpdatable>.Get())
            {
                var deltaTime = Time.deltaTime;
                tempSet.UnionWith(_updatablesSet);
                tempSet.ForEach(x => x.OnUpdate(deltaTime));
            }
        }

        #endregion Methods
    }
}