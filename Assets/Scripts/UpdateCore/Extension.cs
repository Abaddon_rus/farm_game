﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class Extension
{

    public static T Find<T>(this ICollection<T> source, Predicate<T> predicate)
    {
        T result = default(T);
        foreach (var item in source)
        {
            if (predicate(item))
            {
                result = item;
                break;
            }
        }

        return result;
    }

    public static int FindAll<T, T0>(this ICollection<T> source, Predicate<T> predicate, ref ICollection<T0> result) where T0 : T
    {
        foreach (var item in source)
        {
            if (predicate(item))
            {
                var resultItem = (T0)item;
                result.Add(resultItem);
            }
        }

        return result.Count;
    }

    public static int FindAll<T>(this ICollection<T> source, Predicate<T> predicate, ICollection<T> result)
    {
        foreach (var item in source)
        {
            if (predicate(item))
            {
                result.Add(item);
            }
        }

        return result.Count;
    }

    public static void ForEach<T>(this ICollection<T> source, Action<T> action)
    {
        foreach (var item in source)
        {
            action(item);
        }
    }

    public static void FindAndRemove<T>(this IList<T> source, Predicate<T> predicate)
    {
        int count = source.Count;
        for (int i = 0; i < count; i++)
        {
            var item = source[i];
            if (predicate(item))
            {
                source.RemoveAt(i);
                return;
            }
        }
    }

    public static void FindAllAndRemove<T>(this IList<T> source, Predicate<T> predicate)
    {
        int count = source.Count;
        for (int i = 0; i < count; i++)
        {
            var item = source[i];
            if (predicate(item))
            {
                source.RemoveAt(i);
            }
        }
    }

    public static void SetLayer(this GameObject source, int layer, bool withChilds)
    {
        source.layer = layer;
        var count = source.transform.childCount;

        if (withChilds)
        {
            for (int i = 0; i < count; i++)
            {
                var transform = source.transform.GetChild(i);
                transform.gameObject.layer = layer;
                transform.gameObject.SetLayer(layer, true);
            }
        }
    }

    public static string ToTime(this int i)
    {
        return (i / 3600).ToString("D2") + ":" + ((i % 3600) / 60).ToString("D2") + ":" + (i % 60).ToString("D2");
    }

    public static string UnixToDateAndTime(this int i)
    {
        return new DateTime(1970, 1, 1, 0, 0, 0).AddSeconds(i).ToLocalTime().ToString();
    }

    public static string UnixToDate(this int i) 
    {
        var dt = new DateTime(1970, 1, 1, 0, 0, 0).AddSeconds(i).ToLocalTime();
        return string.Format($"{dt.Day}.{dt.Month}.{dt.Year}");
    }
}
