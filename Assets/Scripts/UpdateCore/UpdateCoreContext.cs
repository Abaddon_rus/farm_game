﻿using System;
using UnityEngine;

namespace Game.CoreSupport
{
    public class UpdateCoreContext : MonoBehaviour
    {
        #region Event

        public event Action OnUpdateEvent = delegate { };

        #endregion Event
        #region Methods

        private void Update()
        {
            OnUpdateEvent.Invoke();
        }

        #endregion Methods
    }
}