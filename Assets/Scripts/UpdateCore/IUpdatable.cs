﻿namespace Game
{
    public interface IUpdatable
    {
        void OnUpdate(float deltaTime); 
    }
}