﻿using System;
using System.Collections.Generic;

public class CashSet<T> : HashSet<T>, IDisposable
{
    #region Fields

    static Stack<CashSet<T>> _pool = new Stack<CashSet<T>>();

    #endregion Fields
    #region Methods

    public static CashSet<T> Get() => _pool.Count > 0 ? _pool.Pop() : new CashSet<T>();

    static void Return(CashSet<T> list)
    {
        list.Clear();
        _pool.Push(list);
    }
    
    void IDisposable.Dispose()
    {
        Return(this);
    }

    #endregion Methods
}

public class CashList<T> : List<T>, IDisposable
{
    #region Fields

    static Stack<CashList<T>> _pool = new Stack<CashList<T>>();

    #endregion Fields
    #region Methods

    public static CashList<T> Get() => _pool.Count > 0 ? _pool.Pop() : new CashList<T>();

    static void Return(CashList<T> list)
    {
        list.Clear();
        _pool.Push(list);
    }

    public void Dispose()
    {
        Return(this);
    }

    #endregion Methods
}
