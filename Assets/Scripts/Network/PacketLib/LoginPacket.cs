﻿using System;
[Serializable]
public class LoginPacket : Packet
{ 
    public string username;
    public string password;
}
