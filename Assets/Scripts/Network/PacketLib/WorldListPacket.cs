﻿using System;
using System.Collections.Generic;

namespace PacketLib
{
    [Serializable]
    public class WorldListPacket : Packet
    {
        public Dictionary<int, string> WorldList;
    }
}

