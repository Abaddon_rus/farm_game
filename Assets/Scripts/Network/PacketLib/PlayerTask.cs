﻿using System;

[Serializable]
public class PlayerTask : Packet
{
    public int id;
    public string Name;
    public float Price;
    public int Timer;
    public int TimeStart;
    public float Chance;
    public int TimeRelax;
    public int Lvl;
    public bool Rare = false;
}
