﻿using System;

namespace PacketLib
{
    [Serializable]
    public class ComponentPlayerPacket : Packet
    {
        public CardData[] CardData;
        public CoolerData[] CoolerData;
        public HardData[] HardData;
    }

    [Serializable]
    public class CardData
    {
        public int id;
        public int ComponentID;
        public float PriceRepair;
        public int Strenght;
        public int Progress;
        public int PlayerID;
        public float Income;
        public float Temperature;
        public int NumberSlot;
        public int TimeEndRepair;
        public int CountRepair;
    }

    [Serializable]
    public class CoolerData
    {
        public int id;
        public int ComponentID;
        public float PriceRepair;
        public int Strenght;
        public int Progress;
        public int PlayerID;
        public int NumberSlot;
        public int TimeEndRepair;
        public int CountRepair;
    }

    [Serializable]
    public class HardData
    {
        public int id;
        public int ComponentID;
        public float PriceRepair;
        public int Strenght;
        public int Progress;
        public int PlayerID;
        public float Coins;
        public int NumberSlot;
        public int TimeEndRepair;
        public int CountRepair;
    }
}
