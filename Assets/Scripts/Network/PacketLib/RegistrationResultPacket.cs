﻿using System;

namespace PacketLib
{
    [Serializable]
    public class RegistrationResultPacket : Packet
    {
        public bool Result;
        public string Message;
    }
}

