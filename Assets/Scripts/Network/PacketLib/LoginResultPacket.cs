﻿using System;

[Serializable]
public class LoginResultPacket
{
    public bool result;
    public string authKey;
}
