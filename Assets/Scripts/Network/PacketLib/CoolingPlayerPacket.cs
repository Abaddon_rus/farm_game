﻿using System;

namespace PacketLib
{
    [Serializable]
    public class CoolingPlayerPacket : Packet
    {
        public CoolingData[] CoolingData;
    }

    [Serializable]
    public class CoolingData
    {
        public int ID;
        public int PlayerID;
        public int CoolingID;
        public float PriceRepair;
        public int Strenght;
        public int Progress;
        public int NumberSlot;
        public bool Stop;
        public int Level;
        public int TimeEndRepair;
        public int CountRepair;
    }
}
