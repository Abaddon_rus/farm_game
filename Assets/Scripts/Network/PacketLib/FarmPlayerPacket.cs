﻿using System;

namespace PacketLib
{
    [Serializable]
    public class FarmPlayerPacket : Packet
    {
        public FarmData[] FarmData;
    }

    [Serializable]
    public class FarmData
    {
        public int id;
        public int PlayerID;
        public int FarmId;
        public float PriceRepair;
        public int Strenght;
        public int Progress;
        public int PosX;
        public int PosY;
        public string Time;
        public bool Stop;
        public int CountSlotCard;
        public int CountSlotCooler;
        public int CountSlotHard;
        public int FarmLvl;
        public int TimeEndRepair;
        public int CountRepair;
        public ComponentPlayerPacket Components;
        public bool Crash;
    }
}
