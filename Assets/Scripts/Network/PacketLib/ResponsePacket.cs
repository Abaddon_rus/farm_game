﻿using System;

namespace PacketLib
{
    [Serializable]
    public class ResponsePacket : Packet
    {
        public string Type;
        public string Code;
        public string Data;
    }
}

