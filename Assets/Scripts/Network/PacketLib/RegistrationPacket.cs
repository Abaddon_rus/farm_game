﻿using System;

namespace PacketLib
{
    [Serializable]
    public class RegistrationPacket : Packet
    {
        public string username;
        public string password;
    }
}

