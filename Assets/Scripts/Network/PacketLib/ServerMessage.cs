﻿using System;

namespace PacketLib
{
    [Serializable]
    public class ServerMessage : Packet
    {
        public int CodeResult;
        public string Message;
    }
}

