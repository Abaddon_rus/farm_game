﻿using System;


[Serializable]
public class BuyFarmPacket : Packet
{
    public int x;
    public int y;
}
