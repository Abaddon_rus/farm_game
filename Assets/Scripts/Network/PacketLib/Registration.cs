﻿using PacketLib;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class Registration : MonoBehaviour
    {

        [SerializeField]
        InputField _login;
        [SerializeField]
        InputField _password;
        [SerializeField]
        Button _buttonOk;
        [SerializeField]
        Text _eror;


        private void OnEnable()
        {
            _buttonOk.onClick.AddListener(SendReg);
        }

        private void OnDisable()
        {
            _buttonOk.onClick.RemoveListener(SendReg);
        }

        private void SendReg()
        {
            NetworkCore.Send("registration", new RegistrationPacket
            {
                username = _login.text,
                password = _password.text
            }, (data) => {
                ClosePanel(Convert.ToBoolean(data));
            });
        }

        void ClosePanel(bool result)
        {
            if (result)
            {
                gameObject.SetActive(false);
            }
            else
            {
                _eror.text = "Имя пользователя занято.";
            }
        }
    }
}