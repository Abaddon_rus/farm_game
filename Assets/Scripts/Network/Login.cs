﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class Login : MonoBehaviour
    {
        [SerializeField]
        InputField _login;
        [SerializeField]
        InputField _password;
        [SerializeField]
        Button _buttonOk;
        [SerializeField]
        Button _buttonReg;
        [SerializeField]
        Text _eror;

        [SerializeField]
        Registration _registration;

        bool _loginBool = false;

        private void Update()
        {
            if (_loginBool)
            {
                gameObject.SetActive(false);
            }
        }

        private void Awake()
        {
            transform.position = new Vector3();
        }

        private void OnEnable()
        {
            _buttonOk.onClick.AddListener(SendLogin);
            _buttonReg.onClick.AddListener(OpenRegPanel);
        }

        private void OnDisable()
        {
            _buttonOk.onClick.RemoveListener(SendLogin);
            _buttonReg.onClick.RemoveListener(OpenRegPanel);
        }

        public void SendLogin()
        {
            NetworkCore.Send("login", new LoginPacket{
                username = _login.text,
                password = _password.text
            }, (data) => {
                LoginResultPacket loginResultPacket = JsonUtility.FromJson<LoginResultPacket>(data);
                NetworkCore.Auth(loginResultPacket);
                ClosePanel(loginResultPacket.result);
            });
        }

        IEnumerator SendCoroutine()
        {
            yield return null;
        }

        void ClosePanel(bool result) 
        {
            if (result)
            {
                gameObject.SetActive(false);
                StaticStorage.GetLink<LoadManager>("LoadManager").Load();
            }
            else
            {
                _eror.text = "Не правильный логин или пароль!";
            }
        }

        private void OpenRegPanel()
        {
            _registration.gameObject.SetActive(true);
        }
    }
}