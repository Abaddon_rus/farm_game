﻿using UnityEngine;
using System.Text;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using PacketLib;
public class NetworkCore : MonoBehaviour
{
    [SerializeField]
    GameObject _loading;

    private static readonly string serverAddress = "http://178.128.252.156:1337/";
    private static string authKey = "";

    public static NetworkCore _networkCore;

    private void Awake()
    {
        _networkCore = this;
    }

    public static void Send<T>(string serverFunc, T values, Action<string> handlerData) where T : Packet
    {
        _networkCore.StartCoroutine(_networkCore._SendCoroutine<T>(serverAddress + serverFunc, values, handlerData));
    }

    private IEnumerator _SendCoroutine<T>(string uri, T values, Action<string> handlerData) where T : Packet
    {
        _loading.SetActive(true);
        values.AuthKey = authKey;
        string json = ToJSON(values);
        List<IMultipartFormSection> formData = new List<IMultipartFormSection>
        {
            new MultipartFormDataSection(json)
        };
        UnityWebRequest www = UnityWebRequest.Post(uri, formData);
        UnityWebRequestAsyncOperation unityWebRequestAsyncOperation = www.SendWebRequest();
        yield return new WaitUntil(() => unityWebRequestAsyncOperation.isDone);

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            _loading.SetActive(false);
        }
        else
        {
            byte[] data = www.downloadHandler.data;
            ResponsePacket responsePacket = JsonUtility.FromJson<ResponsePacket>(Encoding.Default.GetString(data));
            //Debug.Log(Encoding.Default.GetString(data));
            if (responsePacket.Type != "Error") {
                handlerData(responsePacket.Data);
            } else {
                ShowError(responsePacket.Code, responsePacket.Data);
            }
            _loading.SetActive(false);
        }
    }
    
    private static void ShowError(string code, string message)
    {
        Debug.LogError(code + ": " + message);
        //something
    }

    private static string ToJSON<T>(T objectToSend)
    {
        return JsonUtility.ToJson(objectToSend);
    }

    internal static void Auth(LoginResultPacket loginResultPacket)
    {
        authKey = loginResultPacket.authKey;
    }
}