﻿using LevelEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Game
{
    public class CoolingPlant : MonoBehaviour, IPointerClickHandler
    {
        public SlotCooling _coolingSlot;

        public int _number;

        [SerializeField]
        bool _free = true;

        GamePlay _gamePlay;
        EditorMapRoot _editorMapRoot;
        BuyCooling _buyCooling;

        private void Start()
        {
            Links();
        }

        void Links()
        {
            _buyCooling = StaticStorage.GetLink<BuyCooling>("BuyCooling");
            _gamePlay = StaticStorage.GetLink<GamePlay>("GamePlay");
            _editorMapRoot = StaticStorage.GetLink<EditorMapRoot>("EditorMapRoot");
            _editorMapRoot._coolingPlant.Add(this);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            _gamePlay.ClosePanels();
            if (_free && Input.GetMouseButtonUp(0))
            {
                if (PlantingComponents.Cooling == null)
                {
                    _buyCooling.Show(this);
                }
                else
                {
                    Planting(PlantingComponents.Cooling);
                    StaticStorage.GetLink<Inventory>("Inventory").Remove(PlantingComponents.Cooling);
                    Destroy(PlantingComponents.DragItem);
                    PlantingComponents.Clear();
                }
            }
        }

        public void Free(bool free)
        {
            _free = free;
        }

        public void Planting(Cooling cooling)
        {
            _free = false;
            _coolingSlot.gameObject.AddComponent<PolygonCollider2D>();
            _coolingSlot.ChangePlant(this);
            _coolingSlot.ID = cooling.ID;
            _coolingSlot.Cooling = cooling;
            _coolingSlot.Cooling.NumberSlot = _number;
            _coolingSlot.IsStoped = cooling.Stop;
        }
    }
}