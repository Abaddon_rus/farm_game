﻿using PacketLib;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Game
{
    public class Plant : MonoBehaviour, IPointerClickHandler /*IDropHandler*/
    {
        [SerializeField]
        bool _free = true;

        BuyFarm _buyFarm;

        public SlotFarm _slotFarm;

        public Vector2Int _positionXY;

        GamePlay _gamePlay;

        private void Start()
        {
            _gamePlay = StaticStorage.GetLink<GamePlay>("GamePlay");
            _buyFarm = StaticStorage.GetLink<BuyFarm>("BuyFarm");
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            _gamePlay.ClosePanels();
            if (_free && Input.GetMouseButtonUp(0))
            {
                _buyFarm.Show(this);
                //if (PlantingComponents.Farm == null)
                //{

                //}
                //else
                //{
                //    Planting(PlantingComponents.Farm);
                //    StaticStorage.GetLink<Inventory>("Inventory").Remove(PlantingComponents.Farm);
                //    Destroy(PlantingComponents.DragItem);
                //    PlantingComponents.Clear();
                //}
            }
        }

        public void Clean()
        {
            _free = true;
        }

        public void Planting(Farm farm, int countCards, int countCoolers, int countHards, ComponentPlayerPacket components)
        {
            if (farm != null)
            {
                _free = false;
                _slotFarm.FARM = farm;
                _slotFarm.ID = farm.ID;
                _slotFarm.FARM.PositionXY = _positionXY;
                for (int i = 0; i < countCards; i++)
                {
                    _slotFarm.FARM.SlotsCards.Add(new SlotCard());
                    _slotFarm.FARM.SlotsCards[i]._slotFarm = _slotFarm;
                }
                for (int i = 0; i < countCoolers; i++)
                {
                    _slotFarm.FARM.SlotsCoolers.Add(new SlotCooler());
                    _slotFarm.FARM.SlotsCoolers[i]._slotFarm = _slotFarm;
                }
                for (int i = 0; i < countHards; i++)
                {
                    _slotFarm.FARM.SlotsHards.Add(new SlotHard());
                    _slotFarm.FARM.SlotsHards[i]._slotFarm = _slotFarm;
                }
                _slotFarm.NewSlot();
                if (components != null)
                {
                    if (components.CardData != null)
                    {
                        for (int i = 0; i < components.CardData.Length; i++)
                        {
                            CardData cardData = components.CardData[i];
                            VideoCard card = StaticStorage.GetLink<VideoCardPack>("VideoCardPack").GetVideoCard(cardData.ComponentID);
                            _slotFarm.FARM.SlotsCards[cardData.NumberSlot].VideoCard = new VideoCard
                            {
                                ID = cardData.id,
                                Index = cardData.ComponentID,
                                Name = card.Name,
                                Icon = card.Icon,
                                Price = card.Price,
                                PriceRepair = cardData.PriceRepair,
                                Strength = cardData.Strenght,
                                MaxStrength = card.MaxStrength,
                                Electricity = card.Electricity,
                                Progress = cardData.Progress,
                                Level = card.Level,
                                Income = cardData.Income,
                                Temperature = cardData.Temperature,
                                TemperatureMax = card.TemperatureMax,
                                HeatDissipation = card.HeatDissipation
                            };
                        }
                    }
                    if (components.CoolerData != null)
                    {
                        for (int i = 0; i < components.CoolerData.Length; i++)
                        {
                            CoolerData coolerData = components.CoolerData[i];
                            Cooler cooler = StaticStorage.GetLink<CoolerPack>("CoolerPack").GetCooler(coolerData.ComponentID);
                            _slotFarm.FARM.SlotsCoolers[coolerData.NumberSlot].Cooler = new Cooler
                            {
                                ID = coolerData.id,
                                Index = coolerData.ComponentID,
                                Name = cooler.Name,
                                Icon = cooler.Icon,
                                Price = cooler.Price,
                                PriceRepair = coolerData.PriceRepair,
                                Strength = coolerData.Strenght,
                                MaxStrength = cooler.MaxStrength,
                                Electricity = cooler.Electricity,
                                Progress = coolerData.Progress,
                                Efficiency = cooler.Efficiency,
                                Size = cooler.Size
                            };
                        }
                    }
                    if (components.HardData != null)
                    {
                        for (int i = 0; i < components.HardData.Length; i++)
                        {
                            HardData hardData = components.HardData[i];
                            Hard hard = StaticStorage.GetLink<HardPack>("HardPack").GetHard(hardData.ComponentID);
                            _slotFarm.FARM.SlotsHards[hardData.NumberSlot].HARD = new Hard
                            {
                                ID = hardData.id,
                                Index = hardData.ComponentID,
                                Name = hard.Name,
                                Icon = hard.Icon,
                                Price = hard.Price,
                                PriceRepair = hardData.PriceRepair,
                                Strength = hardData.Strenght,
                                MaxStrength = hard.MaxStrength,
                                Electricity = hard.Electricity,
                                Progress = hardData.Progress,
                                Coins = hardData.Coins,
                                Volume = hard.Volume
                            };
                        }
                    }
                }
                _slotFarm.FirsteHard();
                _slotFarm.CRASH = farm.Crash;
            }
        }

        //public void OnDrop(PointerEventData eventData)
        //{
        //    if (_free == true && PlantingComponents.Farm != null)
        //    {
        //        Planting(PlantingComponents.Farm);
        //        PlantingComponents.SlotFarm.Clear();
        //        PlantingComponents.SlotFarm = null;
        //    }
        //}
    }
}