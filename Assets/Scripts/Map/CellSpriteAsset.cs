﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Data
{
    [CreateAssetMenu(fileName = "CellSpriteAsset", menuName = "Game/SpritePack/CellSpriteAsset")]
    public class CellSpriteAsset : ScriptableObject
    {
        #region Fields

        [SerializeField]
        Sprite _defaultSprite;

        [SerializeField]
        string _typeId;

        [SerializeField]
        List<CellSprite> _sprites;

        #endregion Fields
        #region Properties

        public List<CellSprite> Sprites
        {
            get
            {
                return _sprites;
            }
        }

        public string TypeId
        {
            get
            {
                return _typeId;
            }
        }

        #endregion Properties
        #region Methods

        public Sprite GetSprite(string subType)
        {
            Sprite sprite = _defaultSprite;
            var item = _sprites.Find(x => x.SubType == subType);
            if (item != null)
            {
                sprite = item.Sprite;
            }

            return sprite;
        }

        #endregion Methods
    }

    [Serializable]
    public class CellSprite
    {
        [SerializeField]
        string _subTypeId;

        [SerializeField]
        Sprite _sprite;
        
        public Sprite Sprite
        {
            get
            {
                return _sprite;
            }
        }

        public string SubType
        {
            get
            {
                return _subTypeId;
            }
        }
    }
}