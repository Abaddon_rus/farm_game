﻿using LevelEditor;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Game
{
    public class Arrow : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField]
        BuyPlant _buyPlant;
        [SerializeField]
        EditorMapRoot _editorMapRoot;
        [SerializeField]
        Vector2Int _offset;

        GamePlay _gamePlay;

        private void Start()
        {
            _gamePlay = StaticStorage.GetLink<GamePlay>("GamePlay");
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            _gamePlay.ClosePanels();
            int y = _editorMapRoot._yCount;
            int x = -_editorMapRoot._xCount;
            int count = (x + _offset.x) * y - x * (y + _offset.y);
            float priceSum = count * 5;
            _buyPlant.Show(count, 5, priceSum, _offset);
        }

        private void OnEnable()
        {
            _editorMapRoot.OnSizeChanged += CheckSize;  
        }

        private void OnDisable()
        {
            _editorMapRoot.OnSizeChanged -= CheckSize;
        }

        void CheckSize(int x, int y)
        {
            if (_offset.y == 1 && y == 9)
            {
                gameObject.SetActive(false);
            }
            else if (_offset.x == 1 && x == 9)
            {
                gameObject.SetActive(false);
            }
        }
    }
}