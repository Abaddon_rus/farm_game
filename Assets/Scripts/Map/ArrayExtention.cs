﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public static class ArrayExtention
    {

        public static T[,] IndexCopy<T>(this T[,] source, int xCount, int yCount) where T : new()
        {
            var target = source;
            int sourceX = source.GetUpperBound(0) + 1;
            int sourceY = source.GetUpperBound(1) + 1;

            if (xCount > sourceX && yCount > sourceY)
            {
                target = new T[xCount, yCount];
            }
            else if (xCount > sourceX)
            {
                target = new T[xCount, sourceY];
            }
            else if (yCount > sourceY)
            {
                target = new T[sourceX, yCount];
            }
            else
            {
                return target;
            }

            for (int x = 0; x < sourceX; x++)
            {
                for (int y = 0; y < sourceY; y++)
                {
                    target[x, y] = source[x, y];
                }
            }

            return target;
        }

    }
}