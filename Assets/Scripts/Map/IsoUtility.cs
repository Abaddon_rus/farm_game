﻿using UnityEngine;

public static class IsoUtility
{
    #region Const

    const float WIDTH = 3.65f; //First value in unity units
    const float HEIGHT = 2.12f; //First value in unity units
    const float HALF_WIDTH = WIDTH / 2f; 
    const float HALF_HEIGHT = HEIGHT / 2f;

    #endregion Const
    #region Methods

    public static Vector2Int ToIsoFromWorld(Vector2 position, Vector2 localOffset)
    {
        position -= localOffset;
        float xValue = (position.x / WIDTH + position.y / HEIGHT);
        float yValue = (position.y / HEIGHT - (position.x / WIDTH));
        int x = (int)Mathf.Floor(xValue);
        int y = (int)Mathf.Floor(yValue);
        return new Vector2Int(x, y);
    }

    public static Vector2Int ToIsoFromLocal(Vector2 position)
    {
        int xValue = (int)(position.x / WIDTH + position.y / HEIGHT);
        int yValue = (int)(position.y / HEIGHT - (position.x / WIDTH));
        return new Vector2Int(xValue, yValue);
    }

    public static Vector2 FromIso(float x, float y)
    {
        float xValue = (x - y) * HALF_WIDTH;
        float yValue = (x + y) * HALF_HEIGHT;

        return new Vector2(xValue, yValue);
    }

    #endregion Methods
}
