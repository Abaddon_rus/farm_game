﻿using System.Collections.Generic;
using UnityEngine;

namespace Data
{
    [CreateAssetMenu(fileName = "CellSpritePack", menuName = "Game/SpritePack/CellSpritePack")]
    public class CellSpriteAssetPack : ScriptableObject
    {
        #region Fields

        [SerializeField]
        Sprite _default;

        [SerializeField]
        List<CellSpriteAsset> _assets;

        #endregion Fields
        #region Properties
        
        public List<CellSpriteAsset> Assets
        {
            get
            {
                return _assets;
            }
        }

        #endregion Properties
        #region Methods

        public Sprite GetSprite(string typeId, string subtypeId)
        {
            var asset = Assets.Find(x => x.TypeId == typeId);
            var sprite = asset != null ? asset.GetSprite(subtypeId) : _default;

            return sprite;
        }

        #endregion Methods
    }
} 