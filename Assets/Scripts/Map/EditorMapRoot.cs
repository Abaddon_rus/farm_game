﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Game;

namespace LevelEditor
{
    public class EditorMapRoot : AEditorEntity
    {
        public event Action<int, int> OnSizeChanged = delegate { };

        [SerializeField]
        Transform _startPoint;
        [SerializeField]
        Transform _leftArrow;
        [SerializeField]
        Transform _rightArrow;

        [SerializeField]
        EditorCore _core;

        [SerializeField]
        Plant _plantPrefab;

        public Plant[,] _tiles;
        [SerializeField]
        public List<CoolingPlant> _coolingPlant;

        public int _xCount = 0;

        public int _yCount = 0;

        [SerializeField]
        List<Wall> walls_l;
        [SerializeField]
        List<Wall> walls_r;

        void Awake()
        {
            _core.Register(this);
            _tiles = new Plant[0, 0];
            StaticStorage.AddLink("EditorMapRoot", this);

        }

        private void Start()
        {
            Resize(1, 1);
        }

        public void Resize(int xCount, int yCount)
        {
            ClearOld(xCount, yCount);

            var xIndex = _xCount;
            var yIndex = _yCount;
            RecreateField(xCount, yCount);

            CreateField(xIndex, yIndex);
            OnSizeChanged.Invoke(xCount, yCount);

            StaticStorage.GetLink<GamePlay>("GamePlay")._maxDistance = 4 + (xCount + yCount) * 0.9f;
            StaticStorage.GetLink<GamePlay>("GamePlay")._maxBorder = (xCount * yCount) / 18f;
            StaticStorage.GetLink<GamePlay>("GamePlay")._minBorder = -(xCount * yCount) / 18f;
            Camera.main.orthographicSize = StaticStorage.GetLink<GamePlay>("GamePlay")._maxDistance;
            Camera.main.transform.position = new Vector3(0, 0, 0);
        }

        void RecreateField(int xCount, int yCount)
        {
            _tiles = _tiles.IndexCopy(xCount, yCount);

            _xCount = xCount;
            _yCount = yCount;
        }

        void CreateField(int xIndex, int yIndex)
        {
            for (int x = 0; x < _xCount; x++)
            {
                for (int y = 0; y < _yCount; y++)
                {
                    Plant plant;
                    if (x >= xIndex || y >= yIndex)
                    {
                        plant = Instantiate(_plantPrefab, _startPoint);
                        var position = IsoUtility.FromIso(-x, -y);
                        _leftArrow.localPosition = IsoUtility.FromIso(-x - 1, -y / 2);
                        _rightArrow.localPosition = IsoUtility.FromIso(-x / 2, -y - 1);
                        plant.transform.localPosition = position;
                        plant._positionXY = new Vector2Int(x, y);
                        _tiles[x, y] = plant;
                    }
                    else
                    {
                        plant = _tiles[x, y];
                    }
                }
            }
            for (int i = 0; i < _yCount; i++)
            {
                walls_r[i].gameObject.SetActive(true);
            }
            for (int i = 0; i < _xCount; i++)
            {
                walls_l[i].gameObject.SetActive(true);
            }
        }

        void ClearOld(int xCount, int yCount)
        {
            for (int x = 0; x < _xCount; x++)
            {
                for (int y = 0; y < _yCount; y++)
                {
                    if (x >= xCount || y >= yCount)
                    {
                        var tile = _tiles[x, y];
                        if (tile != null)
                        {
                            Destroy(tile.gameObject);
                            _tiles[x, y] = null;
                        }
                    }
                }
            }
        }

        bool CheckX(int x)
        {
            return x >= 0 && x < _xCount;
        }

        bool CheckY(int y)
        {
            return y >= 0 && y < _yCount;
        }
    }
}