﻿using System;
using UnityEngine;

namespace Data
{
    [Serializable]
    public class CellSettings
    {
        public CellSettings(CellSpriteData spriteData, CellFactorData cellData)
        {
            _spriteData = spriteData;
            _cellData = cellData;
        }

        [SerializeField]
        CellSpriteData _spriteData;

        [SerializeField]
        CellFactorData _cellData;

        public CellSpriteData SpriteData
        {
            get
            {
                return _spriteData;
            }
        }

        public CellFactorData CellData
        {
            get
            {
                return _cellData;
            }
        }
    }

    [Serializable]
    public class CellSpriteData
    {
        public string Type;
        public string Subtype;
    }

    [Serializable]
    public class CellFactorData
    {
        /// <summary>
        /// Модификатор атаки
        /// </summary>
        public float AtackFactor;

        /// <summary>
        /// Модификатор дальности
        /// </summary>
        public float RangeFactor;

        /// <summary>
        /// Модификатор цены
        /// </summary>
        public float CostFactor;

        /// <summary>
        /// Модификатор прироста золота
        /// </summary>
        public float CoinsFactor;
    }
}