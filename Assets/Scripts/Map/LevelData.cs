﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Data
{

    [Serializable]
    public class LevelData
    {
        #region Constructor

        public LevelData(List<CellSettings> cells, int xCount, int yCount, Vector3 cameraPosition)
        {
            _xCount = xCount;
            _yCount = yCount;
            _cells = cells;
            _cameraPosition = cameraPosition;
        }

        #endregion Constructor
        #region Fields        

        [SerializeField]
        int _xCount;

        [SerializeField]
        int _yCount;

        [SerializeField]
        List<CellSettings> _cells;

        [SerializeField]
        Vector3 _cameraPosition;
        
        #endregion Fields
        #region Properties
        
        public List<CellSettings> Cells
        {
            get
            {
                return _cells;
            }
        }

        public Vector3 CameraPosition
        {
            get
            {
                return _cameraPosition;
            }
        }

        public int XCount
        {
            get
            {
                return _xCount;
            }
        }

        public int YCount
        {
            get
            {
                return _yCount;
            }
        }

        #endregion Properties
    }
}