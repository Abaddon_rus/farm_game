﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace LevelEditor
{
    public class EditorCore : MonoBehaviour
    {
        Dictionary<Type, AEditorEntity> _entityes = new Dictionary<Type, AEditorEntity>();

        public void Register<T>(T core) where T : AEditorEntity
        {
            var type = typeof(T);
            if (!_entityes.ContainsKey(type))
            {
                _entityes.Add(type, core);
            }
        }

        public void Unregister<T>(T core) where T : AEditorEntity
        {
            var type = typeof(T);
            if (_entityes.ContainsKey(type))
            {
                _entityes.Remove(type);
            }
        }

        public T GetEntity<T>() where T : AEditorEntity
        {
            var type = typeof(T);
            AEditorEntity core = null;
            if (!_entityes.TryGetValue(type, out core))
            {
                Debug.LogError($"Не найдено Ядро редактора: {type}");
                core = Activator.CreateInstance(type) as T;
                _entityes.Add(type, core);
            }
            return core as T;
        }
    }

    public class AEditorEntity : MonoBehaviour { }
}