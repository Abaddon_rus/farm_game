﻿using UnityEngine;
using UnityEngine.EventSystems;
using Game;
using System;
using UnityEngine.Rendering;

public class GetCoins : MonoBehaviour, IPointerClickHandler
{
    [SerializeField]
    SlotFarm _farm;
    [SerializeField]
    SortingGroup _sortingGroup;
    [SerializeField]
    SpriteRenderer _spriteRendererThis;

    public void OnPointerClick(PointerEventData eventData)
    {
        NetworkCore.Send("PickedUpCoins", new DataPacket
        {
            v1 = _farm.ID
        }, (data) => {
            if (Convert.ToBoolean(data))
            {
                _farm.GetCoins();
                PlayerLevel.Exp = 5;
            }
        });
    }

    private void Start()
    {
        _spriteRendererThis.sortingOrder = _sortingGroup.sortingOrder + 3;
    }
}
