﻿using System;
using System.Collections;
using System.Globalization;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class Coin : MonoBehaviour
    {
        [SerializeField]
        TextMeshProUGUI _coinWiget;
        [SerializeField]
        float _coin;

        private void Awake()
        {
            StaticStorage.AddLink("Coin", this);
        }

        public float COIN
        {
            set
            {
                if (_coin != value)
                {
                    _coin = Mathf.Clamp((float)Math.Round(value, 6), 0, float.MaxValue);
                    _coinWiget.text = _coin.ToString();
                }
            }
            get
            {
                return _coin;
            }
        }

        void RefreshCoins(float i)
        {
            COIN = i;
        }

        public void CheckCoins()
        {
            NetworkCore.Send("CheckCoins", new DataPacket
            {
                v1 = 0
            }, (data) =>
            {
                RefreshCoins((float)Convert.ToDouble(data, CultureInfo.InvariantCulture));
                
            });
        }
        

    }


}