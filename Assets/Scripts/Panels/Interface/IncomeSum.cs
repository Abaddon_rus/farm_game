﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Game
{
    public class IncomeSum : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField]
        TextMeshProUGUI _textHour;
        [SerializeField]
        TextMeshProUGUI _textDay;
        [SerializeField]
        TextMeshProUGUI _textMonth;

        List<VideoCard> _videoCards = new List<VideoCard>();

        float _income;

        int stat = 1;

        private void Awake()
        {
            StaticStorage.AddLink("IncomeSum", this);
        }

        public void Add(VideoCard videoCard)
        {
            _videoCards.Add(videoCard);
            Convers();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (stat == 1)
            {
                stat = 2;
                _textHour.gameObject.SetActive(false);
                _textDay.gameObject.SetActive(true);
                _textMonth.gameObject.SetActive(false);
            }
            else if (stat == 2)
            {
                stat = 3;
                _textHour.gameObject.SetActive(false);
                _textDay.gameObject.SetActive(false);
                _textMonth.gameObject.SetActive(true);
            }
            else
            {
                stat = 1;
                _textHour.gameObject.SetActive(true);
                _textDay.gameObject.SetActive(false);
                _textMonth.gameObject.SetActive(false);
            }
        }

        public void Remove(VideoCard videoCard)
        {
            _videoCards.Remove(videoCard);
            Convers();
        }

        private void Convers()
        {
            float income = 0;
            for (int i = 0; i < _videoCards.Count; i++)
            {
                income += _videoCards[i].Income;
            }
            _income = income;
            _textHour.text = _income + " $/h";
            _textDay.text = _income * 24 + " $/d";
            _textMonth.text = _income * 24 * 30 + " $/m";
        }
    }
}