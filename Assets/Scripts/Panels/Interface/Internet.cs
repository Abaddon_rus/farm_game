﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using LevelEditor;

namespace Game
{
    public class Internet : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField]
        TextMeshProUGUI _dateEnd;
        [SerializeField]
        TextMeshProUGUI _newDate;
        [SerializeField]
        TextMeshProUGUI _price;

        [SerializeField]
        Button _buttonOpenPanel;

        [SerializeField]
        GameObject _panelBuyInternet;
        [SerializeField]
        Button _buyInternet;

        int _timeEndInternet;
        EditorMapRoot _editorMapRoot;
        Coin _coin;

        private void Awake()
        {
            StaticStorage.AddLink("Internet", this);
        }

        void Start()
        {
            _editorMapRoot = StaticStorage.GetLink<EditorMapRoot>("EditorMapRoot");
            _coin = StaticStorage.GetLink<Coin>("Coin");
            _buttonOpenPanel.onClick.AddListener(OpenPanel);
            _buyInternet.onClick.AddListener(BuyInternet);
            ClosePanel();
            _panelBuyInternet.transform.localPosition = new Vector3(0, 0, 0);
        }

        void BuyInternet()
        {
            NetworkCore.Send("BuyIntegnet", new Packet(), (data) =>
            {
                if (Convert.ToBoolean(data))
                {
                    ChangeTimeEndInternet((int)DateTimeOffset.UtcNow.ToUnixTimeSeconds() + 30 * 24 * 3600);
                    ClosePanel();
                }
            });
        }

        public void ChangeTimeEndInternet(int i)
        {
            _timeEndInternet = i;
            _dateEnd.text = i.UnixToDate();
        }

        void OpenPanel()
        {
            float price = (2 + (_editorMapRoot._xCount * _editorMapRoot._yCount - 1) / 100 * 16);
            _price.text = price + "$";
            _buyInternet.interactable = _coin.COIN >= price;
            _newDate.text = _dateEnd.text + " >>> " + ((int)DateTimeOffset.UtcNow.ToUnixTimeSeconds() + 30 * 24 * 3600).UnixToDate();
            _panelBuyInternet.SetActive(true);
        }

        public void ClosePanel()
        {
            _panelBuyInternet.SetActive(false);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            _dateEnd.text = _timeEndInternet.UnixToDateAndTime();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            _dateEnd.text = _timeEndInternet.UnixToDate();
        }
    }
}