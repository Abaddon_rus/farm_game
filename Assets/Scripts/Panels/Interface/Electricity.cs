﻿using LevelEditor;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class Electricity : MonoBehaviour, IUpdatable
    {
        List<SlotFarm> _farm = new List<SlotFarm>(9);
        List<SlotCooling> _coolingSlot = new List<SlotCooling>(9);

        [SerializeField]
        Text _vt;
        [SerializeField]
        Text _time;

        [SerializeField]
        float _electricity;
        float _expenses;
        float _hours;
        Coin _coin;
        EditorMapRoot _editorMapRoot;

        int _countBuy;
        float _price;

        [SerializeField]
        Button _buttonOpenPanel;
        [SerializeField]
        Slider _slideBuy;
        [SerializeField]
        Button _buttonBuy;
        [SerializeField]
        TextMeshProUGUI _textPoint;
        [SerializeField]
        TextMeshProUGUI _description;
        [SerializeField]
        GameObject _electricityPanel;

        public float ELECTRICITY
        {
            set
            {
                _electricity = value;
                ElectricityConverse();
            }
            get
            {
                return _electricity;
            }
        }

        void Awake()
        {
            StaticStorage.AddLink("Electricity", this);
            _buttonBuy.onClick.AddListener(Buy);
            _buttonOpenPanel.onClick.AddListener(OpenPanel);
            _slideBuy.onValueChanged.AddListener(ChangeSlider);
            _electricityPanel.SetActive(false);
            _electricityPanel.transform.localPosition = new Vector3(0, 0, 0);
        }

        private void OpenPanel()
        {
            _electricityPanel.SetActive(true);
        }

        public void ClosePanel()
        {
            _electricityPanel.SetActive(false);
        }

        private void ChangeSlider(float value)
        {
            _countBuy = Convert.ToInt32(value);
            _price = _countBuy * (0.01f - (_editorMapRoot._xCount * _editorMapRoot._yCount - 1) / 100000 * 2);
            _textPoint.text = _countBuy + "kVt - " + _price + "$";
            int day = (int)Mathf.Round(_countBuy * 1000 / _expenses / 24);
            if (day > 0)
            {
                _description.text = "Enough for " + day + " days farm operation";
            }
            else
            {
                _description.text = "";
            }
        }

        void Start()
        {
            _coin = StaticStorage.GetLink<Coin>("Coin");
            _editorMapRoot = StaticStorage.GetLink<EditorMapRoot>("EditorMapRoot");
            StaticStorage.UpdateCore.Add(this);
        }

        void OnDestroy() => StaticStorage.UpdateCore.Remove(this);

        private void Buy()
        {
            if (_coin.COIN >= _price)
            {
                NetworkCore.Send("BuyElectricity", new DataPacket
                {
                    v1 = _countBuy
                }, (data) =>
                {
                    if (Convert.ToBoolean(data))
                    {
                        ELECTRICITY += _countBuy * 1000;
                        _coin.COIN -= _price;
                        ClosePanel();
                    }
                });
            }
        }

        public void ElectricityConverse()
        {
            _expenses = 0;
            for (int i = 0; i < _farm.Count; i++)
                if (!_farm[i].STOP)
                    _expenses += _farm[i]._consumption;
            for (int i = 0; i < _coolingSlot.Count; i++)
                if (!_coolingSlot[i].IsStoped)
                    _expenses += _coolingSlot[i]._consumption;
            _hours = Mathf.Round(_electricity / _expenses);
            Calculate();
        }

        void Calculate()
        {
            if (_electricity / 1000 >= 10)
            {
                _vt.text = Mathf.Round(_electricity / 1000) + " kVt";
            }
            else
            {
                _vt.text = Math.Round(_electricity, 2) + " Vt";
            }
            if (_expenses == 0)
            {
                _time.text = "/ --- h.";
            }
            else
            {
                _time.text = "/ " + _hours + " h.";
            }
            if (_electricity <= 0)
            {
                for (int i = 0; i < _farm.Count; i++)
                    _farm[i].STOP = true;
                for (int i = 0; i < _coolingSlot.Count; i++)
                    _coolingSlot[i].IsStoped = true;
            }
        }

        public void AddFarm(SlotFarm farm)
        {
            _farm.Add(farm);
            ElectricityConverse();
        }

        public void FarmRemove(SlotFarm farm)
        {
            _farm.Remove(farm);
            ElectricityConverse();
        }

        public void CoolingAdd(SlotCooling cooling)
        {
            _coolingSlot.Add(cooling);
            ElectricityConverse();
        }

        public void CoolingRemove(SlotCooling cooling)
        {
            _coolingSlot.Remove(cooling);
            ElectricityConverse();
        }

        void IUpdatable.OnUpdate(float deltaTime)
        {
            _electricity -= _expenses / 3600f * deltaTime;
            Calculate();
        }
    }
}