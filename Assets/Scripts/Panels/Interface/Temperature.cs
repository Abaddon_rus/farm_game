﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class Temperature : MonoBehaviour
    {
        List<SlotFarm> _slotFarm = new List<SlotFarm>();
        List<SlotCooling> _slotCoolings = new List<SlotCooling>();

        [SerializeField]
        Text _text;

        [SerializeField]
        Coroutine _heatDissipationCorutine;

        float _heatDissipation;
        public float _temperature = 18;
        float _conditionerMin = 18;

        public int _emergencyCoolingStart;

        public float TEMPERATURE
        {
            set
            {
                _temperature = value;
                _text.text = (float)Math.Round(_temperature, 2) + " Cº";
            }
        }

        private void Awake()
        {
            StaticStorage.AddLink("Temperature", this);
            _text.text = "+" + _temperature + " Cº";
        }

        private void Start()
        {
            _heatDissipationCorutine = StartCoroutine(HeatDissipation());
        }

        public void Add(SlotFarm slotFarm)
        {
            if (!_slotFarm.Find(x => slotFarm))
            {
                _slotFarm.Add(slotFarm);
            }
            Conversion();
        }

        public void Remove(SlotFarm slotFarm)
        {
            if (_slotFarm.Find(x => slotFarm))
            {
                _slotFarm.Remove(slotFarm);
            }
            Conversion();
        }

        public void Add(SlotCooling slotCooling)
        {
            if (!_slotCoolings.Find(x => slotCooling))
            {
                _slotCoolings.Add(slotCooling);
            }
            Conversion();
        }

        public void Remove(SlotCooling slotCooling)
        {
            if (_slotCoolings.Find(x => slotCooling))
            {
                _slotCoolings.Remove(slotCooling);
            }
            Conversion();
        }

        void Conversion()
        {
            _heatDissipation = 0;
            for (int i = 0; i < _slotFarm.Count; i++)
            {
                _heatDissipation += _slotFarm[i]._heatDissipation;
            }
            for (int i = 0; i < _slotCoolings.Count; i++)
            {
                _heatDissipation += _slotCoolings[i].Cooling.CoolingTemperature;
            }
        }

        private IEnumerator HeatDissipation()
        {
            yield return new WaitForSecondsRealtime(1);
            _temperature = (float)Math.Round(Mathf.Clamp(_temperature + _heatDissipation / 3600, _conditionerMin, 120), 8);
            _text.text = (float)Math.Round(_temperature, 2) + " Cº";
            _heatDissipationCorutine = StartCoroutine(HeatDissipation());
        }
    }
}