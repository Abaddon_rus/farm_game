﻿using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class TopMenu : MonoBehaviour
    {
        [SerializeField]
        Button _farmsButton;
        [SerializeField]
        Button _workplaceButton;
        [SerializeField]
        Button _datacenterButton;

        [SerializeField]
        Workplace _workplace;
        [SerializeField]
        DataCenter _dataCenter;

        [SerializeField]
        Image _select;

        private void Awake()
        {
            _farmsButton.onClick.AddListener(OpenFarms);
            _workplaceButton.onClick.AddListener(OpenWorkplace);
            _datacenterButton.onClick.AddListener(OpenDatacenter);
        }

        private void OpenFarms()
        {
            Close();
            _select.transform.position = _farmsButton.transform.position;
        }

        private void OpenDatacenter()
        {
            Close();
            _dataCenter.OpenDataCenter();
            _select.transform.position = _datacenterButton.transform.position;
        }

        private void OpenWorkplace()
        {
            Close();
            _workplace.gameObject.SetActive(true);
            _select.transform.position = _workplaceButton.transform.position;
        }

        private void Close()
        {
            _workplace.gameObject.SetActive(false);
            _dataCenter.gameObject.SetActive(false);
        }
    }
}