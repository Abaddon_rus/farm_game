﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class MiniBarTasks : MonoBehaviour
    {
        [SerializeField]
        Text _timerText;
        [SerializeField]
        Image _fill;

        public void Refresh(string time, float fill)
        {
            _fill.fillAmount = fill;
            _timerText.text = time;
        }
    }
}
