﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class ObjectInInventoryOther : MonoBehaviour
    {
        [SerializeField]
        Text _name;
        [SerializeField]
        Image _icon;
        [SerializeField]
        Text _descriptionTxt;
        [SerializeField]
        Button _buttonBuy;
        [SerializeField]
        Text _count;

        Other _other;

        private void OnEnable()
        {
            _buttonBuy.onClick.AddListener(Use);
            _buttonBuy.interactable = false;
        }

        private void OnDisable()
        {
            _buttonBuy.onClick.RemoveListener(Use);
        }

        private void Update()
        {
            _buttonBuy.interactable = _other.Count > 0;
        }

        public void Instant(Other other)
        {
            _other = other;
            _name.text = _other.Name;
            _icon.sprite = _other.Icon;
            _count.text = _other.Count.ToString();
            _descriptionTxt.text = _other.Description;
        }

        private void Use()
        {

        }
    }
}