﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
namespace Game
{
    public class ObjectInInventory : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
    {
        [SerializeField]
        Image _gray;
        [SerializeField]
        TextMeshProUGUI _name;
        [SerializeField]
        Image _icon;
        [SerializeField]
        TextMeshProUGUI _priceTxt;
        [SerializeField]
        TextMeshProUGUI _parametr1;
        [SerializeField]
        TextMeshProUGUI _parametr2;
        [SerializeField]
        TextMeshProUGUI _parametr3;
        [SerializeField]
        TextMeshProUGUI _parametr4;
        [SerializeField]
        TextMeshProUGUI _parametr5;
        [SerializeField]
        Button _buttonRepair;
        [SerializeField]
        Button _setItem;
        Packs _packs;

        [SerializeField]
        RepairPanel _repairPanel;

        [SerializeField]
        TimerPanel _timerRepair;

        GameObject _drag;
        Coin _coin;
        Action _action;

        private void OnEnable()
        {
            _buttonRepair.onClick.AddListener(Repair);
            _setItem.onClick.AddListener(SetItem);
        }

        private void Start()
        {
            _repairPanel = StaticStorage.GetLink<RepairPanel>("RepairPanel");
            _coin = StaticStorage.GetLink<Coin>("Coin");
            _buttonRepair.interactable = _coin.COIN >= _packs.Price && _packs.CountRepairs < 3;
            if (_packs.CountRepairs >= 3)
            {
                _priceTxt.text = "Non-repairable";
            }
        }

        private void OnDisable()
        {
            _buttonRepair.onClick.RemoveListener(Repair);
            _setItem.onClick.RemoveListener(SetItem);
        }

        private void Repair()
        {
            _repairPanel.OpenPanel(_packs);
        }

        public void Instant(Packs packs)
        {
            _packs = packs;
            _name.text = _packs.Name;
            _icon.sprite = _packs.Icon;
            _parametr2.text = "Strength: " + _packs.Strength + "/" + _packs.MaxStrength;

            _gray.gameObject.SetActive(false);

            if (packs is VideoCard)
            {
                VideoCard videoCard = packs as VideoCard;
                _parametr1.text = "Icome: " + videoCard.Income + "$";
                _parametr3.text = "Heat dissipation: " + videoCard.HeatDissipation + " C°";
                _parametr4.text = "Consumption: " + videoCard.Electricity + " Vt";
                _parametr5.text = "Minimal server level: " + videoCard.MinFarmLvl + " lvl";
            }
            else if (packs is Cooler)
            {
                Cooler cooler = packs as Cooler;
                _parametr1.text = "Cooling: " + cooler.Efficiency + " C°";
                _parametr3.text = "";
                _parametr4.text = "";
                _parametr5.text = "";
            }
            else if (packs is Hard)
            {
                Hard hard = packs as Hard;
                _parametr1.text = "Volume: " + hard.Volume + " $";
                _parametr3.text = "";
                _parametr4.text = "";
                _parametr5.text = "";
            }
            _action = () => { _timerRepair.gameObject.SetActive(false); };
            _timerRepair.TimerStart(_packs.TimeEndRepair, _action);
        }

        public void Setect(bool value)
        {
            _gray.gameObject.SetActive(value);
        }

        void SetItem()
        {
            PlantingComponents.Clear();
            if (PlantingComponents.DragItem != null)
                Destroy(PlantingComponents.DragItem);
            Setect(true);
            PlantingComponents.Temp(_packs);
            PlantingComponents.DragItem = new GameObject();
            _drag = PlantingComponents.DragItem;
            _drag.name = "dragObject";
            _drag.AddComponent<DragItem>()._objectInInventory = this;
            _drag.AddComponent<Pursuit>();
            SpriteRenderer spriteRenderer = _drag.AddComponent<SpriteRenderer>();
            spriteRenderer.sprite = _packs.Icon;
            spriteRenderer.sortingOrder = 101;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            Destroy(_drag);
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            SetItem();
        }

        public void OnDrag(PointerEventData eventData)
        {
            Vector3 end = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            _drag.transform.position = end + new Vector3(0, 0, 10);
            RaycastHit2D raycastHit2D = Physics2D.Raycast(end, end);
            if (raycastHit2D.collider != null)
            {
                ComponentInpanel cp = raycastHit2D.collider.GetComponent<ComponentInpanel>();
                if (cp != null)
                {
                    _drag.transform.position = cp.transform.position;
                }
            }
        }
    }
}