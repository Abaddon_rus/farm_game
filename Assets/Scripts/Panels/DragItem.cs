﻿using UnityEngine;

namespace Game
{
    public class DragItem : MonoBehaviour
    {
        public ObjectInInventory _objectInInventory;

        private void OnDestroy()
        {
            if (_objectInInventory != null)
            {
                _objectInInventory.Setect(false);
            }
        }
    }
}