﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Game
{
    public class ObjectInShop : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField]
        TextMeshProUGUI _name;
        [SerializeField]
        Image _icon;
        [SerializeField]
        TextMeshProUGUI _priceTxt;
        [SerializeField]
        TextMeshProUGUI _parametr1;
        [SerializeField]
        TextMeshProUGUI _parametr2;
        [SerializeField]
        TextMeshProUGUI _parametr3;
        [SerializeField]
        TextMeshProUGUI _parametr4;
        [SerializeField]
        TextMeshProUGUI _parametr5;
        [SerializeField]
        Button _buttonBuy;
        Packs _packs;

        [SerializeField]
        GameObject _donate;

        private void OnEnable()
        {
            _buttonBuy.onClick.AddListener(Buy);
        }

        private void OnDisable()
        {
            _buttonBuy.onClick.RemoveListener(Buy);
        }

        private void Start()
        {
            _buttonBuy.interactable = StaticStorage.GetLink<Coin>("Coin").COIN >= _packs.Price;
        }

        private void Buy()
        {
            if (StaticStorage.GetLink<Coin>("Coin").COIN >= _packs.Price)
            {
                if (_packs is VideoCard)
                {
                    NetworkCore.Send("BuyCard", new DataPacket
                    {
                        v1 = _packs.Index
                    }, (data) =>
                    {
                        AddInventory(Convert.ToInt32(data));
                    });
                }
                else if (_packs is Cooler)
                {
                    NetworkCore.Send("BuyCooler", new DataPacket
                    {
                        v1 = _packs.Index
                    }, (data) =>
                    {
                        AddInventory(Convert.ToInt32(data));
                    });
                }
                else if (_packs is Hard)
                {
                    NetworkCore.Send("BuyHard", new DataPacket
                    {
                        v1 = _packs.Index
                    }, (data) =>
                    {
                        AddInventory(Convert.ToInt32(data));
                    });
                }
                PlayerLevel.Exp = 20;
            }
            else
            {
                Debug.Log("Не хватает средств");
            }
        }

        void AddInventory(int id)
        {
            if (id != 0)
            {
                StaticStorage.GetLink<Coin>("Coin").CheckCoins();
                _packs.ID = id;
                StaticStorage.GetLink<Inventory>("Inventory").Add(_packs);
            }
        }


        public void Instant(Packs packs)
        {
            _packs = packs;
            _name.text = _packs.Name;
            _icon.sprite = _packs.Icon;
            _priceTxt.text = "Buy: " + _packs.Price + "$";
            _parametr2.text = "Strength: " + _packs.Strength;

            if (packs is VideoCard)
            {
                VideoCard videoCard = packs as VideoCard;
                _parametr1.text = "Income: " + videoCard.Income.ToString() + "$";
                _parametr3.text = "Heat dissipation: " + videoCard.HeatDissipation + " C°";
                _parametr4.text = "Consumption: " + videoCard.Electricity + " Vt";
                _parametr5.text = "Minimal server level: " + videoCard.MinFarmLvl + " lvl";
            }
            else if (packs is Cooler)
            {
                Cooler cooler = packs as Cooler;
                _parametr1.text = "Cooling: " + cooler.Efficiency.ToString() + " C°";
                _parametr3.text = "";
                _parametr4.text = "";
                _parametr5.text = "";
            }
            else if (packs is Hard)
            {
                Hard hard = packs as Hard;
                _parametr1.text = "Volume: " + hard.Volume.ToString() + " $";
                _parametr3.text = "";
                _parametr4.text = "";
                _parametr5.text = "";
            }
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (!_buttonBuy.interactable)
            {
                _donate.SetActive(true);
            }
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            _donate.SetActive(false);
        }
    }
}