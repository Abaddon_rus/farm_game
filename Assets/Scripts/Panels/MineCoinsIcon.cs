﻿using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class MineCoinsIcon : MonoBehaviour
    {
        [SerializeField]
        Image _icon;
        [SerializeField]
        Vector3 _offset;

        Transform _transformFarm;

        public MineCoinsIcon Transform(Transform transform)
        {
            _transformFarm = transform;
            return this;
        }

        private void Update()
        {
            if (_transformFarm != null)
            {
                var camera = Camera.main;
                var worldPosition = _transformFarm.transform.position + _offset;
                var viewportPosition = camera.WorldToViewportPoint(worldPosition);
                var rt = transform as RectTransform;

                rt.anchorMin = viewportPosition;
                rt.anchorMax = viewportPosition;
            }
        }

        public void Show(float fillAmount)
        {
            _icon.fillAmount = fillAmount;
        }
    }
}
