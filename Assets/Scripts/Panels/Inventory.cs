﻿using PacketLib;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class Inventory : MonoBehaviour
    {
        [SerializeField]
        GameObject _plaseOfObjects;
        [SerializeField]
        FarmPack _farmPackInventory;
        [SerializeField]
        CoolingPack _coolingPackInventory;
        [SerializeField]
        VideoCardPack _videoCardPackInventory;
        [SerializeField]
        CoolerPack _coolerPackInventory;
        [SerializeField]
        HardPack _hardPackInventory;
        [SerializeField]
        OtherPack _otherPackInventory;
        [SerializeField]
        ObjectInInventory _prefabObjectInInventory;
        [SerializeField]
        ObjectInInventoryOther _prefabObjectInInventoryOther;
        [SerializeField]
        Transform _bgMenuButtons;
        [SerializeField]
        Menu _menu;

        //[SerializeField]
        //Button _farmInInventoryButton;
        //[SerializeField]
        //Button _coolingInInventoryButton;
        [SerializeField]
        Button _videcardInInventoryButton;
        [SerializeField]
        Button _coolerInInventoryButton;
        [SerializeField]
        Button _hardInInventoryButton;
        [SerializeField]
        Button _otherInventoryButton;
        [SerializeField]
        Text _title;

        private void ClearInventory()
        {
            //_farmPackInventory.Farms.Clear();
            //_coolingPackInventory.Cooling.Clear();
            _videoCardPackInventory.Cards.Clear();
            _coolerPackInventory.Cooler.Clear();
            _hardPackInventory.Hard.Clear();
        }

        private void Start()
        {
            gameObject.SetActive(false);
        }

        private void OnDestroy()
        {
            ClearInventory();
        }

        private void OnEnable()
        {
            //_farmInInventoryButton.onClick.AddListener(FarmInInventory);
            //_coolingInInventoryButton.onClick.AddListener(CoolingInInventory);
            _videcardInInventoryButton.onClick.AddListener(VidecardInInventory);
            _coolerInInventoryButton.onClick.AddListener(CoolerInInventory);
            _hardInInventoryButton.onClick.AddListener(HardInInventory);
            _otherInventoryButton.onClick.AddListener(OtherInInventory);
            _bgMenuButtons.localPosition = new Vector3(-123.48f, 0, 0);
        }

        private void OnDisable()
        {
            //_farmInInventoryButton.onClick.RemoveListener(FarmInInventory);
            //_coolingInInventoryButton.onClick.RemoveListener(CoolingInInventory);
            _videcardInInventoryButton.onClick.RemoveListener(VidecardInInventory);
            _coolerInInventoryButton.onClick.RemoveListener(CoolerInInventory);
            _hardInInventoryButton.onClick.RemoveListener(HardInInventory);
            _otherInventoryButton.onClick.RemoveListener(OtherInInventory);
        }

        public void Add(Packs pack)
        {
            if (pack is VideoCard)
            {
                Add(pack as VideoCard);
            }
            else if (pack is Cooler)
            {
                Add(pack as Cooler);
            }
            else if (pack is Hard)
            {
                Add(pack as Hard);
            }
            //else if (pack is Cooling)
            //{
            //    Add(pack as Cooling);
            //}
            //else if (pack is Farm)
            //{
            //    Add(pack as Farm);
            //}
            else
            {
                Debug.LogWarning("Чё за херня?");
            }
        }

        public void Remove(Packs pack)
        {
            if (pack is VideoCard)
            {
                _videoCardPackInventory.Cards.Remove(pack as VideoCard);
                VidecardInInventory();
            }
            else if (pack is Cooler)
            {
                _coolerPackInventory.Cooler.Remove(pack as Cooler);
                CoolerInInventory();
            }
            else if (pack is Hard)
            {
                _hardPackInventory.Hard.Remove(pack as Hard);
                HardInInventory();
            }
            //else if (pack is Cooling)
            //{
            //    _coolingPackInventory.Cooling.Remove(pack as Cooling);
            //    CoolerInInventory();
            //}
            //else if (pack is Farm)
            //{
            //    _farmPackInventory.Farms.Remove(pack as Farm);
            //    FarmInInventory();
            //}
            else
            {
                Debug.LogWarning("Чё за херня?");
            }
        }

        void Add(VideoCard videoCard)
        {
            var dub = videoCard.Clone();
            _videoCardPackInventory.Cards.Add(dub);
        }

        void Add(Cooler cooler)
        {
            var dub = cooler.Clone();
            _coolerPackInventory.Cooler.Add(dub);
        }

        void Add(Hard hard)
        {
            var dub = hard.Clone();
            _hardPackInventory.Hard.Add(dub);
        }

        void Add(Farm farm)
        {
            var dub = farm.Clone();
            _farmPackInventory.Farms.Add(dub);
        }

        void Add(Cooling cooling)
        {
            var dub = cooling.Clone();
            _coolingPackInventory.Cooling.Add(dub);
        }

        public void Add(Other other, int count)
        {
            _otherPackInventory.AddCountOther(other.Name, count);
        }

        void Clear()
        {
            _menu.OpenPanel();
            _menu.InventoryFocus();
            ObjectInInventory[] objectInInventorys = _plaseOfObjects.GetComponentsInChildren<ObjectInInventory>();
            for (int i = 0; i < objectInInventorys.Length; i++)
                Destroy(objectInInventorys[i].gameObject);
            ObjectInInventoryOther[] ObjectInInventoryOther = _plaseOfObjects.GetComponentsInChildren<ObjectInInventoryOther>();
            for (int i = 0; i < ObjectInInventoryOther.Length; i++)
                Destroy(ObjectInInventoryOther[i].gameObject);
        }

        public void Check()
        {
            if (_title.text == "Videocards")
            {
                VidecardInInventory();
            }
            else if (_title.text == "Coolers")
            {
                CoolerInInventory();
            }
            else if (_title.text == "SSD")
            {
                HardInInventory();
            }
            else
            {
                OtherInInventory();
            }
        }

        public void VidecardInInventory()
        {
            Clear();
            _title.text = "Videocards";
            if (_videoCardPackInventory.Cards.Count > 0)
            {
                for (int i = 0; i < _videoCardPackInventory.Cards.Count; i++)
                {
                    ObjectInInventory objectInInventory = Instantiate(_prefabObjectInInventory, _plaseOfObjects.transform);
                    objectInInventory.Instant(_videoCardPackInventory.Cards[i]);
                }
            }
            else
            {
                _menu.OpenShop();
            }
        }

        public void CoolerInInventory()
        {
            Clear();
            _title.text = "Coolers";
            if (_coolerPackInventory.Cooler.Count > 0)
            {
                for (int i = 0; i < _coolerPackInventory.Cooler.Count; i++)
                {
                    ObjectInInventory objectInInventory = Instantiate(_prefabObjectInInventory, _plaseOfObjects.transform);
                    objectInInventory.Instant(_coolerPackInventory.Cooler[i]);
                }
            }
            else
            {
                _menu.OpenShop();
            }
        }

        public void HardInInventory()
        {
            Clear();
            _title.text = "SSD";
            if (_hardPackInventory.Hard.Count > 0)
            {
                for (int i = 0; i < _hardPackInventory.Hard.Count; i++)
                {
                    ObjectInInventory objectInInventory = Instantiate(_prefabObjectInInventory, _plaseOfObjects.transform);
                    objectInInventory.Instant(_hardPackInventory.Hard[i]);
                }
            }
            else
            {
                _menu.OpenShop();
            }
        }

        private void OtherInInventory()
        {
            Clear();
            _title.text = "Other";
            if (_otherPackInventory.Other.Count > 0)
            {
                for (int i = 0; i < _otherPackInventory.Other.Count; i++)
                {
                    ObjectInInventoryOther ObjectInInventoryOther = Instantiate(_prefabObjectInInventoryOther, _plaseOfObjects.transform);
                    ObjectInInventoryOther.Instant(_otherPackInventory.Other[i]);
                }
            }
            else
            {
                _menu.OpenShop();
            }
        }

        //public void CoolingInInventory()
        //{
        //    Clear();
        //    for (int i = 0; i < _coolingPackInventory.Cooling.Count; i++)
        //    {
        //        ObjectInInventory objectInInventory = Instantiate(_prefabObjectInInventory, _plaseOfObjects.transform);
        //        objectInInventory.Instant(_coolingPackInventory.Cooling[i]);
        //    }
        //}

        //public void FarmInInventory()
        //{
        //    Clear();
        //    for (int i = 0; i < _farmPackInventory.Farms.Count; i++)
        //    {
        //        ObjectInInventory objectInInventory = Instantiate(_prefabObjectInInventory, _plaseOfObjects.transform);
        //        objectInInventory.Instant(_farmPackInventory.Farms[i]);
        //    }
        //}
    }
}