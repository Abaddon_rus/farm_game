﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class Menu : MonoBehaviour
    {
        [SerializeField]
        Button _openShop;
        [SerializeField]
        Button _openInventory;
        [SerializeField]
        Shop _shop;
        [SerializeField]
        Inventory _inventory;
        [SerializeField]
        GameObject _shopInventoryPanel;
        [SerializeField]
        Transform _bgButtons;

        private void Awake()
        {
            StaticStorage.AddLink("Menu", this);
            StaticStorage.AddLink("Shop", _shop);
            StaticStorage.AddLink("Inventory", _inventory);
        }

        private void Start()
        {
            _openShop.onClick.AddListener(OpenShop);
            _openInventory.onClick.AddListener(OpenInventory);
        }

        public void OpenInventory()
        {
            InventoryFocus();
            _inventory.Check();
        }

        public void OpenShop()
        {
            ShopFocus();
            _shop.Check();
        }

        public void ShopFocus()
        {
            _shop.gameObject.SetActive(true);
            _inventory.gameObject.SetActive(false);
            _bgButtons.localPosition = new Vector3(123.48f, 0, 0);
        }

        public void InventoryFocus()
        {
            _shop.gameObject.SetActive(false);
            _inventory.gameObject.SetActive(true);
            _bgButtons.localPosition = new Vector3(-123.48f, 0, 0);
        }

        public void OpenPanel()
        {
            StaticStorage.UpdateCore.Remove(StaticStorage.GetLink<GamePlay>("GamePlay"));
            _shopInventoryPanel.transform.localPosition = new Vector3(0, 0, 0);
        }

        public void ClosePanel()
        {
            StaticStorage.UpdateCore.Add(StaticStorage.GetLink<GamePlay>("GamePlay"));
            _shopInventoryPanel.transform.localPosition = new Vector3(720, 0, 0);
        }
    }
}