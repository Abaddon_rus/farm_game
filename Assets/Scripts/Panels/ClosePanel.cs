﻿using UnityEngine;
using UnityEngine.EventSystems;
using Game;

public class ClosePanel : MonoBehaviour, IPointerClickHandler
{
    GamePlay _gamePlay;

    private void Start()
    {
        _gamePlay = StaticStorage.GetLink<GamePlay>("GamePlay");
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        _gamePlay.ClosePanels();
    }
}
