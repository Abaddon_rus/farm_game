﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class DataCenterInPanel : MonoBehaviour
    {
        [SerializeField]
        Text _name;
        [SerializeField]
        Text _lvl;
        [SerializeField]
        Text _addiction;
        [SerializeField]
        Text _description;
        [SerializeField]
        Text _party;
        [SerializeField]   
        Button _buttonJoin;
        //[SerializeField]
        //Image _icon;

        private void OnEnable()
        {
            _buttonJoin.onClick.AddListener(Join);
        }

        private void OnDisable()
        {
            _buttonJoin.onClick.RemoveListener(Join);
        }

        private void Join()
        {
            
        }

        public void Insta(DataCenterJoinData dc)
        {
            _name.text = dc.Name;
            _lvl.text = dc.Lvl.ToString();
            _addiction.text = "Уровень игрока: " + dc.LvlAddiction + "    Коллличество ферм:" + dc.FarmAddiction + "    Площадь помещения:" + dc.RoomAddiction;
            _description.text = dc.Description;
            _party.text = dc.PartyCount + "/" + (20 + ((dc.Lvl - 1) * 5)).ToString();
        }
    }

    public class DataCenterJoinData
    {
        public string Name;
        public int Lvl;
        public int LvlAddiction;
        public int FarmAddiction;
        public int RoomAddiction;
        public string Description;
        public int PartyCount;
        public int Icon; 
    }
}