﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class IconInIconPanel : MonoBehaviour
    {
        [SerializeField]
        Image _icon;
        [SerializeField]
        TextMeshProUGUI _name;

        [SerializeField]
        Button _button;

        Icon _iconDate;

        int _iconId;

        Action<Sprite, int> _action;

        private void OnEnable()
        {
            _button.onClick.AddListener(Choose);
        }

        private void OnDisable()
        {
            _button.onClick.RemoveListener(Choose);
        }

        private void Choose()
        {
            _action(_icon.sprite, _iconId);
        }

        public void Insta(Icon icon, int id, Action<Sprite, int> action)
        {
            _action = action;
            _iconDate = icon;
            _iconId = id;
            _icon.sprite = _iconDate.Sprite;
            _name.text = _iconDate.Name;
        }
    }
}