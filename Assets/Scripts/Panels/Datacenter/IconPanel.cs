﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class IconPanel : MonoBehaviour
    {
        [SerializeField]
        IconInIconPanel _iconInIconPanel;

        [SerializeField]
        Transform _list;
        
        public IconPack _iconPack;

        [SerializeField]
        CreateDC _createDC;

        private void Start()
        {
            for (int i = 0; i < _iconPack.Icon.Count; i++)
            {
                Instantiate(_iconInIconPanel, _list).Insta(_iconPack.Icon[i], i, _createDC.ChangeIcon);
            }
        }

        public void OpenPanel()
        {
            gameObject.SetActive(true);
        }

        public void ClosePanel()
        {
            gameObject.SetActive(false);
        }
    }
}