﻿using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class DataCenter : MonoBehaviour
    {
        [SerializeField]
        JoinPanel _joinPanel;

        [SerializeField]
        DataCenterInfo _dataCenterInfo;

        [SerializeField]
        Text _nameTop;

        DCData _dCData;

        public DCData DCData
        {
            get
            {
                return _dCData;
            }
            set
            {
                _dCData = value;
                if (value != null)
                {
                    _nameTop.text = _dCData.Name;
                }
                else
                {
                    _nameTop.text = "";
                }
            }
        }

        private void Awake()
        {
            StaticStorage.AddLink("DataCenter", this);
        }

        public void OpenDataCenter()
        {
            if (_dCData == null)
            {
                _dataCenterInfo.ClosePanel();
                _joinPanel.OpenPanel();
            }
            else
            {
                _joinPanel.ClosePanel();
                _dataCenterInfo.ShowDC(_dCData);
            }
            gameObject.SetActive(true);
        }
    }
}