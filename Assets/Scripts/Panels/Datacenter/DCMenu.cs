﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class DCMenu : MonoBehaviour
    {
        [SerializeField]
        Button _buttonAccount;

        [SerializeField]
        Button _buttonStaff;

        [SerializeField]
        Button _buttonActivity;

        [SerializeField]
        Sprite _select;

        [SerializeField]
        GameObject _account;
        [SerializeField]
        GameObject _staff;
        [SerializeField]
        GameObject _activity;

        void OnEnable()
        {
            _buttonAccount.onClick.AddListener(OpenAccount);
            _buttonActivity.onClick.AddListener(OpenActivity);
            _buttonStaff.onClick.AddListener(OpenStaff);
        }

        void OpenStaff()
        {
            Select(_buttonStaff);
            SelectAndClose();
            _staff.SetActive(true);
        }

        void OpenActivity()
        {
            Select(_buttonActivity);
            SelectAndClose();
            _activity.SetActive(true);
        }

        void OpenAccount()
        {
            Select(_buttonAccount);
            SelectAndClose();
            _account.SetActive(true);
        }

        void Select(Button button)
        {
            _buttonAccount.image.sprite = null;
            _buttonAccount.image.color = new Color(0.09f, 0.1294f, 0.1568f);
            _buttonStaff.image.sprite = null;
            _buttonStaff.image.color = new Color(0.09f, 0.1294f, 0.1568f);
            _buttonActivity.image.sprite = null;
            _buttonActivity.image.color = new Color(0.09f, 0.1294f, 0.1568f);
            button.image.sprite = _select;
            button.image.color = Color.white;
        }

        void SelectAndClose()
        {
            _account.SetActive(false);
            _activity.SetActive(false);
            _staff.SetActive(false);
        }

    }
}