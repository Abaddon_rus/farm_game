﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class CreateDC : MonoBehaviour
    {
        DataCenter _dataCenter;

        int _iconID;

        [SerializeField]
        Image _icon;

        [SerializeField]
        TMP_InputField _title;

        [SerializeField]
        TextMeshProUGUI _textCrLeft;

        [SerializeField]
        Toggle _toggleOpen;

        [SerializeField]
        Toggle _toggleClose;

        [SerializeField]
        TMP_Dropdown _dropdownPlayerLvl;

        [SerializeField]
        TMP_Dropdown _dropdownDoneTask;

        [SerializeField]
        TMP_Dropdown _dropdownCollecring;

        [SerializeField]
        Slider _sliderTax;

        [SerializeField]
        TMP_InputField _description;

        [SerializeField]
        TextMeshProUGUI _textTexGold;

        [SerializeField]
        Button _buttonCreate;

        [SerializeField]
        Button _changeIcon;

        [SerializeField]
        Button _buttonClose;

        [SerializeField]
        IconPanel _iconPanel;

        int _tax;


        private void OnEnable()
        {
            _buttonCreate.onClick.AddListener(Create);
            _changeIcon.onClick.AddListener(OpenPanelIcons);
            _buttonClose.onClick.AddListener(ClosePanel);
            _title.onValueChanged.AddListener(ChangeName);
            _sliderTax.onValueChanged.AddListener(ChangeTex);
        }

        private void OnDisable()
        {
            _buttonCreate.onClick.RemoveListener(Create);
            _changeIcon.onClick.RemoveListener(OpenPanelIcons);
            _buttonClose.onClick.RemoveListener(ClosePanel);
            _title.onValueChanged.RemoveListener(ChangeName);
            _sliderTax.onValueChanged.RemoveListener(ChangeTex);
        }

        private void Start()
        {
            _dataCenter = StaticStorage.GetLink<DataCenter>("DataCenter");
        }

        public void EditDataCenter(DCData dcd)
        {
            _title.text = dcd.Name;
            _sliderTax.value = dcd.Tax;
            _toggleClose.isOn = dcd.Close;
            _description.text = dcd.Description;
            _dropdownPlayerLvl.value = dcd.AddictionLvl;
            _dropdownDoneTask.value = dcd.AddictionDoneTask;
            _dropdownCollecring.value = dcd.AddictionCollecting;
            _iconID = dcd.Icon;
            _icon.sprite = _iconPanel._iconPack.Icon[_iconID].Sprite;
        }

        private void ChangeTex(float value)
        {
            _tax = (int)value;
            _textTexGold.text = _tax + "%";
        }

        private void ChangeName(string value)
        {
            _textCrLeft.text = 17 - value.Length + " characters left";
        }

        private void OpenPanelIcons()
        {
            _iconPanel.OpenPanel();
        }

        public void OpenPanel()
        {
            gameObject.SetActive(true);
        }

        public void ClosePanel()
        {
            gameObject.SetActive(false);
        }

        public void ChangeIcon(Sprite icon, int id)
        {
            _icon.sprite = icon;
            _iconID = id;
            _iconPanel.ClosePanel();
        }

        private void Create()
        {
            if (_iconID != 0 && _title.text.Length > 0 && _toggleClose.isOn != _toggleOpen.isOn)
            {
                DataCenterPacket dcp = new DataCenterPacket()
                {
                    Name = _title.text,
                    Tax = _tax,
                    Close = _toggleClose.isOn,
                    Description = _description.text,
                    AddictionLvl = _dropdownPlayerLvl.value,
                    AddictionDoneTask = _dropdownDoneTask.value,
                    AddictionCollecting = _dropdownCollecring.value,
                    Icon = _iconID
                };
                NetworkCore.Send("CreateDataCenter", dcp, (data) =>
                {
                    DCData dcd = JsonUtility.FromJson<DCData>(data);
                    if (dcd != null)
                    {
                        _dataCenter.DCData = dcd;
                        _dataCenter.OpenDataCenter();
                        ClosePanel();
                    }
                });
            }
        }
    }

    public class DataCenterPacket : Packet
    {
        public string Name;
        public float Tax;
        public bool Close;
        public string Description;
        public int AddictionLvl;
        public int AddictionDoneTask;
        public int AddictionCollecting;
        public int Icon;
    }
}