﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class JoinPanel : MonoBehaviour
    {
        [SerializeField]
        Transform _list;

        [SerializeField]
        JoinDC _prefabJoinDC;

        [SerializeField]
        Button _buttonCreateDC;

        [SerializeField]
        CreateDC _panelCreateDC;

        private void OnEnable()
        {
            _buttonCreateDC.onClick.AddListener(OpenPanelCreatDataCenter);
        }

        private void OnDisable()
        {
            _buttonCreateDC.onClick.RemoveListener(OpenPanelCreatDataCenter);
        }

        void GetListDataCenter()
        {
            Clear();
            NetworkCore.Send("GetListDataCenter", new Packet(), (data) =>
            {
                DCDatas dCDatas = JsonUtility.FromJson<DCDatas>(data);
                for (int i = 0; i < dCDatas.DCData.Length; i++)
                {
                    JoinDC joinDC = Instantiate(_prefabJoinDC, _list);
                    joinDC.Insta(dCDatas.DCData[i]);
                }
            });
        }

        void Clear()
        {
            JoinDC[] joinDCs = GetComponentsInChildren<JoinDC>();
            for (int i = 0; i < joinDCs.Length; i++)
            {
                Destroy(joinDCs[i].gameObject);
            }
        }

        public void OpenPanel()
        {
            GetListDataCenter();
            gameObject.SetActive(true);
        }

        public void ClosePanel()
        {
            gameObject.SetActive(false);
        }

        void OpenPanelCreatDataCenter()
        {
            _panelCreateDC.OpenPanel();
        }
    }

    [Serializable]
    public class DCDatas : Packet
    {
        public DCData[] DCData;
    }
}