﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class Staff : MonoBehaviour
    {
        [SerializeField]
        StaffDC _prefabStaff;

        [SerializeField]
        TextMeshProUGUI _staff;
        [SerializeField]
        TextMeshProUGUI _kick;
        [SerializeField]
        TextMeshProUGUI _NameHost;
        [SerializeField]
        TextMeshProUGUI _countCoinsHost;
        [SerializeField]
        TextMeshProUGUI _doneTaskHost;
        [SerializeField]
        TextMeshProUGUI _lvlHost;
        [SerializeField]
        Image _icon;

        [SerializeField]
        DataCenterInfo _dataCenterInfo;

        private void Start()
        {
            LoadStaff();
        }

        void LoadStaff()
        {
            _kick.gameObject.SetActive(StaticStorage.Prifile.Name == _dataCenterInfo._DCData.NameHost);
            _staff.text = _dataCenterInfo._DCData.Staff + "/" + _dataCenterInfo._DCData.StaffMax;
            NetworkCore.Send("LoadStaffDataCenter", new DataPacket
            {
                v1 = StaticStorage.GetLink<DataCenter>("DataCenter").DCData.id
            }, (data) =>
            {
                StaffProfiles dataProfile = JsonUtility.FromJson<StaffProfiles>(data);
                if (dataProfile.DataProfile != null)
                {
                    LoadHost(dataProfile);
                    for (int i = 0; i < dataProfile.DataProfile.Count; i++)
                    {
                        Instantiate(_prefabStaff, transform).Insta(dataProfile.DataProfile[i], i + 2);
                    }
                }
            });
        }

        void LoadHost(StaffProfiles staffProfiles)
        {
            string nameHost = _dataCenterInfo._DCData.NameHost;
            _NameHost.text = nameHost;
            DataProfile dataProfile = staffProfiles.DataProfile.Find(x => x.Name == nameHost);
            _icon.sprite = StaticStorage.GetLink<IconPack>("IconPack").Icon[1].Sprite;
            _countCoinsHost.text = dataProfile.CountCoinsDay.ToString();
            _doneTaskHost.text = dataProfile.TaskDoneDay.ToString();
            _lvlHost.text = dataProfile.Level.ToString();
            staffProfiles.DataProfile.FindAndRemove(x => x.Name == nameHost);
        }

    }

    public class StaffProfiles : Packet
    {
        public List<DataProfile> DataProfile;
    }
}