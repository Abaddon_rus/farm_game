﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class JoinDC : MonoBehaviour
    {
        [SerializeField]
        Image _icon;
        [SerializeField]
        TextMeshProUGUI _lvl;
        [SerializeField]
        TextMeshProUGUI _name;
        [SerializeField]
        TextMeshProUGUI _tax;
        [SerializeField]
        TextMeshProUGUI _staff;
        [SerializeField]
        TextMeshProUGUI _nameHost;
        [SerializeField]
        TextMeshProUGUI _description;
        [SerializeField]
        Button _buttonJoin;

        [SerializeField]
        Image _addict1;
        [SerializeField]
        Image _addict2;
        [SerializeField]
        Image _addict3;

        DCData _DCData;

        DataCenter _dataCenter;

        private void OnEnable()
        {
            _buttonJoin.onClick.AddListener(JoinToDataCenter);
        }

        private void OnDisable()
        {
            _buttonJoin.onClick.RemoveListener(JoinToDataCenter);
        }

        private void Start()
        {
            _dataCenter = StaticStorage.GetLink<DataCenter>("DataCenter");
        }

        public void Insta(DCData dcdata)
        {
            _DCData = dcdata;
            List<Icon> icons = StaticStorage.GetLink<IconPack>("IconPack").Icon;
            _icon.sprite = icons[Mathf.Clamp(_DCData.Icon, 0, icons.Count)].Sprite;
            _name.text = _DCData.Name;
            _nameHost.text = _DCData.NameHost;
            _tax.text = $"{_DCData.Tax}%";
            _staff.text = $"{_DCData.Staff}/{_DCData.StaffMax}";
            _description.text = _DCData.Description;
            _lvl.text = _DCData.Lvl.ToString();
            if (_DCData.AddictionLvl > 0)
            {
                _addict1.gameObject.SetActive(true);
            }
            if (_DCData.AddictionDoneTask > 0)
            {
                _addict2.gameObject.SetActive(true);
            }
            if (_DCData.AddictionCollecting > 0)
            {
                _addict3.gameObject.SetActive(true);
            }
        }

        void JoinToDataCenter()
        {
            NetworkCore.Send("JoinToDataCenter", new DataPacket()
            {
                v1 = _DCData.id
            }, (data) =>
            {
                if (Convert.ToBoolean(data))
                {
                    _DCData.Staff++;
                    _dataCenter.DCData = _DCData;
                    _dataCenter.OpenDataCenter();
                }
            });
        }
    }

    [Serializable]
    public class DCData : Packet
    {
        public int id;
        public string Name;
        public string NameHost;
        public float Tax;
        public int Staff;
        public int StaffMax;
        public int TimeStart;
        public bool Close;
        public string Description;
        public int AddictionLvl;
        public int AddictionDoneTask;
        public int AddictionCollecting;
        public int Lvl;
        public int Icon;
    }
}