﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class DataCenterInfo : MonoBehaviour
    {
        [SerializeField]
        DataCenter _dataCenter;

        [SerializeField]
        TextMeshProUGUI _name;

        [SerializeField]
        TextMeshProUGUI _nameHost;

        [SerializeField]
        TextMeshProUGUI _staff;

        [SerializeField]
        TextMeshProUGUI _lvl;

        [SerializeField]
        TextMeshProUGUI _tax;

        [SerializeField]
        TextMeshProUGUI _description;

        [SerializeField]
        TextMeshProUGUI _taskName;

        [SerializeField]
        TextMeshProUGUI _levelTask;

        [SerializeField]
        TextMeshProUGUI _countTask;

        [SerializeField]
        TextMeshProUGUI _timeLeftTask;

        [SerializeField]
        Button _buttonEdit;

        [SerializeField]
        Image _icon;

        [SerializeField]
        Image _fillTask;

        [SerializeField]
        Image _add1;

        [SerializeField]
        Image _add2;

        [SerializeField]
        Image _add3;

        //[SerializeField]
        //Image _boost1;

        //[SerializeField]
        //Image _boost2;

        //[SerializeField]
        //Image _boost3;

        [SerializeField]
        Button _buttonLeave;

        [SerializeField]
        IconPack _iconPack;

        [SerializeField]
        CreateDC _editPanel;

        [SerializeField]
        Transform _botInfo; 

        public DCData _DCData; 

        private void OnEnable()
        {
            _buttonEdit.gameObject.SetActive(StaticStorage.Prifile.Name == _DCData.NameHost);
            _buttonEdit.onClick.AddListener(OpenEditPanel);
            _buttonLeave.gameObject.SetActive(!(StaticStorage.Prifile.Name == _DCData.NameHost));
            _buttonLeave.onClick.AddListener(Leave);
        }

        private void OnDisable()
        {
            _buttonEdit.onClick.RemoveListener(OpenEditPanel);
            _buttonLeave.onClick.RemoveListener(Leave);
        }

        private void Awake()
        {
            StaticStorage.AddLink("DataCenterInfo", this);
        }

        private void Update()
        {
            if (Input.GetAxis("Mouse ScrollWheel") > 0f)
            {
                StopAllCoroutines();
                StartCoroutine(Down());
            }
            else if (Input.GetAxis("Mouse ScrollWheel") < 0f)
            {
                StopAllCoroutines();
                StartCoroutine(Up());
            }
        }

        IEnumerator Up()
        {
            while (_botInfo.localPosition.y < 0)
            {
                _botInfo.localPosition = new Vector3(0, Mathf.Clamp(40 + _botInfo.localPosition.y, -592, -60), 0);
                yield return null;
            }
        }

        IEnumerator Down()
        {
            while (_botInfo.localPosition.y > -1230)
            {
                _botInfo.localPosition = new Vector3(0, Mathf.Clamp(-40 + _botInfo.localPosition.y, -592, -60), 0);
                yield return null;
            }
        }

        public void ShowDC(DCData data)
        {
            _DCData = data;
            _icon.sprite = _iconPack.Icon[_DCData.Icon].Sprite;
            _name.text = _DCData.Name;
            _nameHost.text = string.Format($"Host: { _DCData.NameHost}");
            _tax.text = string.Format($"Tax: {_DCData.Tax}%");
            _staff.text = string.Format($"{_DCData.Staff}/{_DCData.StaffMax}");
            _description.text = string.Format($"Description: {_DCData.Description}");
            _lvl.text = string.Format($"Level: {_DCData.Lvl}");
            if (_DCData.AddictionLvl > 0)
            {
                _add1.gameObject.SetActive(true);
            }
            if (_DCData.AddictionDoneTask > 0)
            {
                _add2.gameObject.SetActive(true);
            }
            if (_DCData.AddictionCollecting > 0)
            {
                _add3.gameObject.SetActive(true);
            }
            _taskName.text = "";
            _levelTask.text = "";
            _countTask.text = "";
            _timeLeftTask.text = "";
            _fillTask.fillAmount = 0;
            OpenPanel();
        }

        public void OpenEditPanel()
        {
            _editPanel.EditDataCenter(_DCData);
            _editPanel.OpenPanel();
        }

        private void Leave()
        {
            NetworkCore.Send("LeaveDataCenter", new Packet(), (data) =>
            {
                if (Convert.ToBoolean(data))
                {
                    _dataCenter.DCData = null;
                    _dataCenter.OpenDataCenter();
                }
            });
        }

        public void OpenPanel()
        {
            gameObject.SetActive(true);
        }

        public void ClosePanel()
        {
            gameObject.SetActive(false);
        }
    }
}