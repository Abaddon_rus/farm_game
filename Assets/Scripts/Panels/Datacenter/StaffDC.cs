﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class StaffDC : MonoBehaviour
    {
        [SerializeField]
        Image _icon;
        [SerializeField]
        TextMeshProUGUI _name;
        [SerializeField]
        TextMeshProUGUI _countCoins;
        [SerializeField]
        TextMeshProUGUI _TaskDone;
        [SerializeField]
        TextMeshProUGUI _lvl;

        [SerializeField]
        Button _buttonKick;

        [SerializeField]
        TextMeshProUGUI _rang;

        int id;

        private void OnEnable()
        {
            _buttonKick.onClick.AddListener(Kick);
        }
        private void OnDisable()
        {
            _buttonKick.onClick.AddListener(Kick);
        }

        private void Kick()
        {
            NetworkCore.Send("KickDataCenter", new DataPacket
            {
                v1 = id
            }, (data) =>
            {
                if (Convert.ToBoolean(data))
                {
                    Destroy(gameObject);
                    StaticStorage.GetLink<DataCenter>("DataCenter").DCData.Staff--;
                }
            });
        }

        public void Insta(DataProfile dataProfile, int rang)
        {
            _buttonKick.gameObject.SetActive(StaticStorage.Prifile.Name == StaticStorage.GetLink<DataCenterInfo>("DataCenterInfo")._DCData.NameHost);
            _rang.text = rang.ToString();
            id = dataProfile.id;
            _icon.sprite = StaticStorage.GetLink<IconPack>("IconPack").Icon[0].Sprite;
            _name.text = dataProfile.Name;
            _countCoins.text = dataProfile.CountCoinsDay.ToString();
            _TaskDone.text = dataProfile.TaskDoneDay.ToString();
            _lvl.text = dataProfile.Level.ToString();
        }
    }
}