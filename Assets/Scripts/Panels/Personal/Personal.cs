﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class Personal : MonoBehaviour
    {
        public bool _freePlace = true;
        public int _reboots;

        [SerializeField]
        Image _image;
        [SerializeField]
        Sprite _iconFree;
        [SerializeField]
        Sprite _iconBusy;
        [SerializeField]
        GameObject _interface;

        [SerializeField]
        Button _button;
        [SerializeField]
        Button _buttonBuy;
        [SerializeField]
        Button _buttonClose;
        [SerializeField]
        GameObject _massegBuyPersonal;

        [SerializeField]
        Button _buttonAddReboots;
        [SerializeField]
        Button _buttonRemoveReboots;
        [SerializeField]
        Text _rebootsTxt;
        [SerializeField]
        Button _buttonRemovePersonal;

        [SerializeField]
        PersonalData _data;

        private Coin _coin = StaticStorage.GetLink<Coin>("Coin");

        public PersonalData Data
        {
            set
            {
                _freePlace = value.FreePlace;
                _reboots = value.Reboots;
                if (!value.FreePlace)
                {
                    _rebootsTxt.text = _reboots.ToString();
                    _image.sprite = _iconBusy;
                    _interface.SetActive(true);
                }

            }
            get
            {
                _data.FreePlace = _freePlace;
                _data.Reboots = _reboots;

                return _data;
            }
        }

        private void Awake()
        {
            StaticStorage.AddLink("Personal", this);
        }

        private void OnEnable()
        {
            _buttonBuy.onClick.AddListener(AddPersonal);
            _button.onClick.AddListener(OpenMasseg);
            _buttonAddReboots.onClick.AddListener(AddReboots);
            _buttonRemoveReboots.onClick.AddListener(RemoveReboots);
            _buttonRemovePersonal.onClick.AddListener(RemovePersonal);
            _buttonClose.onClick.AddListener(Close);
        }

        private void Start()
        {
            _rebootsTxt.text = _reboots.ToString();
        }

        private void AddReboots()
        {
            if (_coin.COIN >= 1)
            {
                _coin.COIN -= 1;
                _reboots += 5;
                _rebootsTxt.text = _reboots.ToString();
            }
        }

        private void RemoveReboots()
        {
            if (_reboots >= 5)
            {
                _reboots -= 5;
                _coin.COIN += 1;
                _rebootsTxt.text = _reboots.ToString();
            }
        }

        private void OpenMasseg()
        {
            if (_freePlace)
            {
                _massegBuyPersonal.SetActive(true);
            }
            else
            {

            }
        }

        private void AddPersonal()
        {
            if (_freePlace && _coin.COIN >= 5)
            {
                _coin.COIN -= 5;
                _freePlace = false;
                _rebootsTxt.text = _reboots.ToString();
                _image.sprite = _iconBusy;
                _interface.SetActive(true);
                _massegBuyPersonal.SetActive(false);
            }
        }

        void RemovePersonal()
        {
            _freePlace = true;
            _interface.SetActive(false);
            _image.sprite = _iconFree;
            _reboots = 0;
        }

        void Close()
        {
            _massegBuyPersonal.SetActive(false);
        }
    }

    [Serializable]
    public class PersonalData
    {
        public bool FreePlace;
        public int Reboots;
    }
}