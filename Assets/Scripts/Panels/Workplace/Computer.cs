﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class Computer : MonoBehaviour
    {
        [SerializeField]
        Coin _coin;

        [SerializeField]
        List<BoostComputerPanel> _boostComputerPanels;

        [SerializeField]
        Button _buttonUp;

        public Buff _buffPack;

        public InfoBuff _infoBuff;

        public Dictionary<string, int> _lvl = new Dictionary<string, int>();


        int _min = 0;
        int _max;
        int _lvlBoost;
        int _value;
        int Value
        {
            set
            {
                if (value > _max)
                {
                    _value = _min;
                    _lvlBoost++;
                }
                else
                {
                    _value = value;
                }
            }
            get { return _value; }
        }

        int _sum;

        private void OnEnable()
        {
            _buttonUp.onClick.AddListener(Upgrade);
            _buttonUp.interactable = _coin.COIN >= 1 && _sum < _buffPack.BuffWorkplace.Count * 10;
        }

        private void OnDisable()
        {
            _buttonUp.onClick.RemoveListener(Upgrade);
        }

        public void StartComputer()
        {
            for (int i = 0; i < _buffPack.BuffWorkplace.Count; i++)
            {
                _lvl.Add(_buffPack.BuffWorkplace[i].ID, 0);
            }
        }

        public void LoadComputer(WorkPlacePack wp)
        {
            _lvl.Add("MONITOR", wp.Monitor);
            _lvl.Add("CHAIR", wp.Chair);
            _lvl.Add("TABLE", wp.Table);
            _lvl.Add("KEYBOARD", wp.Keyboard);
            _sum = wp.Monitor + wp.Chair + wp.Keyboard + wp.Table;
            _max = _buffPack.BuffWorkplace.Count - 1;
            for (int i = 0; i < _boostComputerPanels.Count; i++)
            {
                _boostComputerPanels[i]._buffWP = _buffPack.BuffWorkplace[i];
            }
            if (_sum == 0)
            {
                BuffWP buffWP = _buffPack.BuffWorkplace[0];
                _boostComputerPanels[0].Next(1);
            }
            for (int i = 0; i < _sum; i++)
            {
                UpgradeButton(i);
            }
            _buttonUp.interactable = _coin.COIN >= 1 && _sum < _buffPack.BuffWorkplace.Count * 10;
        }

        public ButtonBuffComputer EnterButton
        {
            set
            {
                string name = value._buffWPEnum.ToString();
                _infoBuff.Show(_buffPack.GetBuff(name), _lvl[name]);
            }
        }

        void Upgrade()
        {
            string nboost = _buffPack.BuffWorkplace[_value].ID;
            List<BuffParameter> buffParameter = _buffPack.GetBuff(nboost).Parameters;
            if (buffParameter.Count > _lvl[nboost] && _buffPack.GetBuff(nboost).Price <= _coin.COIN)
            {
                if (buffParameter[_lvl[nboost]].PlayerLvl <= PlayerLevel.Lvl)
                {
                    string addiction = buffParameter[_lvl[nboost]].Addiction.ToString();
                    if (addiction == "Ничего" || _lvl[addiction] == buffParameter[_lvl[nboost]].AddictionLvl)
                    {
                        NetworkCore.Send("UpBoost", new DataPacket
                        {
                            s1 = nboost.ToLower(),
                            v1 = _lvl[nboost] + 1
                        }, (data) =>
                        {
                            if (Convert.ToBoolean(data))
                            {
                                _coin.COIN -= _buffPack.GetBuff(nboost).Price;
                                _lvl[nboost]++;
                                UpgradeButton(_sum);
                                _sum++;
                                _buttonUp.interactable = _coin.COIN >= 1 && _sum < _buffPack.BuffWorkplace.Count * 10;
                                PlayerLevel.Exp = 5;
                            }
                        });
                    }
                }
            }
        }

        void UpgradeButton(int sum)
        {
            Value = sum % _buffPack.BuffWorkplace.Count;
            _lvlBoost = (sum / _buffPack.BuffWorkplace.Count) + 1;
            if (_sum <= _buffPack.BuffWorkplace.Count * 10)
            {
                _boostComputerPanels[_value].Upgrade(_lvlBoost);
                Value += 1;
            }
            if (_sum < _buffPack.BuffWorkplace.Count * 10)
            {
                _boostComputerPanels[_value].Next(_lvlBoost);
            }
        }

        public float GetParametr(string buff)
        {
            return _buffPack.GetBuff(buff).Parametr;
        }
    }
}