﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Game
{
    public class ButtonBuffComputer : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField]
        Computer _computer;

        public BuffWPEnum _buffWPEnum;

        public void OnPointerEnter(PointerEventData eventData)
        {
            _computer._infoBuff.Activ(true);
            _computer.EnterButton = this;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            _computer._infoBuff.Activ(false);
        }
    }
}