﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class Works : MonoBehaviour
    {
        public Workplace _workplace;
        public Computer _computer;
        public PlayerLevel _playerLevel;
        public Coin _coin;
        public CoinsIncome _coinsIncome;
        public GameObject _tasksPanel;
        [SerializeField]
        WorkplaceManager _workplaceManager;
        [SerializeField]
        WorkingTimer _workingTimer;
        [SerializeField]
        Transform _taskPlace;
        [SerializeField]
        TaskInPanel _taskInPanelPrefab;
        [SerializeField]
        License _license;


        [SerializeField]
        TextMeshProUGUI _pageTxt;
        [SerializeField]
        Button _buttonLeft;
        [SerializeField]
        Button _buttonRight;

        public List<Sprite> _frames;
        public List<Sprite> _icons;

        List<DataTask> _dataTasks = new List<DataTask>();

        public List<DataTask> DataTask
        {
            set
            {
                var i = value.OrderBy(x => x.order);
                _dataTasks.AddRange(i);
                if (_workplaceManager._busyTask != null)
                {
                    _workplaceManager.StopCoroutine(_workplaceManager._busyTask);
                }
                _workplaceManager._busyTask = _workplaceManager.StartCoroutine(BusyTask());
            }
            get
            {
                return _dataTasks;
            }
        }

        int _page;

        public int Page
        {
            set
            {
                _page = value;
                OpenPage();
            }
            get
            {
                return _page;
            }
        }

        private void OnEnable()
        {
            if (_dataTasks.Count > 0)
            {
                OpenPage();
            }
            _buttonLeft.onClick.AddListener(NextPage);
            _buttonRight.onClick.AddListener(PreviousPage);
        }

        private void OnDisable()
        {
            Clear();
            _buttonLeft.onClick.RemoveListener(NextPage);
            _buttonRight.onClick.RemoveListener(PreviousPage);
        }

        private void Start()
        {
            CheckLicense();
        }

        void Clear()
        {
            TaskInPanel[] taskInPanels = GetComponentsInChildren<TaskInPanel>();
            for (int i = 0; i < taskInPanels.Length; i++)
            {
                Destroy(taskInPanels[i].gameObject);
            }
        }

        void OpenPage()
        {
            Clear();
            _pageTxt.text = _page + "/" + (_dataTasks.Count / 8);
            int i = _page * 8;
            int x = Mathf.Clamp(((_page + 1) * 8), 8, _dataTasks.Count);
            while (i < x)
            {
                TaskInPanel taskInPanel = Instantiate(_taskInPanelPrefab, _taskPlace);
                taskInPanel._works = this;
                taskInPanel._computer = _computer;
                taskInPanel.DataTask = _dataTasks[i];
                i++;
            }
        }

        public void CheckLicense()
        {
            NetworkCore.Send("CheckLicense", new Packet(), (data) =>
            {
                _license.LvlLicense = System.Convert.ToInt32(data);
            });
        }

        public void Working(DataTask dataTask)
        {
            _workingTimer._dataTask = dataTask;
            if (dataTask.Done)
            {
                if (_workplaceManager._workingCorutine != null)
                {
                    _workplaceManager.StopCoroutine(_workplaceManager._workingCorutine);
                }
                _workplaceManager._workingCorutine = _workplaceManager.StartCoroutine(_workingTimer.Relax());
            }
            else
            {
                if (_workplaceManager._workingCorutine != null)
                {
                    _workplaceManager.StopCoroutine(_workplaceManager._workingCorutine);
                }
                _workplaceManager._workingCorutine = _workplaceManager.StartCoroutine(_workingTimer.Working());
            }
            _tasksPanel.SetActive(false);
        }

        private void PreviousPage()
        {
            Page = Mathf.Clamp(_page + 1, 1, _dataTasks.Count / 8);
        }

        private void NextPage()
        {
            Page = Mathf.Clamp(_page - 1, 1, _dataTasks.Count / 8);
        }

        public void Generator()
        {
            if (_workplace._haveComputer && _license.LvlLicense > 0)
            {
                NetworkCore.Send("GetTasks", new Packet(), (data) =>
                {
                    List<DataTask> dataTasks = new List<DataTask>();
                    dataTasks.AddRange(JsonUtility.FromJson<DataTasks>(data).Tasks);
                    DataTask = dataTasks;
                });
            }
        }

        IEnumerator BusyTask()
        {
            List<DataTask> dataTasksLvl3 = _dataTasks.FindAll(x => x.Lvl == 3);
            for (int i = 0; i < dataTasksLvl3.Count; i++)
            {
                int rnd = Random.Range(0, dataTasksLvl3.Count);
                dataTasksLvl3[rnd].Busy = true;
                dataTasksLvl3.Remove(dataTasksLvl3[rnd]);
                yield return new WaitForSecondsRealtime(2);
            }
            List<DataTask> dataTasksLvl2 = _dataTasks.FindAll(x => x.Lvl == 2);
            for (int i = 0; i < dataTasksLvl2.Count; i++)
            {
                int rnd = Random.Range(0, dataTasksLvl2.Count);
                dataTasksLvl2[rnd].Busy = true;
                dataTasksLvl2.Remove(dataTasksLvl2[rnd]);
                yield return new WaitForSecondsRealtime(2);
            }
            List<DataTask> dataTasksLvl1 = _dataTasks.FindAll(x => x.Lvl == 1);
            for (int i = 0; i < dataTasksLvl1.Count; i++)
            {
                int rnd = Random.Range(0, dataTasksLvl1.Count);
                dataTasksLvl1[rnd].Busy = true;
                dataTasksLvl1.Remove(dataTasksLvl1[rnd]);
                yield return new WaitForSecondsRealtime(2);
            }
            yield return null;
            Generator();
        }
    }
}