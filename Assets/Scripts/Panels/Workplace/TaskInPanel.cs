﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class TaskInPanel : MonoBehaviour
    {
        public Works _works;
        public Computer _computer;

        [SerializeField]
        Image _icon;
        [SerializeField]
        Image _ramka;
        [SerializeField]
        Text _name;
        [SerializeField]
        Text _price;
        [SerializeField]
        Text _deposit;
        [SerializeField]
        TextMeshProUGUI _depositTxt;
        [SerializeField]
        Text _time;
        //[SerializeField]
        //Text _timeBuff;
        [SerializeField]
        Text _lvl;
        [SerializeField]
        Button _button;
        [SerializeField]
        GameObject _busy;

        Image _bg;

        DataTask _dataTask;
        int _timerBuff;

        public bool _taskWorking = false;

        public DataTask DataTask
        {
            set
            {
                _dataTask = value;
                _bg = GetComponent<Image>();
                Insta();
            }
            get
            {
                return _dataTask;
            }
        }

        private void OnEnable()
        {
            _button.onClick.AddListener(Working);
        }

        private void OnDisable()
        {
            _button.onClick.RemoveListener(Working);
        }

        public void Busy()
        {
            if (_busy != null)
            {
                _busy.gameObject.SetActive(true);
            }
        }

        private void Working()
        {
            if (_works._coin.COIN >= _dataTask.Deposit)
            {
                NetworkCore.Send("GetTask", new DataPacket
                {
                    v1 = _dataTask.id
                }, (data) =>
                {
                    if (Convert.ToBoolean(data))
                    {
                        _works._coin.COIN -= _dataTask.Deposit;
                        _dataTask.Timer -= _timerBuff;
                        if (_dataTask.Rare)
                        {
                            _dataTask.Price += _dataTask.Price / 2;
                        }
                        _works.Working(_dataTask);
                        _taskWorking = true;
                        Destroy(gameObject);
                    }
                });

            }
        }

        public void Insta()
        {
            if (_computer._lvl["MONITOR"] != 0)
            {
                _timerBuff = (int)(_dataTask.Timer * (_computer._buffPack.GetBuff("MONITOR").Parametr * _computer._lvl["MONITOR"]) / 100);
            }
            if (_dataTask.Rare)
            {
                _bg.color = new Color(0.9f, 0, 1, 1);
            }
            _icon.sprite = _works._icons[_dataTask.Icon];
            _ramka.sprite = _works._frames[_dataTask.Lvl - 1];
            _name.text = _dataTask.Name;
            _price.text = (Math.Floor(_dataTask.Price * 10000) / 10000).ToString() + "$";
            if (_dataTask.Deposit != 0)
            {
                _deposit.text = (Math.Floor(_dataTask.Deposit * 10000) / 10000).ToString() + "$";
            }
            else
            {
                _deposit.gameObject.SetActive(false);
                _depositTxt.gameObject.SetActive(false);
            }
            _time.text = _dataTask.Timer.ToTime();
            _lvl.text = "УРОВЕНЬ " + _dataTask.Lvl;
            _busy.gameObject.SetActive(_dataTask.Busy);
        }
    }

    [Serializable]
    public class DataTask : Packet
    {
        public int id;
        public string Name;
        public int Icon;
        public float Price;
        public int Timer;
        public float Deposit;
        public int Progress;
        public float ChanceDefault;
        public int TimerRelax;
        public bool Rare = false;
        public int Lvl;
        public bool Done;
        public bool Busy = false;
        public string order;
    }

    [Serializable]
    public class DataTasks : Packet
    {
        public DataTask[] Tasks;
    }
}