﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class InfoBuff : MonoBehaviour
    {
        [SerializeField]
        Computer _computer;
        [SerializeField]
        Text _name;
        [SerializeField]
        Text _description;
        [SerializeField]
        Text _addiction;
        [SerializeField]
        Text _price;
        [SerializeField]
        Text _buff;
        [SerializeField]
        Text _buffUP;

        public void Show(BuffWP buffWP, int lvl)
        {
            if (lvl > 0)
            {
                _name.text = buffWP.Name + " " + lvl + " уровень";
            }
            else
            {
                _name.text = buffWP.Name + " : Не куплено";
            }
            _description.text = buffWP.Description;
            if (buffWP.Parameters.Count >= lvl + 1)
            {
                BuffParameter buff = buffWP.Parameters[lvl];

                string addication = "";
                if (buff.AddictionLvl != 0)
                { 
                    addication += _computer._buffPack.GetBuff(buff.Addiction.ToString()).Name + " " + buff.AddictionLvl + " уровня";
                }
                if (buff.PlayerLvl != 0)
                {
                    addication += "\n Игрок " + buff.PlayerLvl + " уровня";
                }
                _addiction.text = addication;
                if (lvl != 0)
                {
                    _buff.text = "+" + buffWP.Parametr * (lvl) + "%";
                }
                else
                {
                    _buff.text = "+0%";
                }
                _buffUP.text = "+" + buffWP.Parametr * (lvl + 1) + "%";
                _price.text = string.Format("{0:0.00}", buffWP.Price) + "$";
            }
            else
            {
                _price.text = "";
                _buff.text = "+" + buffWP.Parametr * lvl + "%";
                _buffUP.text = "MAX";
            }
        }

        public void Activ(bool i)
        {
            gameObject.SetActive(i);
        }
    }
}