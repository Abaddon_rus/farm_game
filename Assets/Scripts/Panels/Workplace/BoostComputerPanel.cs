﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class BoostComputerPanel : MonoBehaviour
    {
        [SerializeField]
        List<Image> _lvls;

        [SerializeField]
        TextMeshProUGUI _boost;

        [SerializeField]
        TextMeshProUGUI _upgrade;

        public BuffWP _buffWP;

        public void Upgrade(int lvl)
        {
            for (int i = 0; i < lvl; i++)
            {
                _lvls[i].color = Color.green;
            }
            _boost.text = _buffWP.Parametr * lvl + _buffWP.StartParametr + "%";
            _upgrade.text = "";
        }

        public void Next(int lvl)
        {
            float i = _buffWP.Parametr;
            if (lvl == 1)
            {
                i += _buffWP.StartParametr;
            }
            _lvls[lvl - 1].color = Color.blue;
            _upgrade.text = "+" + i + "%";
        }
    }
}