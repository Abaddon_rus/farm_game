﻿using UnityEngine;
using UnityEngine.UI;

public class TimerCD : MonoBehaviour
{
    public Text _text;
    [SerializeField]
    Text _name;

    private void OnEnable()
    {
        _name.gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        _name.gameObject.SetActive(true);
    }
}
