﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class Workplace : MonoBehaviour
    {
        [SerializeField]
        Coin _coin;
        [SerializeField]
        Button _buttonComputer;
        [SerializeField]
        Button _buttonWorks;
        [SerializeField]
        Button _buttonBuff;

        [SerializeField]
        Computer _computer;
        [SerializeField]
        Works _works;
        [SerializeField]
        GameObject _buff;
        [SerializeField]
        License _license;
        [SerializeField]
        BuffWorks _buffWorks;
        [SerializeField]
        BuffMenu _buffMenu;

        [SerializeField]
        GameObject _lock;
        [SerializeField]
        Button _buyComputer;
        [SerializeField]
        float _priceComputer = 5;
        public bool _haveComputer = false;

        DataTask _TaskInWork = null;

        public void LoadWorkPlace()
        {
            NetworkCore.Send("CheckWorkplace", new Packet(), (data) =>
            {
                WorkPlacePack wp = JsonUtility.FromJson<WorkPlacePack>(data);
                if (wp != null)
                {
                    _lock.gameObject.SetActive(false);
                    _haveComputer = true;
                    _computer.LoadComputer(wp);
                    _buffWorks.LoadBuff(wp);
                    _license.LvlLicense = wp.LicenseLvl;
                    _buffMenu.CheckCDAll(wp);

                    if (_license.LvlLicense > 0)
                    {
                        NetworkCore.Send("CheckTask", new Packet(), (dataTask) =>
                        {
                            _TaskInWork = JsonUtility.FromJson<DataTask>(dataTask);
                            if (_TaskInWork != null && _TaskInWork.id != 0)
                            {
                                _works.Working(_TaskInWork);
                            }
                        });
                    }
                    else
                    {
                        _license.gameObject.SetActive(true);
                    }
                }
                else
                {
                    _lock.gameObject.SetActive(true);
                    _computer.StartComputer();
                    _buffWorks.StartBuffWorks();
                }
            });
        }

        private void Update()
        {
            if (Input.GetAxis("Mouse ScrollWheel") > 0f)
            {
                StopAllCoroutines();
                StartCoroutine(Down());
            }
            else if (Input.GetAxis("Mouse ScrollWheel") < 0f)
            {
                StopAllCoroutines();
                StartCoroutine(Up());
            }
        }

        IEnumerator Up()
        {
            while (_works.transform.localPosition.y < 0)
            {
                _works.transform.localPosition = new Vector3(-61, Mathf.Clamp(40 + _works.transform.localPosition.y, -716, 0), 0);
                yield return null;
            }
        }

        IEnumerator Down()
        {
            while (_works.transform.localPosition.y > -1230)
            {
                _works.transform.localPosition = new Vector3(-61, Mathf.Clamp(-40 + _works.transform.localPosition.y, -716, 0), 0);
                yield return null;
            }
        }

        private void OnEnable()
        {
            _buttonComputer.onClick.AddListener(OpenComputer);
            _buttonWorks.onClick.AddListener(OpenWorks);
            _buttonBuff.onClick.AddListener(OpenBuff);
            _buyComputer.onClick.AddListener(BuyComputer);
        }

        private void OnDisable()
        {
            _buttonComputer.onClick.RemoveListener(OpenComputer);
            _buttonWorks.onClick.RemoveListener(OpenWorks);
            _buttonBuff.onClick.RemoveListener(OpenBuff);
            _buyComputer.onClick.RemoveListener(BuyComputer);
        }

        private void BuyComputer()
        {
            if (_coin.COIN >= _priceComputer && PlayerLevel.Lvl >= 3)
            {
                NetworkCore.Send("BuyWorkplace", new Packet(), (data) =>
                {
                    if (Convert.ToBoolean(data))
                    {
                        _coin.COIN -= _priceComputer;
                        _lock.gameObject.SetActive(false);
                        _haveComputer = true;
                    }
                });
            }
        }

        void Close()
        {
            _computer.gameObject.SetActive(false);
            _works.gameObject.SetActive(false);
            _buff.SetActive(false);
            ButtonReset();
        }

        void ButtonReset()
        {
            _buttonComputer.image.color = new Color(0, 0.7960785f, 1);
            _buttonWorks.image.color = new Color(0, 0.7960785f, 1);
            _buttonBuff.image.color = new Color(0, 0.7960785f, 1);
        }

        private void OpenBuff()
        {
            Close();
            _buff.SetActive(true);
            _buttonBuff.image.color = new Color(1, 1, 1);
        }

        private void OpenWorks()
        {
            Close();
            _works.gameObject.SetActive(true);
            _buttonWorks.image.color = new Color(1, 1, 1);
        }

        private void OpenComputer()
        {
            Close();
            _computer.gameObject.SetActive(true);
            _works.gameObject.SetActive(true);
            _buttonComputer.image.color = new Color(1, 1, 1);
        }
    }

    public class WorkPlacePack : Packs
    {
        public int LicenseLvl;
        public int Monitor;
        public int Chair;
        public int Keyboard;
        public int Table;
        public int FastRelax;
        public int FlashRelax;
        public int FastTask;
        public int FlashTask;
        public int ExpensiveTask;
        public int DoubleTask;
        public int LuckyTask;
        public int BestTask;
        public int TableCD;
        public int FastRelaxCD;
        public int FlashRelaxCD;
        public int FastTaskCD;
        public int FlashTaskCD;
        public int ExpensiveTaskCD;
        public int DoubleTaskCD;
        public int LuckyTaskCD;
        public int BestTaskCD;
    }
}