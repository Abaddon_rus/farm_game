﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class BuffWorks : MonoBehaviour
    {
        Coin _coin;

        [SerializeField]
        ButtonUpBuffW _prefab;

        public TextMeshProUGUI _name;
        public TextMeshProUGUI _dexcription;
        public TextMeshProUGUI _parametrs;
        public TextMeshProUGUI _addiction;
        public TextMeshProUGUI _price;

        [SerializeField]
        Transform _grid;

        public Sprite _idle;
        public Sprite _onPointer;

        public Dictionary<string, int> _lvlBuff = new Dictionary<string, int>();
        Dictionary<string, ButtonUpBuffW> _buttonBuff = new Dictionary<string, ButtonUpBuffW>();

        [SerializeField]
        Buff _buffPack;

        private void OnEnable()
        {
            Insta();
        }

        public void StartBuffWorks()
        {
            StaticStorage.AddLink("BuffWorks", this);
            _coin = StaticStorage.GetLink<Coin>("Coin");
            for (int i = 0; i < _buffPack.BuffWorkplace.Count; i++)
            {
                _lvlBuff.Add(_buffPack.BuffWorkplace[i].ID, 0);
                _buttonBuff.Add(_buffPack.BuffWorkplace[i].ID, Instantiate(_prefab, _grid));
            }
        }

        public void LoadBuff(WorkPlacePack wp)
        {
            _lvlBuff.Add("FastRelax", wp.FastRelax);
            _lvlBuff.Add("FlashRelax", wp.FlashRelax);
            _lvlBuff.Add("FastTask", wp.FastTask);
            _lvlBuff.Add("FlashTask", wp.FlashTask);
            _lvlBuff.Add("ExpensiveTask", wp.ExpensiveTask);
            _lvlBuff.Add("DoubleTask", wp.DoubleTask);
            _lvlBuff.Add("LuckyTask", wp.LuckyTask);
            _lvlBuff.Add("BestTask", wp.BestTask);
            StaticStorage.AddLink("BuffWorks", this);
            _coin = StaticStorage.GetLink<Coin>("Coin");
            for (int i = 0; i < _buffPack.BuffWorkplace.Count; i++)
            {
                _buttonBuff.Add(_buffPack.BuffWorkplace[i].ID, Instantiate(_prefab, _grid));
            }
        }

        void Insta()
        {
            for (int i = 0; i < _buffPack.BuffWorkplace.Count; i++)
            {
                BuffWP buffWP = _buffPack.BuffWorkplace[i];
                List<BuffParameter> buffParameters = _buffPack.GetBuff(buffWP.ID).Parameters;
                if (buffParameters.Count > _lvlBuff[buffWP.ID])
                {
                    if (PlayerLevel.Lvl >= buffParameters[_lvlBuff[buffWP.ID]].PlayerLvl)
                    {
                        string addiction = buffParameters[_lvlBuff[buffWP.ID]].Addiction.ToString();
                        if (addiction == "Ничего" || _lvlBuff[addiction] >= buffParameters[_lvlBuff[buffWP.ID]].AddictionLvl)
                        {
                            if (_coin.COIN >= buffWP.Price)
                            {
                                _buttonBuff[buffWP.ID].Insta(buffWP, _lvlBuff[buffWP.ID], true);
                                continue;
                            }
                        }
                    }
                }
                _buttonBuff[buffWP.ID].Insta(buffWP, _lvlBuff[buffWP.ID], false);
            }
        }

        public void Upgrate(string id, int lvl)
        {
            BuffWP buffWP = _buffPack.GetBuff(id);
            BuffParameter buffParameter = buffWP.GetParametrs(lvl.ToString());
            List<BuffParameter> buffParameters = _buffPack.GetBuff(id).Parameters;
            if (buffParameter != null)
            {
                if (_lvlBuff[id] == lvl - 1)
                {
                    if (PlayerLevel.Lvl >= buffWP.GetParametrs(lvl.ToString()).PlayerLvl)
                    {
                        string addiction = buffParameters[_lvlBuff[id]].Addiction.ToString();
                        if (addiction == "Ничего" || _lvlBuff[addiction] >= buffParameters[_lvlBuff[id]].AddictionLvl)
                        {
                            if (_coin.COIN >= buffWP.Price)
                            {
                                NetworkCore.Send("UpBoost", new DataPacket
                                {
                                    s1 = id.ToLower(),
                                    v1 = lvl
                                }, (data) =>
                                {
                                    if (Convert.ToBoolean(data))
                                    {
                                        _coin.COIN -= buffWP.Price;
                                        _lvlBuff[id] = lvl;
                                        Insta();
                                    }
                                });
                            }
                        }
                    }
                }
            }
            Insta();
        }

        public int GetLvl(string buff)
        {
            return _lvlBuff[buff];
        }

        public BuffWP GetData(string buff)
        {
            return _buffPack.GetBuff(buff);
        }

        public int GetCD(string buff)
        {
            return _buffPack.GetBuff(buff).Cooldown;
        }
    }
}