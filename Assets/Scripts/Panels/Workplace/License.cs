﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Game
{
    public class License : MonoBehaviour
    {
        [SerializeField]
        WorkSettings _workPlaceSettings;
        [SerializeField]
        LicenseInPanel _prefab;
        [SerializeField]
        Works _works;

        [SerializeField]
        Transform _grid;

        public TextMeshProUGUI _textLvl;
        public TextMeshProUGUI _textPrice;
        public TextMeshProUGUI _textBust;
        public TextMeshProUGUI _textDebust;
        public TextMeshProUGUI _textTime;

        int _lvlLicense;

        public int LvlLicense
        {
            set
            {
                if (value > 0)
                {
                    _lvlLicense = value;
                    gameObject.SetActive(false);
                    NetworkCore.Send("GetTasks", new Packet(), (data) =>
                    {
                        List<DataTask> dataTasks = new List<DataTask>();
                        dataTasks.AddRange(JsonUtility.FromJson<DataTasks>(data).Tasks);
                        _works.DataTask = dataTasks;
                        _works.Page = 1;
                    });
                }
            }
            get
            {
                return _lvlLicense;
            }
        }

        void Start()
        {
            transform.localPosition = new Vector3(0, -90, 0);
            for (int i = 0; i < _workPlaceSettings.LicenseType.Count; i++)
            {
                LicenseInPanel licenseInPanel = Instantiate(_prefab, _grid);
                licenseInPanel.Insta(_workPlaceSettings.LicenseType[i], this);
            }
        }

    }
}