﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Game
{
    public class LicenseInPanel : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        Coin _coin;

        [SerializeField]
        Button _button;
        [SerializeField]
        TextMeshProUGUI _text;
        [SerializeField]
        Image _bg;

        [SerializeField]
        Sprite _idle;
        [SerializeField]
        Sprite _pointerEnter;

        License _license;
        LicenseType _licenseType;

        public void Insta(LicenseType licenseType, License license)
        {
            _licenseType = licenseType;
            _license = license;
        }

        private void OnEnable()
        {
            _button.onClick.AddListener(Buy);
        }

        private void Start()
        {
            _coin = StaticStorage.GetLink<Coin>("Coin");
        }

        private void Buy()
        {
            if (_coin.COIN >= _licenseType.Price)
            {
                NetworkCore.Send("BuyLicense", new DataPacket
                {
                    v1 = _licenseType.Lvl,
                }, (data) =>
                {
                    if (Convert.ToBoolean(data))
                    {
                        _coin.COIN -= _licenseType.Price;
                        _license.LvlLicense = _licenseType.Lvl;
                    }
                });
            }
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            _license._textLvl.text = "License " + _licenseType.Lvl;
            _license._textPrice.text = string.Format("{0:0.00}", _licenseType.Price) + "$";
            _license._textBust.text = _licenseType.PriceTask.ToString() + "$";
            if (_licenseType.Lvl != 1)
            {
                _license._textDebust.text = "-" + ((_licenseType.Lvl - 1) * 2) + "%";
            }
            else
            {
                _license._textDebust.text = "No effect";
            }
            _license._textTime.text = _licenseType.TimeTask.ToTime();
            _text.gameObject.SetActive(true);
            _bg.sprite = _pointerEnter;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            _text.gameObject.SetActive(false);
            _bg.sprite = _idle;
        }
    }
}