﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class BuffMenu : MonoBehaviour
    {
        [SerializeField]
        Button _buttonLuckyTask;
        [SerializeField]
        Button _buttonFlashRelax;
        [SerializeField]
        Button _buttonFastTask;
        [SerializeField]
        Button _buttonExpensiveTask;
        [SerializeField]
        Button _buttonFlashTask;
        [SerializeField]
        Button _buttonFastRelax;
        [SerializeField]
        Button _buttonDoubleTask;
        [SerializeField]
        Button _buttonBestTask;


        [SerializeField]
        WorkingTimer _workingTimer;
        [SerializeField]
        BuffWorks _buffWorks;

        [SerializeField]
        WorkplaceManager _workplaceManager;


        private void OnEnable()
        {
            _buttonLuckyTask.onClick.AddListener(LuckyTask);
            _buttonFlashRelax.onClick.AddListener(FlashRelax);
            _buttonFastTask.onClick.AddListener(FastTask);
            _buttonExpensiveTask.onClick.AddListener(ExpensiveTask);
            _buttonFlashTask.onClick.AddListener(FlashTask);
            _buttonFastRelax.onClick.AddListener(FastRelax);
            _buttonDoubleTask.onClick.AddListener(DoubleTask);
            _buttonBestTask.onClick.AddListener(BestTask);
            ActiveButton();
        }

        private void OnDisable()
        {
            _buttonLuckyTask.onClick.RemoveListener(LuckyTask);
            _buttonFlashRelax.onClick.RemoveListener(FlashRelax);
            _buttonFastTask.onClick.RemoveListener(FastTask);
            _buttonExpensiveTask.onClick.RemoveListener(ExpensiveTask);
            _buttonFlashTask.onClick.RemoveListener(FlashTask);
            _buttonFastRelax.onClick.RemoveListener(FastRelax);
            _buttonDoubleTask.onClick.RemoveListener(DoubleTask);
            _buttonBestTask.onClick.RemoveListener(BestTask);
        }

        void ActiveButton()
        {
            _buttonLuckyTask.interactable = _buffWorks.GetLvl("LuckyTask") != 0 && _workingTimer._working == true;
            _buttonFlashRelax.interactable = _buffWorks.GetLvl("FlashRelax") != 0 && _workingTimer._relax == true;
            _buttonFastTask.interactable = _buffWorks.GetLvl("FastTask") != 0 && _workingTimer._working == true;
            _buttonExpensiveTask.interactable = _buffWorks.GetLvl("ExpensiveTask") != 0 && _workingTimer._working == true;
            _buttonFlashTask.interactable = _buffWorks.GetLvl("FlashTask") != 0 && _workingTimer._working == true;
            _buttonFastRelax.interactable = _buffWorks.GetLvl("FastRelax") != 0 && _workingTimer._relax == true;
            _buttonDoubleTask.interactable = _buffWorks.GetLvl("DoubleTask") != 0 && _workingTimer._working == true;
            _buttonBestTask.interactable = _buffWorks.GetLvl("BestTask") != 0 && _workingTimer._working == false && _workingTimer._relax == false;
        }

        private void FlashTask()
        {
            NetworkCore.Send("FlashTask", new Packet(), (data) =>
            {
                if (Convert.ToBoolean(data))
                {
                    _workingTimer.PlusePrice(-100 + (_buffWorks.GetData("FlashTask").Parametr * _buffWorks.GetLvl("FlashTask") + _buffWorks.GetData("FlashTask").StartParametr));
                    _workingTimer.PluseProgress(100);
                    _workplaceManager.StartCoroutine(CD("FlashTask", _buttonFlashTask, 0));
                }
            });
        }

        private void DoubleTask()
        {
            NetworkCore.Send("DoubleTask", new Packet(), (data) =>
            {
                if (Convert.ToBoolean(data))
                {
                    _workplaceManager.StartCoroutine(CD("DoubleTask", _buttonDoubleTask, 0));
                }
            });
        }

        private void FastRelax()
        {
            NetworkCore.Send("FastRelax", new Packet(), (data) =>
            {
                if (Convert.ToBoolean(data))
                {
                    _workingTimer.MinusTimeRelax(_buffWorks.GetData("FastRelax").Parametr * _buffWorks.GetLvl("FastRelax") + _buffWorks.GetData("FastRelax").StartParametr);
                    _workplaceManager.StartCoroutine(CD("FastRelax", _buttonFastRelax, 0));
                }
            });
        }

        private void BestTask()
        {
            NetworkCore.Send("BestTask", new Packet(), (data) =>
            {
                if (Convert.ToBoolean(data))
                {
                    _workplaceManager.StartCoroutine(CD("BestTask", _buttonBestTask, 0));
                }
            });
        }

        private void ExpensiveTask()
        {
            NetworkCore.Send("ExpensiveTask", new Packet(), (data) =>
            {
                if (Convert.ToBoolean(data))
                {
                    _workingTimer.PlusePrice(_buffWorks.GetData("ExpensiveTask").Parametr * _buffWorks.GetLvl("ExpensiveTask") + _buffWorks.GetData("ExpensiveTask").StartParametr);
                    _workplaceManager.StartCoroutine(CD("ExpensiveTask", _buttonExpensiveTask, 0));
                }
            });
        }

        private void FastTask()
        {
            NetworkCore.Send("FastTask", new Packet(), (data) =>
            {
                if (Convert.ToBoolean(data))
                {
                    _workingTimer.PluseProgress(_buffWorks.GetData("FastTask").Parametr * _buffWorks.GetLvl("FastTask") + _buffWorks.GetData("FastTask").StartParametr);
                    _workplaceManager.StartCoroutine(CD("FastTask", _buttonFastTask, 0));
                }
            });
        }

        private void FlashRelax()
        {
            NetworkCore.Send("FlashRelax", new Packet(), (data) =>
            {
                if (Convert.ToBoolean(data))
                {
                    _workingTimer.MinusTimeRelax(100);
                    _workplaceManager.StartCoroutine(CD("FlashRelax", _buttonFlashRelax, 0));
                }
            });
        }

        private void LuckyTask()
        {
            NetworkCore.Send("LuckyTask", new Packet(), (data) =>
            {
                if (Convert.ToBoolean(data))
                {
                    _workingTimer.PluseChance(_buffWorks.GetData("LuckyTask").Parametr * _buffWorks.GetLvl("LuckyTask") + _buffWorks.GetData("LuckyTask").StartParametr);
                    _workplaceManager.StartCoroutine(CD("LuckyTask", _buttonLuckyTask, 0));
                }
            });
        }

        public void CheckCDAll(WorkPlacePack wp)
        {
            int i = (int)DateTimeOffset.UtcNow.ToUnixTimeSeconds();
            _workplaceManager.StartCoroutine(CD("FlashTask", _buttonFlashTask, i - wp.FlashTaskCD));
            _workplaceManager.StartCoroutine(CD("DoubleTask", _buttonFlashTask, i - wp.DoubleTaskCD));
            _workplaceManager.StartCoroutine(CD("FastRelax", _buttonFlashTask, i - wp.FastRelaxCD));
            _workplaceManager.StartCoroutine(CD("BestTask", _buttonFlashTask, i - wp.BestTaskCD));
            _workplaceManager.StartCoroutine(CD("ExpensiveTask", _buttonFlashTask, i - wp.ExpensiveTaskCD));
            _workplaceManager.StartCoroutine(CD("FastTask", _buttonFlashTask, i - wp.FastTaskCD));
            _workplaceManager.StartCoroutine(CD("FlashRelax", _buttonFlashTask, i - wp.FlashRelaxCD));
            _workplaceManager.StartCoroutine(CD("LuckyTask", _buttonFlashTask, i - wp.LuckyTaskCD));
        }

        IEnumerator CD(string buff, Button button, int progress)
        {
            TimerCD timer = button.GetComponentInChildren<TimerCD>(true);
            timer.gameObject.SetActive(true);
            int cd = _buffWorks.GetCD(buff);
            if (buff == "FlashRelax")
            {
                cd = cd - 1800 * (_buffWorks.GetLvl(buff) - 1);
            }

            int i = cd - progress;

            for (int x = 0; x < i; i--)
            {
                timer._text.text = (i / 3600).ToString("D2") + ":" + ((i % 3600) / 60).ToString("D2") + ":" + (i % 60).ToString("D2");
                yield return new WaitForSecondsRealtime(1);
            }
            ActiveButton();
            timer.gameObject.SetActive(false);
        }
    }
}