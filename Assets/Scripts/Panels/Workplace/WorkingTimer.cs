﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class WorkingTimer : MonoBehaviour
    {
        [SerializeField]
        License _license;
        public Works _works;
        [SerializeField]
        Text _title;
        [SerializeField]
        Text _timer;
        [SerializeField]
        GameObject _info;
        [SerializeField]
        Text _name;
        [SerializeField]
        Text _lvl;
        [SerializeField]
        Text _price;
        [SerializeField]
        Text _chance;
        [SerializeField]
        Text _timeRelax;
        [SerializeField]
        Image _progress;
        [SerializeField]
        Button _button;
        [SerializeField]
        MiniBarTasks _miniBarTasks;

        public DataTask _dataTask;

        public bool _working = true;
        public bool _relax = false;

        private void OnEnable()
        {
            _button.onClick.AddListener(Done);
            _button.gameObject.SetActive(!_working);
        }

        private void OnDisable()
        {
            _button.onClick.RemoveListener(Done);
        }

        public void Done()
        {
            if (_dataTask.Progress >= _dataTask.Timer)
            {
                NetworkCore.Send("DoneTask", new Packet(), (data) =>
                {
                    NetworkCore.Send("CheckTask", new Packet(), (dataTask) =>
                    {
                        _dataTask = JsonUtility.FromJson<DataTask>(dataTask);
                        if (_dataTask != null && _dataTask.id != 0)
                        {
                            _works.Working(_dataTask);
                        }
                    });
                    DataPacket dataPacket = JsonUtility.FromJson<DataPacket>(data);
                    if (Convert.ToBoolean(dataPacket.v1))
                    {
                        if (Convert.ToBoolean(dataPacket.v2))
                        {
                            _works._coinsIncome.Income = _dataTask.Price + _dataTask.Deposit;
                            _works._coin.COIN += _dataTask.Price + _dataTask.Deposit;
                            Debug.Log("Задание выполнено");
                            PlayerLevel.Exp = 10;
                        }
                        else
                        {
                            Debug.Log("Задание провалено");
                        }
                        _works.CheckLicense();
                    }
                });
            }
        }

        public IEnumerator Working()
        {
            _working = true;
            _button.gameObject.SetActive(false);
            int tr = _dataTask.TimerRelax;
            _price.text = (Math.Floor(_dataTask.Price * 10000) / 10000).ToString() + "$";
            _name.text = _dataTask.Name;
            _lvl.text = _dataTask.Lvl.ToString() + " УРОВЕНЬ";
            _timeRelax.text = tr.ToTime();
            _chance.text = Mathf.Clamp((_dataTask.ChanceDefault + (PlayerLevel.Lvl - 3) * 3 - (_license.LvlLicense - 1) * 2), 0, 100).ToString() + "%";
            _title.text = "Выполняется:";
            _info.SetActive(true);
            gameObject.SetActive(true);
            while (_dataTask.Progress < _dataTask.Timer)
            {
                yield return new WaitForSecondsRealtime(1);
                _dataTask.Progress++;
                _progress.fillAmount = (1f / _dataTask.Timer) * _dataTask.Progress;
                int i = _dataTask.Timer - _dataTask.Progress;
                _timer.text = i.ToTime();
                _miniBarTasks.Refresh(_timer.text, _progress.fillAmount);
            }
            _working = false;
        }

        public IEnumerator Relax()
        {
            _relax = true;
            _working = true;
            _title.text = "Отдых:";
            _button.gameObject.SetActive(false);
            gameObject.SetActive(true);
            int relax = _dataTask.TimerRelax; 
            while (_dataTask.Progress < _dataTask.TimerRelax)
            {
                yield return new WaitForSecondsRealtime(1);
                _dataTask.Progress++;
                _progress.fillAmount = (1f / _dataTask.TimerRelax) * _dataTask.Progress;
                int i = _dataTask.TimerRelax - _dataTask.Progress;
                _timer.text = i.ToTime();
                _miniBarTasks.Refresh(_timer.text, _progress.fillAmount);
            }
            _works._tasksPanel.SetActive(true);
            gameObject.SetActive(false);
            _relax = false;
            _miniBarTasks.Refresh("Бездельничает", 0);
        }

        public void PluseProgress(float percent)
        {
            if (_dataTask != null)
            {
                _dataTask.Progress = Mathf.Clamp((int)(_dataTask.Progress + (float)(_dataTask.Timer - _dataTask.Progress) / 100 * percent), 0, _dataTask.Timer);
            }
        }

        public void PlusePrice(float percent)
        {
            if (_dataTask != null)
            {
                _dataTask.Price += _dataTask.Price / 100 * percent;
                _price.text = _dataTask.Price.ToString();
            }
        }

        public void MinusTimeRelax(float percent)
        {
            if (_dataTask != null)
            {
                _dataTask.TimerRelax = Mathf.Clamp((int)(_dataTask.TimerRelax - (float)_dataTask.TimerRelax / 100 * percent), 0, int.MaxValue);
                _timeRelax.text = _dataTask.TimerRelax.ToTime();
            }
        }

        public void PluseChance(float percent)
        {
            if (_dataTask != null)
            {
                _dataTask.ChanceDefault += (_dataTask.ChanceDefault / 100 * percent);
                _chance.text = Mathf.Clamp(_dataTask.ChanceDefault + (PlayerLevel.Lvl - 3) * 3 - (1 - 1) * 2, 0, 100).ToString();
            }
        }

        public DataTask GetDataTask()
        {
            return _dataTask;
        }
    }
}