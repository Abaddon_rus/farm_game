﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

namespace Game
{
    public class ButtonUpBuffW : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        BuffWorks _buffWorks;
        [SerializeField]
        Image _bg;
        [SerializeField]
        Button _button;
        [SerializeField]
        Image _icon;

        int _lvl;
        string _id;

        BuffWP _buffWP;

        private void OnEnable()
        {
            _button.onClick.AddListener(Click);
        }

        private void OnDisable()
        {
            _button.onClick.RemoveListener(Click);
        }

        public void Insta(BuffWP buffWP, int lvl, bool interactable)
        {
            _buffWP = buffWP;
            _icon.sprite = buffWP.Icon;
            _lvl = lvl;
            _button.interactable = interactable;
        }

        private void Click()
        {
            _buffWorks.Upgrate(_id, _lvl + 1);
            PlayerLevel.Exp = 5;
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (_buffWorks == null)
            {
                _buffWorks = StaticStorage.GetLink<BuffWorks>("BuffWorks");
            }

            string addiction = "";

            _id = _buffWP.ID;
            _buffWorks._name.text = _buffWP.Name;
            _buffWorks._dexcription.text = _buffWP.Description;

            BuffParameter parameter = _buffWP.GetParametrs(_lvl.ToString());
            BuffParameter parameterUP = _buffWP.GetParametrs((_lvl + 1).ToString());

            if (_lvl + 1 <= _buffWP.Parameters.Count)
            {
                if (_buffWP.ID != "FlashRelax")
                {
                    if (parameter == null)
                    {
                        _buffWorks._parametrs.text = _lvl + " lvl.: ------ \n" + (_lvl + 1) + " lvl.: " + (_buffWP.Parametr * (_lvl + 1) + _buffWP.StartParametr) + "%       ";
                    }
                    else
                    {
                        _buffWorks._parametrs.text = _lvl + " lvl.: " + (_buffWP.Parametr * (_lvl) + _buffWP.StartParametr) + "% ✓ \n" + (_lvl + 1) + " lvl.: " + (_buffWP.Parametr * (_lvl + 1) + _buffWP.StartParametr) + "%       ";
                    }
                }
                else
                {
                    string time1 = ((int)(_buffWP.Cooldown - (_buffWP.Parametr * (_lvl) + _buffWP.StartParametr))).ToTime();
                    string time2 = ((int)(_buffWP.Cooldown - (_buffWP.Parametr * (_lvl + 1) + _buffWP.StartParametr))).ToTime();
                    if (parameter == null)
                    {
                        _buffWorks._parametrs.text = _lvl + " lvl.: ------ \n" + (_lvl + 1) + " lvl.: " + time2 + " Cooldown     ";
                    }
                    else
                    {
                        _buffWorks._parametrs.text = _lvl + " lvl.: " + time1 + " Cooldown ✓ \n" + (_lvl + 1) + " lvl.: " + time2 + " Cooldown     ";
                    }
                }

                if (parameterUP.AddictionLvl != 0)
                {
                    addiction += "Addiction: " + _buffWorks.GetData(parameterUP.Addiction.ToString()).Name + " " + parameterUP.AddictionLvl + " ур. \n";
                }

                if (parameterUP.PlayerLvl != 0)
                {
                    addiction += "Addiction: " + parameterUP.PlayerLvl + " player lvl.";
                }
                _buffWorks._addiction.text = addiction;
                _buffWorks._price.text = string.Format("{0:0.00}", _buffWP.Price) + " $";
            }
            else
            {
                _buffWorks._parametrs.text = _lvl + " lvl.: " + (_buffWP.Parametr * (_lvl) + _buffWP.StartParametr) + "% ✓";
                _buffWorks._price.text = "MAX";
                _buffWorks._addiction.text = "Maximum level reached";
            }
            _bg.sprite = _buffWorks._onPointer;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            _bg.sprite = _buffWorks._idle;
        }
    }
}