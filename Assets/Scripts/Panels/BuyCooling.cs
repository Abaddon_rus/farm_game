﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class BuyCooling : MonoBehaviour
    {

        [SerializeField]
        Button _button;
        [SerializeField]
        Coin _coin;

        private CoolingPlant _plant;

        private void OnEnable()
        {
            _button.onClick.AddListener(Buy);
        }

        private void OnDisable()
        {
            _button.onClick.RemoveListener(Buy);
        }

        private void Update()
        {
            _button.interactable = _coin.COIN >= 10;
            if (_plant != null)
            {
                var camera = Camera.main;
                var worldPosition = _plant.transform.position + new Vector3(0, 1.5f, 0);
                var viewportPosition = camera.WorldToViewportPoint(worldPosition);
                var rt = transform as RectTransform;

                rt.anchorMin = viewportPosition;
                rt.anchorMax = viewportPosition;
            }
        }

        private void Buy()
        {
            NetworkCore.Send("BuyCooling", new DataPacket
            {
                v1 = _plant._number
            }, (data) =>
            {
                if (Convert.ToInt32(data) > 0)
                {
                    PlantCooling(Convert.ToInt32(data));
                    PlayerLevel.Exp = 20;
                }
                else
                {
                    Debug.Log("Запрос не выполнен");
                }
            });
        }

        void PlantCooling(int result)
        {
            if (result > 0)
            {
                Cooling cooling = StaticStorage.GetLink<CoolingPack>("CoolingPack").Cooling[1].Clone();
                cooling.ID = result;
                _plant.Planting(cooling);
                gameObject.SetActive(false);
                _coin.CheckCoins();
            }
        }

        public void Show(CoolingPlant plant)
        {
            _plant = plant;
            var camera = Camera.main;
            var worldPosition = _plant.transform.position + new Vector3(0, 1.5f, 0);
            var viewportPosition = camera.WorldToViewportPoint(worldPosition);
            var rt = transform as RectTransform;

            rt.anchorMin = viewportPosition;
            rt.anchorMax = viewportPosition;
            gameObject.SetActive(true);
        }
    }
}