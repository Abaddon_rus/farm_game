﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class FarmIcons : MonoBehaviour
    {
        [SerializeField]
        GameObject _prefabMineCoins;
        [SerializeField]
        GameObject _prefabTimer;

        private void Awake()
        {
            StaticStorage.AddLink("FarmIcons", this);
        }

        public MineCoinsIcon StartMine(Transform tf)
        {
            GameObject MineCoins = Instantiate(_prefabMineCoins, transform);
            MineCoinsIcon mineCoinsIcon = MineCoins.GetComponent<MineCoinsIcon>();
            return mineCoinsIcon.Transform(tf);
        }

        public TimerPanel StartTimer(Transform tf)
        {
            GameObject Timer = Instantiate(_prefabTimer, transform);
            TimerPanel timerPanel = Timer.GetComponent<TimerPanel>();
            return timerPanel.Transform(tf);
        }
    }
}
