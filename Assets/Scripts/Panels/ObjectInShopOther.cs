﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Game
{
    public class ObjectInShopOther : MonoBehaviour
    {
        [SerializeField]
        Text _name;
        [SerializeField]
        Image _icon;
        [SerializeField]
        Text _priceTxt;
        [SerializeField]
        Text _descriptionTxt;
        [SerializeField]
        Button _buttonBuy;
        [SerializeField]
        InputField _inputField;
        [SerializeField]
        Button _buttonLeft;
        [SerializeField]
        Button _buttonRight;

        Other _other;

        private void OnEnable()
        {
            _buttonBuy.onClick.AddListener(Buy);
            _buttonLeft.onClick.AddListener(() => ChangeCount(-1));
            _buttonRight.onClick.AddListener(() => ChangeCount(1));
            _buttonBuy.interactable = false;
        }

        private void OnDisable()
        {
            _buttonBuy.onClick.RemoveListener(Buy);
            _buttonLeft.onClick.RemoveListener(() => ChangeCount(-1));
            _buttonRight.onClick.RemoveListener(() => ChangeCount(1));
        }

        private void Update()
        {
            _buttonBuy.interactable = StaticStorage.GetLink<Coin>("Coin").COIN >= _other.Price * Convert.ToInt32(_inputField.text);
            _priceTxt.text = "Купить за: $" + _other.Price * Convert.ToInt32(_inputField.text);
        }
        
        public void Instant(Other other)
        {
            _other = other;
            _name.text = _other.Name;
            _icon.sprite = _other.Icon;
            _priceTxt.text = "Купить за: $" + _other.Price * Convert.ToInt32(_inputField.text);
            _descriptionTxt.text = _other.Description;
        }

        private void Buy()
        {
            int count = Convert.ToInt32(_inputField.text);
            NetworkCore.Send(_other.Query.ToString(), new DataPacket
            {
                v1 = count
            }, (data) =>
            {
                StaticStorage.GetLink<Inventory>("Inventory").Add(_other, count);
            });
        }

        void ChangeCount(int v)
        {
            _inputField.text = Mathf.Clamp((Convert.ToInt32(_inputField.text) + v), 0, int.MaxValue).ToString();
        }
    }
}