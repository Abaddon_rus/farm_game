﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pursuit : MonoBehaviour
{
    [SerializeField]
    Vector3 offset;

    private void OnEnable()
    {
        Vector3 end = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = end + new Vector3(0, 0, 10) + offset;
    }
    
    void FixedUpdate()
    {
        Vector3 end = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = end + new Vector3(0, 0, 10) + offset;
    }
}
