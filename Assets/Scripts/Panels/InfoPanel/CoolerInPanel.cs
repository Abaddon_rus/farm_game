﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Game
{
    public class CoolerInPanel : ComponentInpanel, IPointerClickHandler, IDropHandler
    {
        [SerializeField]
        Image _icon;

        SlotCooler _slotCooler;

        Inventory _inventory;
        InfoPanel _infoPanel;

        private void Start()
        {
            _inventory = StaticStorage.GetLink<Inventory>("Inventory");
            _infoPanel = StaticStorage.GetLink<InfoPanel>("InfoPanel");
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (_slotCooler.Cooler.Index == 0)
            {
                if (PlantingComponents.Cooler == null)
                {
                    StaticStorage.GetLink<Menu>("Menu").OpenInventory();
                    _inventory.CoolerInInventory();
                    DestroyDrag();
                }
                else
                {
                    if (_slotCooler._slotFarm.STOP)
                    {
                        var pack = StaticStorage.GetLink<CoolerPack>("CoolerPack");
                        var minLevel = pack.GetCooler(PlantingComponents.Cooler.Index).MinFarmLvl;
                        if (minLevel <= _slotCooler._slotFarm.FARM.Level)
                        {
                            NetworkCore.Send("PlantCooler", new DataPacket
                            {
                                v1 = _slotCooler._slotFarm.ID,
                                v2 = PlantingComponents.Cooler.ID,
                                v3 = _slotCooler._numberSlot
                            }, (data) =>
                            {
                                Plant(Convert.ToBoolean(data));
                            });
                        }
                        else
                        {
                            _infoPanel.Alert("Нужно улучшение сервера!");
                            DestroyDrag();
                        }
                    }
                    else
                    {
                        _infoPanel.Alert("Сервер включен!");
                        DestroyDrag();
                    }
                }
            }
            else
            {
                _infoPanel._infoComponents.Show(_slotCooler);
            }
        }


        void Plant(bool result)
        {
            if (result)
            {

                _slotCooler.Cooler = PlantingComponents.Cooler;
                _inventory.Remove(PlantingComponents.Cooler);
                _infoPanel.RefreshFarm();
                DestroyDrag();
            }
        }

        public void Open(SlotCooler slotCooler)
        {
            _slotCooler = slotCooler;
            if (slotCooler.Cooler.Index != 0)
            {
                _icon.sprite = _slotCooler.Cooler.Icon;
                _icon.color = new Color(1, 1, 1, 1);
            }
            else
            {
                _icon.color = new Color(1, 1, 1, 0);
            }
        }

        public void OnDrop(PointerEventData eventData)
        {
            if (_slotCooler.Cooler.Index == 0)
            {
                if (PlantingComponents.Cooler == null)
                {
                    StaticStorage.GetLink<Menu>("Menu").OpenInventory();
                    _inventory.CoolerInInventory();
                    DestroyDrag();
                }
                else
                {
                    if (_slotCooler._slotFarm.STOP)
                    {
                        var pack = StaticStorage.GetLink<CoolerPack>("CoolerPack");
                        var minLevel = pack.GetCooler(PlantingComponents.Cooler.Index).MinFarmLvl;
                        if (minLevel <= _slotCooler._slotFarm.FARM.Level)
                        {
                            NetworkCore.Send("PlantCooler", new DataPacket
                            {
                                v1 = _slotCooler._slotFarm.ID,
                                v2 = PlantingComponents.Cooler.ID,
                                v3 = _slotCooler._numberSlot
                            }, (data) =>
                            {
                                Plant(Convert.ToBoolean(data));
                            });
                        }
                        else
                        {
                            _infoPanel.Alert("Нужно улучшение сервера!");
                            DestroyDrag();
                        }
                    }
                    else
                    {
                        _infoPanel.Alert("Сервер включен!");
                        DestroyDrag();
                    }
                }
            }
            else
            {
                _infoPanel._infoComponents.Show(_slotCooler);
                DestroyDrag();
            }
        }

        void DestroyDrag()
        {
            PlantingComponents.Clear();
            if (PlantingComponents.DragItem != null)
                Destroy(PlantingComponents.DragItem);
        }
    }
}