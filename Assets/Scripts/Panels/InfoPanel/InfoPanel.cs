﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using TMPro;

namespace Game
{
    public class InfoPanel : MonoBehaviour, IUpdatable
    {
        SlotFarm _slotFarm;
        SlotCooling _slotCooling;

        [SerializeField]
        Image _icon;
        [SerializeField]
        Text _name;
        [SerializeField]
        Text _parametr;
        [SerializeField]
        Text _parametr2;
        [SerializeField]
        Text _parametr3;
        [SerializeField]
        Text _parametr4;
        [SerializeField]
        Text _parametr5;
        [SerializeField]
        Text _parametr6;
        [SerializeField]
        Text _parametr7;
        [SerializeField]
        Text _priceUP;

        [SerializeField]
        GameObject _alert;

        [SerializeField]
        TextMeshProUGUI _alertText;

        [SerializeField]
        GameObject _alertStart;

        [SerializeField]
        GameObject _progress;
        [SerializeField]
        Image _fillProgress;

        [SerializeField]
        Button _startButton;
        [SerializeField]
        Text _textButton;
        [SerializeField]
        Button _upgrateButton;
        [SerializeField]
        Button _repairButton;

        [SerializeField]
        RepairPanel _repairPanel;

        [SerializeField]
        List<CardInPanel> _cardsInPanel;
        [SerializeField]
        List<CoolerInPanel> _coolerInPanels;
        [SerializeField]
        List<HardInPanel> _hardInPanels;

        public InfoComponents _infoComponents;
        Coin _coin;
        CoolingPack _coolingPack;
        FarmPack _farmPack;
        GamePlay _gamePlay;

        float _volume = 0;

        bool _query = false;
        float priceUp;

        private void Awake()
        {
            StaticStorage.AddLink("InfoPanel", this);
        }

        private void OnEnable()
        {
            _startButton.onClick.AddListener(StartStopFarm);
            //_inInventoryButton.onClick.AddListener(InInventory);
            _upgrateButton.onClick.AddListener(Upgrate);
            _repairButton.onClick.AddListener(Repair);
        }

        private void Start()
        {
            _coin = StaticStorage.GetLink<Coin>("Coin");
            _coolingPack = StaticStorage.GetLink<CoolingPack>("CoolingPack");
            _farmPack = StaticStorage.GetLink<FarmPack>("FarmPack");
            _gamePlay = StaticStorage.GetLink<GamePlay>("GamePlay");
        }

        private void Upgrate()
        {
            if (_slotFarm != null)
            {
                NetworkCore.Send("UpFarm", new DataPacket
                {
                    v1 = _slotFarm.ID
                }, (data) =>
                {
                    UpgrateFarm(Convert.ToBoolean(data));
                });
            }
            else if (_slotCooling != null)
            {
                NetworkCore.Send("UpCooling", new DataPacket
                {
                    v1 = _slotCooling.ID
                }, (data) =>
                {
                    UpgrateCooling(Convert.ToBoolean(data));
                });
            }
        }

        void UpgrateFarm(bool result)
        {
            if (result)
            {
                _slotFarm.Upgrate();
                RefreshFarm();
            }
        }

        void UpgrateCooling(bool result)
        {
            if (result)
            {
                _slotCooling.Upgrate();
                RefreshCooling();
            }
        }

        private void StartStopFarm()
        {
            if (_slotFarm != null)
            {
                _query = true;
                if (_slotFarm.STOP)
                {
                    bool haveCard = false;
                    bool haveHard = false;
                    for (int i = 0; i < _cardsInPanel.Count; i++)
                    {
                        if (_cardsInPanel[i].Busy())
                        {
                            haveCard = true;
                        }
                    }
                    for (int i = 0; i < _hardInPanels.Count; i++)
                    {
                        if (_hardInPanels[i].Busy())
                        {
                            haveHard = true;
                        }
                    }
                    if (haveCard && haveHard)
                    {
                        NetworkCore.Send("StartStopFarm", new DataPacket
                        {
                            v1 = _slotFarm.ID,
                            v2 = 0
                        }, (data) =>
                        {
                            CommandFarm(Convert.ToBoolean(data));
                        });
                    }
                    else
                    {
                        _alertStart.gameObject.SetActive(true);
                        _query = false;
                    }
                }
                else
                {
                    NetworkCore.Send("StartStopFarm", new DataPacket
                    {
                        v1 = _slotFarm.ID,
                        v2 = 1
                    }, (data) =>
                    {
                        CommandFarm(Convert.ToBoolean(data));
                    });
                }

            }
            else
            {
                _query = true;
                if (_slotCooling.IsStoped)
                {
                    NetworkCore.Send("StartStopCooling", new DataPacket
                    {
                        v1 = _slotCooling.ID,
                        v2 = 0
                    }, (data) =>
                    {
                        CommandCooling(Convert.ToBoolean(data));
                    });
                }
                else
                {
                    NetworkCore.Send("StartStopCooling", new DataPacket
                    {
                        v1 = _slotCooling.ID,
                        v2 = 1
                    }, (data) =>
                    {
                        CommandCooling(Convert.ToBoolean(data));
                    });
                }

            }
        }

        void ProgessBar(bool value)
        {
            if (_slotFarm != null)
            {
                _progress.SetActive(value);
            }
            else
            {
                _progress.SetActive(false);
            }
            _repairButton.gameObject.SetActive(!value);
        }

        void CommandFarm(bool result)
        {
            if (result)
            {
                if (_slotFarm.STOP)
                {
                    _slotFarm.STOP = false;
                    ProgessBar(false);
                    _textButton.text = "Switch OFF";
                }
                else
                {
                    _slotFarm.STOP = true;
                    ProgessBar(true);
                    _textButton.text = "Switch ON";
                }
                RefreshFarm();
            }
            _query = false;
        }

        void CommandCooling(bool result)
        {
            if (result)
            {
                if (_slotCooling.IsStoped)
                {
                    _slotCooling.IsStoped = false;
                    ProgessBar(false);
                    _textButton.text = "Switch OFF";
                }
                else
                {
                    _slotCooling.IsStoped = true;
                    ProgessBar(true);
                    _textButton.text = "Switch ON";
                }
                RefreshCooling();
            }
            _query = false;
        }

        void OpenPanel()
        {
            if (!_query)
            {
                _gamePlay.ClosePanels();
                _infoComponents.Close();
                StopAllCoroutines();
                StartCoroutine(Open());
                Clean();
                StaticStorage.UpdateCore.Add(this);
            }
        }

        public void ClosePanel()
        {
            if (!_query)
            {
                StopAllCoroutines();
                StartCoroutine(Close());
                StaticStorage.UpdateCore.Remove(this);
                _infoComponents.Close();
            }
        }

        void Clean()
        {
            if (!_query)
            {
                _volume = 0;
                _slotFarm = null;
                _slotCooling = null;
                DeleteSlots();
            }
        }

        internal void Show(SlotFarm slotFarm)
        {
            if (!_query)
            {
                OpenPanel();
                _slotFarm = slotFarm;
                RefreshFarm();
            }
        }

        internal void Show(SlotCooling slotCooling)
        {
            if (!_query)
            {
                OpenPanel();
                _slotCooling = slotCooling;
                RefreshCooling();
            }
        }

        void DeleteSlots()
        {
            for (int i = 0; i < _cardsInPanel.Count; i++)
            {
                _cardsInPanel[i].gameObject.SetActive(false);
            }
            for (int i = 0; i < _coolerInPanels.Count; i++)
            {
                _coolerInPanels[i].gameObject.SetActive(false);
            }
            for (int i = 0; i < _hardInPanels.Count; i++)
            {
                _hardInPanels[i].gameObject.SetActive(false);
            }
        }

        public void RefreshFarm()
        {
            if (_slotFarm.FARM.Level < 5)
            {
                priceUp = _farmPack.GetFarm(_slotFarm.FARM.Index + 1).Price;
                _priceUP.text = "Upgrade(" + priceUp + "$)";
            }
            else
            {
                _priceUP.text = "Max lvl";
            }
            Farm farm = _slotFarm.FARM;
            _volume = 0;
            float coins = 0;
            for (int i = 0; i < farm.SlotsCards.Count; i++)
            {
                _cardsInPanel[i].gameObject.SetActive(true);
                _cardsInPanel[i].Open(farm.SlotsCards[i]);
            }
            for (int i = 0; i < farm.SlotsCoolers.Count; i++)
            {
                _coolerInPanels[i].gameObject.SetActive(true);
                _coolerInPanels[i].Open(farm.SlotsCoolers[i]);
            }
            for (int i = 0; i < farm.SlotsHards.Count; i++)
            {
                _hardInPanels[i].gameObject.SetActive(true);
                _hardInPanels[i].Open(farm.SlotsHards[i]);
            }
            _icon.sprite = farm.Icon;
            _name.text = farm.Name;
            _parametr.text = "Server " + farm.Level + " lvl.";
            _parametr2.text = "Consumption: " + farm.Electricity + " Vt/h";
            _parametr3.text = "Strength: " + farm.Strength + "/" + farm.MaxStrength;
            if (_slotFarm.STOP)
            {
                _textButton.text = "Switch ON";
                ProgessBar(false);
            }
            else
            {
                ProgessBar(true);
                _textButton.text = "Switch OFF";
            }
            for (int i = 0; i < _slotFarm.FARM.SlotsHards.Count; i++)
            {
                _volume += _slotFarm.FARM.SlotsHards[i].HARD.Volume;
                coins += _slotFarm.FARM.SlotsHards[i].HARD.Coins;
            }
            _fillProgress.fillAmount = coins / _volume;
            _parametr7.text = _volume + " $";
            _parametr6.text = coins + " /" + _volume + " $";
            _parametr5.text = _slotFarm._heatDissipation + " Cº";
            _parametr4.text = "+ " + _slotFarm._incomeFarm + " $/h";
        }

        void IUpdatable.OnUpdate(float deltaTime)
        {
            if (_slotFarm != null)
            {
                float coins = 0;
                for (int i = 0; i < _slotFarm.FARM.SlotsHards.Count; i++)
                {
                    coins += _slotFarm.FARM.SlotsHards[i].HARD.Coins;
                }

                _fillProgress.fillAmount = coins / _volume;
                _parametr6.text = coins.ToString("N8") + " /" + _volume + " $";
                _parametr5.text = _slotFarm._heatDissipation + " Cº";
                _parametr4.text = "+ " + _slotFarm._incomeFarm + " $/h";
                _upgrateButton.interactable = _coin.COIN >= priceUp && _slotFarm.FARM.Level < 5;
            }
            if (_slotCooling != null)
            {
                _upgrateButton.interactable = _slotCooling.Cooling.Level < 5 && _coin.COIN >= priceUp;
            }
        }

        public void RefreshCooling()
        {
            if (_slotCooling.Cooling.Level < 3)
            {
                priceUp = _coolingPack.GetCooling(_slotCooling.Cooling.Index + 1).Price;
            }
            Cooling cooling = _slotCooling.Cooling;
            if (_slotCooling.IsStoped)
            {
                _textButton.text = "Switch ON";
                ProgessBar(false);
            }
            else
            {
                _textButton.text = "Switch OFF";
                ProgessBar(true);
            }
            _name.text = cooling.Name;
            _parametr.text = "Cooling " + cooling.Index + " lvl.";
            _parametr7.text = "";
            _parametr6.text = "";
            _parametr5.text = cooling.CoolingTemperature + " Cº";
            _parametr4.text = "";
            _parametr2.text = "Consumption: " + cooling.Electricity + " Vt/h";
            _parametr3.text = "Strength: " + cooling.Strength + "/" + cooling.MaxStrength;
            if (5 > cooling.Index)
            {
                _priceUP.text = "Upgrade(" + _coolingPack.GetCooling(cooling.Index + 1).Price + "$)";
            }
            else
            {
                _priceUP.text = "Max lvl";
            }
        }

        //private void InInventory()
        //{
        //    if (_slotFarm != null)
        //    {
        //        _slotFarm.InInventory();
        //        _slotFarm = null;
        //    }
        //    else if (_slotCooling != null)
        //    {
        //        _slotCooling.InInventory();
        //        _slotCooling = null;
        //    }
        //    ClosePanel();
        //}

        public void Alert(string textKey)
        {
            _alertText.text = textKey;
            _alert.SetActive(true);
        }

        void Repair()
        {
            if (_slotFarm != null)
            {
                _repairPanel.OpenPanel(_slotFarm);
            }
            else if (_slotCooling != null)
            {
                _repairPanel.OpenPanel(_slotCooling);
            }
        }

        IEnumerator Open()
        {
            while (-610 > transform.localPosition.x)
            {
                transform.localPosition = new Vector2(Mathf.Clamp(transform.localPosition.x + Time.deltaTime * 2000, -1310, -610), transform.localPosition.y);
                yield return null;
            }
        }

        IEnumerator Close()
        {
            while (-1310 < transform.localPosition.x)
            {
                transform.localPosition = new Vector2(Mathf.Clamp(transform.localPosition.x - Time.deltaTime * 2000, -1310, -610), transform.localPosition.y);
                yield return null;
            }
        }
    }
}