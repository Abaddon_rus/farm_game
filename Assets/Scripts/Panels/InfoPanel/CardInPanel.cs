﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Game
{
    public class CardInPanel : ComponentInpanel, IPointerClickHandler, IDropHandler
    {
        [SerializeField]
        Image _icon;

        SlotCard _slotCard;

        Inventory _inventory;

        InfoPanel _infoPanel;

        private void Start()
        {
            _inventory = StaticStorage.GetLink<Inventory>("Inventory");
            _infoPanel = StaticStorage.GetLink<InfoPanel>("InfoPanel");
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (_slotCard.VideoCard.Index == 0)
            {
                if (PlantingComponents.VideoCard == null)
                {
                    StaticStorage.GetLink<Menu>("Menu").OpenInventory();
                    _inventory.VidecardInInventory();
                    DestroyDrag();
                }
                else
                {
                    if (_slotCard._slotFarm.STOP)
                    {
                        var pack = StaticStorage.GetLink<VideoCardPack>("VideoCardPack");
                        var minLevel = pack.GetVideoCard(PlantingComponents.VideoCard.Index).MinFarmLvl;
                        if (minLevel <= _slotCard._slotFarm.FARM.Level)
                        {
                            NetworkCore.Send("PlantCard", new DataPacket
                            {
                                v1 = _slotCard._slotFarm.ID,
                                v2 = PlantingComponents.VideoCard.ID,
                                v3 = _slotCard._numberSlot
                            }, (data) =>
                            {
                                Plant(Convert.ToBoolean(data));
                            });
                        }
                        else
                        {
                            _infoPanel.Alert("Нужно улучшение сервера!");
                            DestroyDrag();
                        }
                    }
                    else
                    {
                        _infoPanel.Alert("Сервер включен!");
                        DestroyDrag();
                    }
                }
            }
            else
            {
                _infoPanel._infoComponents.Show(_slotCard);
            }
        }

        void Plant(bool result)
        {
            if (result)
            {
                _slotCard.VideoCard = PlantingComponents.VideoCard;
                _inventory.Remove(PlantingComponents.VideoCard);
                _infoPanel.RefreshFarm();
                DestroyDrag();
            }
            else
            {
                _inventory.Remove(PlantingComponents.VideoCard);
                _infoPanel.RefreshFarm();
                DestroyDrag();
            }
        }

        public void Open(SlotCard slotCard)
        {
            _slotCard = slotCard;
            if (slotCard.VideoCard.Index != 0)
            {
                _slotCard = slotCard;
                _icon.sprite = _slotCard.VideoCard.Icon;
                _icon.color = new Color(1, 1, 1, 1);
            }
            else
            {
                _icon.color = new Color(1, 1, 1, 0);
            }
        }

        public void OnDrop(PointerEventData eventData)
        {
            if (_slotCard.VideoCard.Index == 0)
            {
                if (PlantingComponents.VideoCard == null)
                {
                    StaticStorage.GetLink<Menu>("Menu").OpenInventory();
                    _inventory.VidecardInInventory();
                    DestroyDrag();
                }
                else
                {
                    if (_slotCard._slotFarm.STOP)
                    {
                        var pack = StaticStorage.GetLink<VideoCardPack>("VideoCardPack");
                        var minLevel = pack.GetVideoCard(PlantingComponents.VideoCard.Index).MinFarmLvl;
                        if (minLevel <= _slotCard._slotFarm.FARM.Level)
                        {
                            NetworkCore.Send("PlantCard", new DataPacket
                            {
                                v1 = _slotCard._slotFarm.ID,
                                v2 = PlantingComponents.VideoCard.ID,
                                v3 = _slotCard._numberSlot
                            }, (data) =>
                            {
                                Plant(Convert.ToBoolean(data));
                            });
                        }
                        else
                        {
                            _infoPanel.Alert("Нужно улучшение сервера!");
                            DestroyDrag();
                        }
                    }
                    else
                    {
                        _infoPanel.Alert("Сервер включен!");
                        DestroyDrag();
                    }
                }
            }
            else
            {
                _infoPanel._infoComponents.Show(_slotCard);
                DestroyDrag();
            }
        }

        void DestroyDrag()
        {
            PlantingComponents.Clear();
            if (PlantingComponents.DragItem != null)
                Destroy(PlantingComponents.DragItem);
        }

        public bool Busy()
        {
            return _slotCard != null && _slotCard.VideoCard.Index != 0;
        }
    }
}