﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Game
{
    public class HardInPanel : ComponentInpanel, IPointerClickHandler, IDropHandler
    {
        [SerializeField]
        Image _icon;

        SlotHard _slotHard;

        Inventory _inventory;
        InfoPanel _infoPanel;

        private void Start()
        {
            _inventory = StaticStorage.GetLink<Inventory>("Inventory");
            _infoPanel = StaticStorage.GetLink<InfoPanel>("InfoPanel");
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (_slotHard.HARD.Index == 0)
            {
                if (PlantingComponents.Hard == null)
                {
                    StaticStorage.GetLink<Menu>("Menu").OpenInventory();
                    _inventory.HardInInventory();
                    DestroyDrag();
                }
                else
                {
                    if (_slotHard._slotFarm.STOP)
                    {
                        var pack = StaticStorage.GetLink<HardPack>("HardPack");
                        var minLevel = pack.GetHard(PlantingComponents.Hard.Index).MinFarmLvl;
                        if (minLevel <= _slotHard._slotFarm.FARM.Level)
                        {
                            NetworkCore.Send("PlantHard", new DataPacket
                            {
                                v1 = _slotHard._slotFarm.ID,
                                v2 = PlantingComponents.Hard.ID,
                                v3 = _slotHard._numberSlot
                            }, (data) =>
                            {
                                Plant(Convert.ToBoolean(data));
                            });
                        }
                        else
                        {
                            _infoPanel.Alert("Нужно улучшение сервера!");
                            DestroyDrag();
                        }
                    }
                    else
                    {
                        _infoPanel.Alert("Сервер включен!");
                        DestroyDrag();
                    }
                }
            }
            else
            {
                _infoPanel._infoComponents.Show(_slotHard);
            }
        }

        void Plant(bool result)
        {
            if (result)
            {
                _slotHard.HARD = PlantingComponents.Hard;
                _inventory.Remove(PlantingComponents.Hard);
                _infoPanel.RefreshFarm();
                DestroyDrag();

            }
        }

        public void Open(SlotHard slotHard)
        {
            _slotHard = slotHard;
            if (slotHard.HARD.Index != 0)
            {
                _icon.sprite = _slotHard.HARD.Icon;
                _icon.color = new Color(1, 1, 1, 1);
            }
            else
            {
                _icon.color = new Color(1, 1, 1, 0);
            }
        }

        public void OnDrop(PointerEventData eventData)
        {
            if (_slotHard.HARD.Index == 0)
            {
                if (PlantingComponents.Hard == null)
                {
                    StaticStorage.GetLink<Menu>("Menu").OpenInventory();
                    _inventory.HardInInventory();
                    DestroyDrag();
                }
                else
                {
                    if (_slotHard._slotFarm.STOP)
                    {
                        var pack = StaticStorage.GetLink<HardPack>("HardPack");
                        var minLevel = pack.GetHard(PlantingComponents.Hard.Index).MinFarmLvl;
                        if (minLevel <= _slotHard._slotFarm.FARM.Level)
                        {
                            NetworkCore.Send("PlantHard", new DataPacket
                            {
                                v1 = _slotHard._slotFarm.ID,
                                v2 = PlantingComponents.Hard.ID,
                                v3 = _slotHard._numberSlot
                            }, (data) =>
                            {
                                Plant(Convert.ToBoolean(data));
                            });
                        }
                        else
                        {
                            _infoPanel.Alert("Нужно улучшение сервера!");
                            DestroyDrag();
                        }
                    }
                    else
                    {
                        _infoPanel.Alert("Сервер включен!");
                        DestroyDrag();
                    }
                }
            }
            else
            {
                _infoPanel._infoComponents.Show(_slotHard);
                DestroyDrag();
            }
        }

        void DestroyDrag()
        {
            PlantingComponents.Clear();
            if (PlantingComponents.DragItem != null)
                Destroy(PlantingComponents.DragItem);
        }

        public bool Busy()
        {
            return _slotHard != null && _slotHard.HARD.Index != 0;
        }
    }
}