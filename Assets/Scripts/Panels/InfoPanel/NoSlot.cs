﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class NoSlot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField]
    Text _text;

    public void OnPointerEnter(PointerEventData eventData)
    {
        _text.gameObject.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        _text.gameObject.SetActive(false);
    }
}
