﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Alert : MonoBehaviour
{
    [SerializeField]
    Image _bg;
    [SerializeField]
    Vector2 _alpha = new Vector2(0.2f, 0.35f);

    private void OnEnable()
    {
        StartCoroutine(AlertCorut());
    }

    IEnumerator AlertCorut()
    {
        for (int x = 0; x < 2; x++)
        {
            for (float i = _alpha.x; i <= _alpha.y; i += Time.deltaTime)
            {
                _bg.color = new Color(1, 0, 0, i);
                yield return new WaitForSecondsRealtime(0.01f);
            }
            for (float i = _alpha.y; i >= _alpha.x; i -= Time.deltaTime)
            {
                _bg.color = new Color(1, 0, 0, i);
                yield return new WaitForSecondsRealtime(0.01f);
            }
        }
        gameObject.SetActive(false);
    }
}
