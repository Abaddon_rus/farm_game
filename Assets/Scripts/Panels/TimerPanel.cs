﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;

namespace Game
{
    public class TimerPanel : MonoBehaviour, IUpdatable
    {
        [SerializeField]
        TextMeshProUGUI _timer;

        int _timeleft;

        Transform _transform;

        [SerializeField]
        Vector3 _offset;

        public TimerPanel Transform(Transform transform)
        {
            _transform = transform;
            return this;
        }

        void IUpdatable.OnUpdate(float deltaTime)
        {
            if (_transform != null)
            {
                var camera = Camera.main;
                var worldPosition = _transform.transform.position + _offset;
                var viewportPosition = camera.WorldToViewportPoint(worldPosition);
                var rt = transform as RectTransform;

                rt.anchorMin = viewportPosition;
                rt.anchorMax = viewportPosition;
            }
        }

        public bool TimerStart(int timeEnd, Action action)
        {
            if (_transform != null)
            {
                StaticStorage.UpdateCore.Add(this);
            }
            _timeleft = timeEnd - (int)DateTimeOffset.UtcNow.ToUnixTimeSeconds();
            if (_timeleft > 0)
            {
                _timer.text = _timeleft.ToTime();
                gameObject.SetActive(true);
                StartCoroutine(Timer(action));
                return true;
            }
            else
            {
                return false;
            }
        }

        IEnumerator Timer(Action action)
        {
            while (_timeleft > 0)
            {
                yield return new WaitForSecondsRealtime(1);
                _timeleft--;

                _timer.text = _timeleft.ToTime();
            }
            action();
            gameObject.SetActive(false);
        }

        private void OnDisable()
        {
            StaticStorage.UpdateCore.Remove(this);
        }

        private void OnDestroy()
        {
            StopAllCoroutines();
        }
    }
}