﻿using System;
using LevelEditor;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class BuyPlant : MonoBehaviour
    {
        [SerializeField]
        EditorMapRoot _editorMapRoot;

        [SerializeField]
        Text _count;
        [SerializeField]
        Text _price;
        [SerializeField]
        Text _priceSum;

        [SerializeField]
        Button _buyButton;
        [SerializeField]
        Button _closeButton;

        float _priceTemp;

        int _xOffset;
        int _yOffset;

        private Coin _coin;

        private void Start()
        {
            _coin = StaticStorage.GetLink<Coin>("Coin");
            StaticStorage.AddLink("BuyPlant", this);
        }

        private void OnEnable()
        {
            _buyButton.onClick.AddListener(Buy);
            _closeButton.onClick.AddListener(Cancel);
        }

        private void OnDisable()
        {
            _buyButton.onClick.RemoveListener(Buy);
            _closeButton.onClick.RemoveListener(Cancel);
        }

        private void Update()
        {
            _buyButton.interactable = _priceTemp <= _coin.COIN;
        }

        public void Show(int count, float price, float priceSum, Vector2Int offsec)
        {
            _xOffset = offsec.x;
            _yOffset = offsec.y;
            _count.text = count.ToString();
            _price.text = price.ToString();
            _priceSum.text = priceSum.ToString();
            gameObject.SetActive(true);
            _priceTemp = priceSum;
        }

        void Buy()
        {
            NetworkCore.Send("BuyCell", new DataPacket
            {
                v1 = _xOffset,
                v2 = _yOffset
            }, (data) =>
            {
                SetCell(Convert.ToBoolean(data));
            });
        }

        void SetCell(bool result)
        {
            if (result)
            {
                _coin.COIN -= _priceTemp;
                _editorMapRoot.Resize(_editorMapRoot._xCount + _xOffset, _editorMapRoot._yCount + _yOffset);
                Cancel();
            }
        }

        void Cancel()
        {
            gameObject.SetActive(false);
        }
    }
}