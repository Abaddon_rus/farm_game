﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class PlayerLevel : MonoBehaviour
    { 
        static PlayerLevel _pl;

        int _lvl = 1;

        int _exp = 0;

        static public int Lvl
        {
            set
            {
                _pl._lvl = value;
                _pl._textLvl.text = _pl._lvl.ToString();
            }
            get
            {
                return _pl._lvl;
            }
        }

        static public int Exp
        {
            set
            {
                _pl.PullExp(value);
                int i = _pl.HowNeedExp();
                _pl._textExp.text = _pl._exp + "/" + i;
                _pl._progress.fillAmount = 1f / i * _pl._exp;
            }
            get
            {
                return _pl._exp;
            }
        }

        [SerializeField]
        TextMeshProUGUI _textLvl;

        [SerializeField]
        TextMeshProUGUI _textExp;

        [SerializeField]
        Image _progress;

        private void Awake()
        {
            _pl = this;
        }

        void Start()
        {
            Lvl = _lvl;
        }

        void PullExp(int exp)
        {
            while (exp > 0)
            {
                if (_exp + exp > HowNeedExp())
                {
                    exp -= HowNeedExp() - _exp;
                    Lvl++;
                    _exp = 0;
                }
                else
                {
                    _exp += exp;
                    exp = 0;
                }
            }
        }

        int HowNeedExp()
        {
            if (_lvl == 1)
            {
                return 30;
            }
            else if (_lvl == 2)
            {
                return 200;
            }
            else
            {
                return (int)(200 * Mathf.Pow(1.26f, _lvl - 2));
            }
        }
    }

}