﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class BuyFarm : MonoBehaviour
    {
        [SerializeField]
        Button _button;

        [SerializeField]
        Coin _coin;

        private Plant _plant;

        private void OnEnable()
        {
            _button.onClick.AddListener(Buy);
        }

        private void OnDisable()
        {
            _button.onClick.RemoveListener(Buy);
        }

        private void Update()
        {
            _button.interactable = _coin.COIN >= 10;
            if (_plant != null)
            {
                var camera = Camera.main;
                var worldPosition = _plant.transform.position + new Vector3(0, 2, 0);
                var viewportPosition = camera.WorldToViewportPoint(worldPosition);
                var rt = transform as RectTransform;

                rt.anchorMin = viewportPosition;
                rt.anchorMax = viewportPosition;
            }
        }

        private void Buy()
        {
            NetworkCore.Send("BuyFarm", new DataPacket
            {
                v1 = _plant._positionXY.x,
                v2 = _plant._positionXY.y
            }, (data) => {
                PlantFarm(Convert.ToInt32(data));
                PlayerLevel.Exp = 20;
            });
        }

        public void PlantFarm(int result)
        {
            if (result > 0)
            {
                Farm farm = StaticStorage.GetLink<FarmPack>("FarmPack").Farms[1].Clone();
                farm.ID = result;
                _plant.Planting(farm, 2, 2, 1, null);
                _coin.CheckCoins();
                gameObject.SetActive(false);
            }
        }

        public void Show(Plant plant)
        {
            _plant = plant;
            var camera = Camera.main;
            var worldPosition = _plant.transform.position + new Vector3(0, 2, 0);
            var viewportPosition = camera.WorldToViewportPoint(worldPosition);
            var rt = transform as RectTransform;

            rt.anchorMin = viewportPosition;
            rt.anchorMax = viewportPosition;
            gameObject.SetActive(true);
        }
    }
}