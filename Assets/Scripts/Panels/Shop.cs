﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class Shop : MonoBehaviour
    {
        [SerializeField]
        GameObject _plaseOfObjects;
        //[SerializeField]
        //FarmPack _farmPack;
        [SerializeField]
        VideoCardPack _videoCardPack;
        [SerializeField]
        CoolerPack _coolerPack;
        [SerializeField]
        HardPack _hardPack;
        [SerializeField]
        OtherPack _otherPack;
        //[SerializeField]
        //CoolingPack _coolingPack;
        [SerializeField]
        ObjectInShop _prefabObjectInShop;
        [SerializeField]
        ObjectInShopOther _prefabObjectInShopOther;
        [SerializeField]
        Transform _bgMenuButtons;
        [SerializeField]
        Menu _menu;

        //[SerializeField]
        //Button _farmInShopButton;
        //[SerializeField]
        //Button _coolingInShopButton;
        [SerializeField]
        Button _videcardInShopButton;
        [SerializeField]
        Button _coolerInShopButton;
        [SerializeField]
        Button _hardInShopButton;
        [SerializeField]
        Button _otherInventoryButton;
        [SerializeField]
        Text _title;

        private void Awake()
        {
            for (int i = 0; i < _videoCardPack.Cards.Count; i++)
            {
                ObjectInShop objectInShop = Instantiate(_prefabObjectInShop, _plaseOfObjects.transform);
                objectInShop.Instant(_videoCardPack.GetVideoCard(i + 1));
            }
        }

        private void OnEnable()
        {
            //_farmInShopButton.onClick.AddListener(FarmInShop);
            //_coolingInShopButton.onClick.AddListener(CoolingInShop);
            _videcardInShopButton.onClick.AddListener(VidecardInShop);
            _coolerInShopButton.onClick.AddListener(CoolerInShop);
            _hardInShopButton.onClick.AddListener(HardInShop);
            _otherInventoryButton.onClick.AddListener(OtherInShop);
            _bgMenuButtons.localPosition = new Vector3(123.48f, 0, 0);
        }

        private void OnDisable()
        {
            //_farmInShopButton.onClick.RemoveListener(FarmInShop);
            _videcardInShopButton.onClick.RemoveListener(VidecardInShop);
            _coolerInShopButton.onClick.RemoveListener(CoolerInShop);
            _hardInShopButton.onClick.RemoveListener(HardInShop);
            _otherInventoryButton.onClick.RemoveListener(OtherInShop);
        }

        void Clear()
        {
            _menu.OpenPanel();
            _menu.ShopFocus();
            ObjectInShop[] objectInShops = _plaseOfObjects.GetComponentsInChildren<ObjectInShop>();
            for (int i = 0; i < objectInShops.Length; i++)
                Destroy(objectInShops[i].gameObject);
            ObjectInShopOther[] ObjectInShopOther = _plaseOfObjects.GetComponentsInChildren<ObjectInShopOther>();
            for (int i = 0; i < ObjectInShopOther.Length; i++)
                Destroy(ObjectInShopOther[i].gameObject);
        }

        public void Check()
        {
            if (_title.text == "Videocards")
            {
                VidecardInShop();
            }
            else if (_title.text == "Coolers")
            {
                CoolerInShop();
            }
            else if (_title.text == "SSD")
            {
                HardInShop();
            }
            else
            {
                OtherInShop();
            }
        }

        void VidecardInShop()
        {
            Clear();
            _title.text = "Videocards";
            for (int i = 0; i < _videoCardPack.Cards.Count; i++)
            {
                if (_videoCardPack.Cards[i].Invisible == false)
                {
                    ObjectInShop objectInShop = Instantiate(_prefabObjectInShop, _plaseOfObjects.transform);
                    objectInShop.Instant(_videoCardPack.GetVideoCard(i + 1));
                }
            }
        }

        void CoolerInShop()
        {
            Clear();
            _title.text = "Coolers";
            for (int i = 0; i < _coolerPack.Cooler.Count; i++)
            {
                if (_coolerPack.Cooler[i].Invisible == false)
                {
                    ObjectInShop objectInShop = Instantiate(_prefabObjectInShop, _plaseOfObjects.transform);
                    objectInShop.Instant(_coolerPack.GetCooler(i + 1));
                }
            }
        }

        void HardInShop()
        {
            Clear();
            _title.text = "SSD";
            for (int i = 0; i < _hardPack.Hard.Count; i++)
            {
                if (_hardPack.Hard[i].Invisible == false)
                {
                    ObjectInShop objectInShop = Instantiate(_prefabObjectInShop, _plaseOfObjects.transform);
                    objectInShop.Instant(_hardPack.GetHard(i + 1));
                }
            }
        }

        void OtherInShop()
        {
            Clear();
            _title.text = "Other";
            for (int i = 0; i < _otherPack.Other.Count; i++)
            {
                ObjectInShopOther objectInShop = Instantiate(_prefabObjectInShopOther, _plaseOfObjects.transform);
                objectInShop.Instant(_otherPack.Other[i]);
            }
        }

        //void FarmInShop()
        //{
        //    Clear();
        //    for (int i = 0; i < _farmPack.Farms.Count; i++)
        //    {
        //        ObjectInShop objectInShop = Instantiate(_prefabObjectInShop, _plaseOfObjects.transform);
        //        objectInShop.Instant(_farmPack.GetFarm(i + 1));
        //    }
        //}

        //void CoolingInShop()
        //{
        //    Clear();
        //    for (int i = 0; i < _coolingPack.Cooling.Count; i++)
        //    {
        //        ObjectInShop objectInShop = Instantiate(_prefabObjectInShop, _plaseOfObjects.transform);
        //        objectInShop.Instant(_coolingPack.GetCooling(i + 1));
        //    }
        //}

    }
}