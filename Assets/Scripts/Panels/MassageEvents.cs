﻿using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class MassageEvents : MonoBehaviour
    {
        //[SerializeField]
        //Text _crashText;
        [SerializeField]
        Text _coinsText;
        [SerializeField]
        Text _electricityText;
        [SerializeField]
        Button _button;

        public float coins;
        public float electrisity;

        public void Open()
        {
            _button.onClick.AddListener(Close);
            gameObject.SetActive(true);
            _coinsText.text = "Заработано: " + coins + " Coins";
            _electricityText.text = "Потрачено электричества: " + electrisity + " Вт";
        }

        void Close()
        {
            gameObject.SetActive(false);
            _button.onClick.RemoveListener(Close);
        }
    }
}