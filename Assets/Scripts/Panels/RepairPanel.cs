﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class RepairPanel : MonoBehaviour
    {
        [SerializeField]
        TextMeshProUGUI _countInstrument;
        [SerializeField]
        TextMeshProUGUI _priceRepair;
        [SerializeField]
        Button _buttonManual;
        [SerializeField]
        Button _buttonService;

        [SerializeField]
        OtherPack _otherPackInventory;

        [SerializeField]
        Inventory _inventory;

        [SerializeField]
        Coin _coin;

        Action _action;

        Packs _packs;

        int _typeComponent;

        private void Awake()
        {
            StaticStorage.AddLink("RepairPanel", this);
            gameObject.SetActive(false);
            transform.localPosition = new Vector3(0, 0);
        }

        private void OnEnable()
        {
            _buttonManual.onClick.AddListener(Manual);
            _buttonService.onClick.AddListener(Service);
        }

        private void OnDisable()
        {
            _buttonManual.onClick.RemoveListener(Manual);
            _buttonService.onClick.RemoveListener(Service);
        }

        public void OpenPanel(Packs packs)
        {
            _packs = packs;

            if (_packs is VideoCard)
            {
                _typeComponent = 1;
            }
            else if (_packs is Cooler)
            {
                _typeComponent = 2;
            }
            else if (_packs is Hard)
            {
                _typeComponent = 3;
            }
            else
            {
                Debug.Log("ЧТО ЗА!?(В панель ремонта прилетела неведомая херня)");
            }
            _action = _inventory.Check;
            ShowData();
        }

        public void OpenPanel(SlotFarm slotFarm)
        {
            _packs = slotFarm.FARM;

            _typeComponent = 4;
            _action = slotFarm.Repair;
            ShowData();
        }

        public void OpenPanel(SlotCooling slotCooling)
        {
            _packs = slotCooling.Cooling;

            _typeComponent = 5;
            _action = slotCooling.Repair;
            ShowData();
        }

        public void ClosePanel()
        {
            gameObject.SetActive(false);
        }

        void ShowData()
        {
            _countInstrument.text = _otherPackInventory.Other[1].Count.ToString();
            _priceRepair.text = _packs.PriceRepair.ToString() + "$";
            gameObject.SetActive(true);
        }

        private void Service()
        {
            float priceRepair = _packs.PriceRepair;
            Packs pack = _packs;
            NetworkCore.Send("ServiceRepair", new DataPacket()
            {
                v1 = _typeComponent,
                v2 = _packs.ID
            }, (data) =>
            {
                if (Convert.ToInt32(data) > 0)
                {
                    pack.Strength = _packs.MaxStrength;
                    pack.TimeEndRepair = Convert.ToInt32(data);
                    ClosePanel();
                    _action();
                    _coin.COIN -= priceRepair;
                }
            });
        }

        private void Manual()
        {
            NetworkCore.Send("ManualRepair", new DataPacket()
            {
                v1 = _typeComponent,
                v2 = _packs.ID
            }, (data) =>
            {
                DataManualRepair DataManualRepair = JsonUtility.FromJson<DataManualRepair>(data);
                if (DataManualRepair.Query)
                {
                    if (DataManualRepair.Repair)
                    {
                        _packs.Strength = _packs.MaxStrength;
                        _packs.TimeEndRepair = DataManualRepair.TimeEndRepair;
                    }
                    else
                    {
                        _packs.Strength = 0;
                        _packs.TimeEndRepair = DataManualRepair.TimeEndRepair;
                    }
                    _otherPackInventory.Other[1].Count--;
                    ClosePanel();
                    _action();
                }
            });
        }
    }

    public class DataManualRepair : Packet
    {
        public bool Query;
        public bool Repair;
        public int TimeEndRepair;
    }
}