﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class EventManager : MonoBehaviour
    {
        //        [SerializeField]
        //        MassageEvents _massageEvents;

        //        private Electricity _electricity;
        //        private Temperature _temperature;

        //        public DataForSave Data
        //        {
        //            set
        //            {
        //                Events(value);
        //            }
        //        }

        //        private void Awake()
        //        {
        //            StaticStorage.AddLink("EventManager", this);
        //        }

        //        private void Start()
        //        {
        //            _electricity = StaticStorage.GetLink<Electricity>("Electricity");
        //            _temperature = StaticStorage.GetLink<Temperature>("Temperature");
        //        }

        //        void Events(DataForSave data)
        //        {
        //            long elapsedTime = (DateTime.Now.Ticks - data.Time) / 10000000;
        //            Debug.Log(elapsedTime);
        //            int hours = (int)elapsedTime / 3600;
        //            for (int y = 0; y < hours; y++)
        //            {
        //                for (int i = 0; i < data.Farms.Count; i++)
        //                {
        //                    Farm farm = data.Farms[i];
        //                    float electricity = Electricity(farm);
        //                    if (_electricity.ELECTRICITY / electricity > 1 && farm.Crash == false && farm.Stop == false)
        //                    {
        //                        _electricity.ELECTRICITY -= electricity;
        //                        //_massageEvents.electrisity += electricity;
        //                        farm.Strength--;

        //                        Cards(farm);

        //                        Coolers(farm);
        //                        for (int h = 0; h < farm.SlotsHard.Count; h++)
        //                        {
        //                            SlotHard slotHard = farm.SlotsHard[h];
        //                            if (slotHard.HARD.Index != 0 && slotHard.HARD.Strength != 0)
        //                            {
        //                                slotHard.HARD.Strength = Mathf.Clamp((int)Mathf.Round(slotHard.HARD.Strength - (1 + (_temperature._temperature - 25))), 0, int.MaxValue);
        //                            }
        //                        }
        //                    }
        //                }
        //                for (int i = 0; i < data.Coolings.Count; i++)
        //                {
        //                    if (data.Coolings[i].Strength != 0)
        //                    {
        //                        data.Coolings[i].Strength--;
        //                        _electricity.ELECTRICITY -= data.Coolings[i].Electricity;
        //                        _temperature.Cooling = data.Coolings[i].CoolingTemperature;
        //                    }
        //                }
        //            }
        //            RemainingTime(elapsedTime, data);
        //            //_massageEvents.Open();
        //        }

        //        private float Electricity(Farm farm)
        //        {
        //            float electricity = 0;
        //            float electricityCard = 0;
        //            float electricityCooler = 0;
        //            float electricityHard = 0;
        //            for (int x = 0; x < farm.SlotsCards.Count; x++)
        //            {
        //                if (farm.SlotsCards[x].VIDEOCARD != null)
        //                {
        //                    electricityCard += farm.SlotsCards[x].VIDEOCARD.Electricity;
        //                }
        //            }
        //            for (int x = 0; x < farm.SlotsCoolers.Count; x++)
        //            {
        //                if (farm.SlotsCoolers[x].COOLER != null)
        //                {
        //                    electricityCooler += farm.SlotsCoolers[x].COOLER.Electricity;
        //                }
        //            }
        //            for (int x = 0; x < farm.SlotsHard.Count; x++)
        //            {
        //                if (farm.SlotsHard[x].HARD != null)
        //                {
        //                    electricityHard += farm.SlotsHard[x].HARD.Electricity;
        //                }
        //            }
        //            electricity += farm.Electricity + electricityCard + electricityCooler + electricityHard;

        //            return electricity;
        //        }

        //        private void Cards(Farm farm)
        //        {
        //            for (int a = 0; a < farm.SlotsCards.Count; a++)
        //            {
        //                if (farm.Crash != true || farm.Stop != true)
        //                {
        //                    if (farm.SlotsCards[a].VIDEOCARD.Index != 0 && farm.SlotsCards[a].VIDEOCARD.Strength != 0)
        //                    {
        //                        ConverseCard(farm.SlotsCards[a], farm);
        //                    }
        //                }
        //            }
        //        }

        //        private void ConverseCard(SlotCard slotCard, Farm farm)
        //        {
        //            VideoCard videoCard = slotCard.VIDEOCARD;
        //            for (int i = 0; i < farm.SlotsCards.Count; i++)
        //            {
        //                if (farm.SlotsHard[i].HARD.Index != 0 && farm.SlotsHard[i].HARD.Coins < farm.SlotsHard[i].HARD.Volume)
        //                {
        //                    farm.SlotsHard[i].COIN = videoCard.Income;
        //                    //_massageEvents.coins += videoCard.Income;
        //                    break;
        //                }
        //            }
        //            if (Mathf.Round(slotCard._efficiency) < 50)
        //            {
        //                int rnd = UnityEngine.Random.Range(0, 101);
        //                if (rnd > 60 - (50 - Mathf.Round(slotCard._efficiency)))
        //                {
        //                    farm.Crash = true;
        //                    return;
        //                }
        //            }
        //            if (farm.SlotsCoolers[slotCard._numberSlot].COOLER.Index != 0)
        //            {
        //                slotCard._heatDissipation = videoCard.HeatDissipation + farm.SlotsCoolers[slotCard._numberSlot].COOLER.Efficiency;
        //            }
        //            slotCard._temperature = Mathf.Clamp(slotCard._temperature + slotCard._heatDissipation, videoCard.Temperature, videoCard.TemperatureMax);
        //            videoCard.Strength = Mathf.Clamp((int)Mathf.Round(videoCard.Strength - (1 + (slotCard._temperature - videoCard.Temperature))), 0, int.MaxValue);
        //            videoCard.PriceRepair = Mathf.Clamp((float)System.Math.Round((slotCard._price - (slotCard._price - videoCard.Income * (slotCard.VIDEOCARD.MaxStrength - videoCard.Strength))), 2), 0.1f, slotCard._price);
        //        }

        //        private void Coolers(Farm farm)
        //        {
        //            for (int a = 0; a < farm.SlotsCoolers.Count; a++)
        //            {
        //                if (farm.SlotsCoolers[a].COOLER.Index != 0 && farm.SlotsCoolers[a].COOLER.Strength != 0)
        //                {
        //                    ConverseCooler(farm.SlotsCoolers[a]);
        //                }
        //            }
        //        }

        //        private void ConverseCooler(SlotCooler slotCooler)
        //        {
        //            Cooler cooler = slotCooler.COOLER;
        //            cooler.PriceRepair = Mathf.Clamp((float)System.Math.Round(cooler.Price - cooler.Price * ((float)cooler.Strength / (float)cooler.MaxStrength), 2), 0.1f, cooler.Price);
        //            cooler.Strength = Mathf.Clamp((int)Mathf.Round(cooler.Strength - (1 + (_temperature._temperature - 25))), 0, int.MaxValue);
        //        }

        //        private void RemainingTime(long elapsedTime, DataForSave data)
        //        {
        //            for (int i = 0; i < data.Farms.Count; i++)
        //            {
        //                Farm farm = data.Farms[i];
        //                float electricity = Electricity(farm);
        //                if (_electricity.ELECTRICITY / electricity > 1 && farm.Crash == false && farm.Stop == false)
        //                {
        //                    farm.Progress += elapsedTime;
        //                    if (farm.Progress > 3600)
        //                    {
        //                        farm.Progress -= 3600;
        //                        _electricity.ELECTRICITY -= electricity;
        //                        //_massageEvents.electrisity += electricity;
        //                        farm.Strength--;
        //                    }
        //                }
        //                for (int a = 0; a < farm.SlotsCards.Count; a++)
        //                {
        //                    SlotCard slotCard = farm.SlotsCards[a];
        //                    if (slotCard.VIDEOCARD.Index != 0 && slotCard.VIDEOCARD.Strength != 0)
        //                    {
        //                        slotCard.VIDEOCARD.Progress += elapsedTime;
        //                        if (slotCard.VIDEOCARD.Progress > 3600)
        //                        {
        //                            slotCard.VIDEOCARD.Progress -= 3600;
        //                            ConverseCard(slotCard, farm);
        //                            for (int h = 0; h < farm.SlotsHard.Count; h++)
        //                            {
        //                                if (farm.SlotsHard[h].HARD.Coins < farm.SlotsHard[h].HARD.Volume)
        //                                {
        //                                    farm.SlotsHard[h].COIN = slotCard.VIDEOCARD.Progress * slotCard.VIDEOCARD.Income / 3600;
        //                                    //_massageEvents.coins += slotCard.VIDEOCARD.Progress * slotCard.VIDEOCARD.Income / 3600;
        //                                    break;
        //                                }
        //                            }
        //                        }
        //                        else
        //                        {
        //                            for (int h = 0; h < farm.SlotsHard.Count; h++)
        //                            {
        //                                if (farm.SlotsHard[h].HARD.Coins < farm.SlotsHard[h].HARD.Volume)
        //                                {
        //                                    farm.SlotsHard[h].COIN = elapsedTime * farm.SlotsCards[a].VIDEOCARD.Income / 3600;
        //                                    //_massageEvents.coins += elapsedTime * farm.SlotsCards[a].VIDEOCARD.Income / 3600;
        //                                    break;
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //                for (int a = 0; a < farm.SlotsCoolers.Count; a++)
        //                {
        //                    SlotCooler slotCooler = farm.SlotsCoolers[a];
        //                    if (slotCooler.COOLER.Index != 0 && slotCooler.COOLER.Strength != 0)
        //                    {
        //                        slotCooler.COOLER.Progress += elapsedTime;
        //                        if (slotCooler.COOLER.Progress > 3600)
        //                        {
        //                            slotCooler.COOLER.Progress -= 3600;
        //                            ConverseCooler(slotCooler);
        //                        }
        //                    }
        //                }
        //                for (int a = 0; a < farm.SlotsHard.Count; a++)
        //                {
        //                    SlotHard slotHard = farm.SlotsHard[a];
        //                    if (slotHard.HARD.Index != 0 && slotHard.HARD.Strength != 0)
        //                    {
        //                        slotHard.HARD.Progress += elapsedTime;
        //                        if (slotHard.HARD.Progress > 3600)
        //                        {
        //                            slotHard.HARD.Progress -= 3600;
        //                            slotHard.HARD.Strength = Mathf.Clamp((int)Mathf.Round(slotHard.HARD.Strength - (1 + (_temperature._temperature - 25))), 0, int.MaxValue);
        //                        }
        //                    }
        //                }
        //            }
        //            for (int i = 0; i < data.Coolings.Count; i++)
        //            {
        //                if (data.Coolings[i].Strength != 0)
        //                {
        //                    data.Coolings[i].Progress += elapsedTime;
        //                    if (data.Coolings[i].Progress > 3600)
        //                    {
        //                        data.Coolings[i].Progress -= 3600;
        //                        data.Coolings[i].Strength--;
        //                        _electricity.ELECTRICITY -= data.Coolings[i].Electricity;
        //                        _temperature.Cooling = data.Coolings[i].CoolingTemperature;
        //                    }
        //                }
        //            }
        //        }
    }
}


