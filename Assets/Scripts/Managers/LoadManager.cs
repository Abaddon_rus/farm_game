﻿using LevelEditor;
using PacketLib;
using System;
using UnityEngine;

namespace Game
{
    public class LoadManager : MonoBehaviour
    {
        [SerializeField]
        EditorMapRoot _editorMapRoot;
        [SerializeField]
        Coin _coin;
        [SerializeField]
        Electricity _electricity;
        [SerializeField]
        Temperature _temperature;
        [SerializeField]
        Workplace _workplace;
        [SerializeField]
        CoinsIncome _coinsIncome;
        [SerializeField]
        Internet _internet;
        [SerializeField]
        DataCenter _dataCenter;

        [SerializeField]
        VideoCardPack _videoCardPackInventory;
        [SerializeField]
        CoolerPack _coolerPackInventory;
        [SerializeField]
        HardPack _hardPackInventory;
        [SerializeField]
        OtherPack _otherPackInventory;

        DataProfile _dp;

        private void Awake()
        {
            StaticStorage.AddLink("LoadManager", this);
        }

        public void Load()
        {
            LoadProfile();
            _workplace.LoadWorkPlace();
        }

        void LoadProfile()
        {
            NetworkCore.Send("LoadProfile", new Packet(), (data) =>
            {
                _dp = JsonUtility.FromJson<DataProfile>(data);
                StaticStorage.Prifile = _dp;
                _editorMapRoot.Resize(_dp.RoomX, _dp.RoomY);
                PlayerLevel.Lvl = _dp.Level;
                PlayerLevel.Exp = _dp.Exp;
                _coin.COIN = _dp.Coins;
                _coinsIncome.Income = _dp.CoinsIncome;
                _internet.ChangeTimeEndInternet(_dp.TimeStartInternet + 30 * 24 * 3600);
                _temperature.TEMPERATURE = _dp.Temperature;
                _temperature._emergencyCoolingStart = _dp.EmergencyCoolingStart;
                _electricity.ELECTRICITY = _dp.Electricity;
                _otherPackInventory.Other[0].Count = _dp.Extinguisher;
                _otherPackInventory.Other[1].Count = _dp.Instruments;
                _otherPackInventory.Other[2].Count = _dp.EmergencyCooling;
                if (_dp.DataCenterID > 0)
                {
                    LoadDataCenter();
                }
                LoadCooling();
            });
        }

        private void LoadDataCenter()
        {
            NetworkCore.Send("LoadDataCenter", new DataPacket()
            {
                v1 = _dp.DataCenterID
            }, (data) =>
            {
                DCData dcd = JsonUtility.FromJson<DCData>(data);
                _dataCenter.DCData = dcd;
            });
        }

        private void LoadInventory()
        {
            NetworkCore.Send("LoadInventory", new Packet(), (data) =>
            {
                ComponentPlayerPacket component = JsonUtility.FromJson<ComponentPlayerPacket>(data);
                if (component.CardData != null)
                {
                    for (int i = 0; i < component.CardData.Length; i++)
                    {
                        CardData cardData = component.CardData[i];
                        VideoCard card = StaticStorage.GetLink<VideoCardPack>("VideoCardPack").GetVideoCard(cardData.ComponentID);
                        _videoCardPackInventory.Cards.Add(new VideoCard
                        {
                            ID = cardData.id,
                            Index = cardData.ComponentID,
                            Name = card.Name,
                            Icon = card.Icon,
                            Price = card.Price,
                            PriceRepair = card.PriceRepair,
                            Strength = cardData.Strenght,
                            MaxStrength = card.MaxStrength,
                            Electricity = card.Electricity,
                            Progress = cardData.Progress,
                            Level = card.Level,
                            Income = cardData.Income,
                            Temperature = cardData.Temperature,
                            TemperatureMax = card.TemperatureMax,
                            HeatDissipation = card.HeatDissipation,
                            TimeEndRepair = cardData.TimeEndRepair,
                            CountRepairs = cardData.CountRepair,
                            MinFarmLvl = card.MinFarmLvl
                        });
                    }
                }
                if (component.CoolerData != null)
                {
                    for (int i = 0; i < component.CoolerData.Length; i++)
                    {
                        CoolerData coolerData = component.CoolerData[i];
                        Cooler cooler = StaticStorage.GetLink<CoolerPack>("CoolerPack").GetCooler(coolerData.ComponentID);
                        _coolerPackInventory.Cooler.Add(new Cooler
                        {
                            ID = coolerData.id,
                            Index = coolerData.ComponentID,
                            Name = cooler.Name,
                            Icon = cooler.Icon,
                            Price = cooler.Price,
                            PriceRepair = coolerData.PriceRepair,
                            Strength = coolerData.Strenght,
                            MaxStrength = cooler.MaxStrength,
                            Electricity = cooler.Electricity,
                            Progress = coolerData.Progress,
                            Efficiency = cooler.Efficiency,
                            Size = cooler.Size,
                            Level = 1,
                            TimeEndRepair = coolerData.TimeEndRepair,
                            CountRepairs = coolerData.CountRepair
                        });
                    }
                }
                if (component.HardData != null)
                {
                    for (int i = 0; i < component.HardData.Length; i++)
                    {
                        HardData hardData = component.HardData[i];
                        Hard hard = StaticStorage.GetLink<HardPack>("HardPack").GetHard(hardData.ComponentID);
                        _hardPackInventory.Hard.Add(new Hard
                        {
                            ID = hardData.id,
                            Index = hardData.ComponentID,
                            Name = hard.Name,
                            Icon = hard.Icon,
                            Price = hard.Price,
                            PriceRepair = hardData.PriceRepair,
                            Strength = hardData.Strenght,
                            MaxStrength = hard.MaxStrength,
                            Electricity = hard.Electricity,
                            Progress = hardData.Progress,
                            Volume = hard.Volume,
                            Coins = hardData.Coins,
                            Level = 1,
                            TimeEndRepair = hardData.TimeEndRepair,
                            CountRepairs = hardData.CountRepair
                        });
                    }
                }
            });
        }

        private void LoadCooling()
        {
            NetworkCore.Send("LoadCoolings", new Packet(), (data) =>
            {
                CoolingPlayerPacket coolings = JsonUtility.FromJson<CoolingPlayerPacket>(data);
                if (coolings.CoolingData != null)
                {
                    for (int i = 0; i < coolings.CoolingData.Length; i++)
                    {
                        CoolingData coolingData = coolings.CoolingData[i];
                        Cooling cooling = StaticStorage.GetLink<CoolingPack>("CoolingPack").GetCooling(coolingData.CoolingID).Clone();
                        _editorMapRoot._coolingPlant.Find(x => x._number == coolingData.NumberSlot).Planting(new Cooling
                        {
                            ID = coolingData.ID,
                            Name = cooling.Name,
                            Icon = cooling.Icon,
                            CoolingTemperature = cooling.CoolingTemperature,
                            Electricity = cooling.Electricity,
                            Index = coolingData.CoolingID,
                            MaxStrength = cooling.MaxStrength,
                            NumberSlot = coolingData.NumberSlot,
                            Price = cooling.Price,
                            PriceRepair = coolingData.PriceRepair,
                            Progress = coolingData.Progress,
                            Strength = coolingData.Strenght,
                            Level = coolingData.Level,
                            Stop = coolingData.Stop,
                            TimeEndRepair = coolingData.TimeEndRepair
                        });
                    }
                }
                LoadFarms();
            });

        }

        private void LoadFarms()
        {
            NetworkCore.Send("LoadFarms", new Packet(), (data) =>
            {
                FarmPlayerPacket farms = JsonUtility.FromJson<FarmPlayerPacket>(data);
                if (farms.FarmData != null)
                {
                    for (int i = 0; i < farms.FarmData.Length; i++)
                    {
                        FarmData farmData = farms.FarmData[i];
                        int x = farmData.PosX;
                        int y = farmData.PosY;
                        int id = farmData.FarmId;
                        Farm farm = StaticStorage.GetLink<FarmPack>("FarmPack").GetFarm(id).Clone();
                        _editorMapRoot._tiles[x, y].Planting(new Farm
                        {
                            ID = farmData.id,
                            Index = id,
                            Icon = farm.Icon,
                            Name = farm.Name,
                            Strength = farmData.Strenght,
                            Progress = farmData.Progress,
                            PositionXY = new Vector2Int(x, y),
                            Stop = farmData.Stop,
                            Level = farmData.FarmLvl,
                            MaxStrength = farm.MaxStrength,
                            Electricity = farm.Electricity,
                            PriceRepair = farmData.PriceRepair,
                            CriticalTemperature = farm.CriticalTemperature,
                            Price = farm.Price,
                            TimeEndRepair = farmData.TimeEndRepair,
                            Crash = farmData.Crash
                        }, farmData.CountSlotCard, farmData.CountSlotCooler, farmData.CountSlotHard, farmData.Components);
                    }
                }
                LoadInventory();
            });
        }
    }

    [Serializable]
    public class DataProfile : Packet
    {
        public int id;
        public string Name;
        public float Coins;
        public float Electricity;
        public int RoomX;
        public int RoomY;
        public float Temperature;
        public int Level;
        public int Exp;
        public float CoinsIncome;
        public bool Computer;
        public int Extinguisher;
        public int Instruments;
        public int EmergencyCooling;
        public int EmergencyCoolingStart;
        public int TimeStartInternet;
        public int DataCenterID;
        public int CountCoinsDay;
        public int TaskDoneDay;
    }
}