﻿using System;
using UnityEngine;

namespace Game
{
    [Serializable]
    public class SlotCard : IUpdatable
    {
        [SerializeField]
        VideoCard _videoCard = new VideoCard();

        public SlotFarm _slotFarm;

        public int _numberSlot;

        IncomeSum _incomeSum;

        public float _price;

        public float _heatDissipation;

        float _maxIncome;

        public float _temperature;

        public VideoCard VideoCard
        {
            set
            {
                if (value != null)
                {
                    _videoCard = value;
                    ChangeCard();
                }
            }
            get
            {
                return _videoCard;
            }
        }

        void ChangeCard()
        {
            _price = _videoCard.Price;

            _incomeSum = StaticStorage.GetLink<IncomeSum>("IncomeSum");
            _maxIncome = StaticStorage.GetLink<VideoCardPack>("VideoCardPack").GetVideoCard(_videoCard.Index).Income;
            _temperature = StaticStorage.GetLink<VideoCardPack>("VideoCardPack").GetVideoCard(_videoCard.Index).Temperature;

            GetHeanDissipation();
            CalculateIncome();
            _slotFarm.Converse();

            _incomeSum.Add(_videoCard);
            StaticStorage.UpdateCore.Add(this);
        }

        public void Remove()
        {
            StaticStorage.UpdateCore.Remove(this);
            StaticStorage.GetLink<Inventory>("Inventory").Add(_videoCard);

            _incomeSum.Remove(_videoCard);
            _videoCard = new VideoCard();
            _slotFarm.Converse();
        }

        void HoureTurn()
        {
            _videoCard.Progress -= 3600;

            CalculateTemperature();
            _videoCard.Strength = Mathf.Clamp((int)Mathf.Round(_videoCard.Strength - (1 + (_videoCard.Temperature - _temperature))), 0, int.MaxValue);
            CalculateIncome();
            CheckCrash();
            _slotFarm.Converse();
        }

        void CalculateIncome()
        {
            float percent = ((float)_videoCard.Strength / _videoCard.MaxStrength) * 100;
            float efficiency = 0;
            if (percent <= 50)
            {
                efficiency = GetEfficiency(percent);
            }
            else
            {
                efficiency = 100;
            }

            _videoCard.Income = (_maxIncome * (float)Math.Round(efficiency, 2) / 100);
        }

        //TODO: Делать запрос на сервер - не случился ли краш
        void CheckCrash()
        {
            float percent = (float)_videoCard.Strength / _videoCard.MaxStrength * 100;
            float value = percent <= 50 ? GetEfficiency(percent) : 100;
            if (value < 50)
            {
                /*
                int rnd = UnityEngine.Random.Range(0, 101);
                if (rnd > 80 - (50 - Mathf.Round(_efficiency)))
                {
                    _slotFarm.CRASH = true;
                }*/
            }
        }

        public float GetHeanDissipation()
        {
            _heatDissipation = _videoCard.HeatDissipation;
            if (_slotFarm.FARM.SlotsCoolers[_numberSlot].Cooler.Index != 0)
            {
                _heatDissipation = _videoCard.HeatDissipation + _slotFarm.FARM.SlotsCoolers[_numberSlot].Cooler.Efficiency;
            }
            return _heatDissipation;
        }

        void CalculateTemperature()
        {
            _videoCard.Temperature = Mathf.Clamp(_videoCard.Temperature + GetHeanDissipation(), _temperature, _videoCard.TemperatureMax);
        }

        void IUpdatable.OnUpdate(float deltaTime)
        {
            if (_slotFarm.STOP == false && _videoCard.Strength != 0)
            {
                _videoCard.Progress += deltaTime;
                if (_videoCard.Progress > 3600)
                {
                    HoureTurn();
                }

                for (int i = 0; i < _slotFarm.FARM.SlotsHards.Count; i++)
                {
                    _slotFarm.AddCoin();
                    if (_slotFarm.FARM.SlotsHards[i].COIN < _slotFarm.FARM.SlotsHards[i].HARD.Volume)
                    {
                        _slotFarm.FARM.SlotsHards[i].COIN = _videoCard.Income / 3600 * deltaTime;
                        break;
                    }
                }
            }
        }

        float GetEfficiency(float percent) => 0.000027f * Mathf.Pow(percent, 4) - 0.003194f * Mathf.Pow(percent, 3) + 0.11805555f * percent * percent - 0.3889f * percent + 50;
    }
}