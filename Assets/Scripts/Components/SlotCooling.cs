﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Game
{
    public class SlotCooling : MonoBehaviour, IPointerClickHandler, IUpdatable
    {
        [SerializeField]
        SpriteRenderer _spriteRenderer;

        TimerPanel _timerPanel;

        CoolingPlant _coolingPlant;

        Cooling _cooling;

        Electricity _electricity;

        Temperature _temperature;

        InfoPanel _infoPanel;

        Coin _coin;

        bool _isStoped;

        public float _consumption;

        public int ID;

        Action _action;

        public Cooling Cooling
        {
            set
            {
                _cooling = value.Clone();
                ChangeCooling();
            }
            get
            {
                return _cooling;
            }
        }

        public bool IsStoped
        {
            set
            {
                _isStoped = value;
                if (value)
                {
                    StaticStorage.UpdateCore.Remove(this);
                    _electricity.CoolingRemove(this);
                    _spriteRenderer.color = new Color(0.5f, 0.5f, 1);
                    _temperature.Remove(this);
                }
                else
                {
                    _electricity.CoolingAdd(this);
                    _spriteRenderer.color = new Color(1, 1, 1);
                    _temperature.Add(this);
                    StaticStorage.UpdateCore.Add(this);
                }
            }
            get
            {
                return _isStoped;
            }
        }

        private void Start()
        {
            _electricity = StaticStorage.GetLink<Electricity>("Electricity");
            _temperature = StaticStorage.GetLink<Temperature>("Temperature");
            _infoPanel = StaticStorage.GetLink<InfoPanel>("InfoPanel");
            _coin = StaticStorage.GetLink<Coin>("Coin");
        }

        void ChangeCooling()
        {
            _consumption = _cooling.Electricity;
            _spriteRenderer.sprite = _cooling.Icon;
            _coolingPlant.Free(false);
            if (_cooling.TimeEndRepair > (int)DateTimeOffset.UtcNow.ToUnixTimeSeconds())
            {
                Repair();
            }
        }

        public void ChangePlant(CoolingPlant coolingPlant)
        {
            _coolingPlant = coolingPlant;
        }

        public void InInventory()
        {
            StaticStorage.GetLink<Inventory>("Inventory").Add(_cooling);
            Clear();
        }

        public void Clear()
        {
            _cooling = null;
            _coolingPlant.Free(true);
            _electricity.CoolingRemove(this);
            _spriteRenderer.sprite = null;
        }

        public void Repair()
        {
            if (_timerPanel == null)
            {
                _timerPanel = StaticStorage.GetLink<FarmIcons>("FarmIcons").StartTimer(transform);
            }
            _action = () => { IsStoped = true; gameObject.SetLayer(0, true); };
            if (_timerPanel.TimerStart(_cooling.TimeEndRepair, _action))
            {
                gameObject.SetLayer(2, true);
                _spriteRenderer.color = new Color(0.5f, 0.5f, 0.5f);
            }
        }

        void IUpdatable.OnUpdate(float deltaTime)
        {
            if (_cooling.Strength > 0 && _electricity.ELECTRICITY >= _cooling.Electricity)
            {
                _spriteRenderer.color = new Color(1, 1, 1);
                _cooling.PriceRepair = _cooling.Price * (_cooling.Strength / _cooling.MaxStrength);
                _cooling.Progress += deltaTime;
                if (_cooling.Progress > 3600)
                {
                    HourTurn();
                }
            }
            else
            {
                _spriteRenderer.color = new Color(0.5f, 0.5f, 1);
            }
        }

        void HourTurn()
        {
            _cooling.Strength--;
            _electricity.ELECTRICITY -= _cooling.Electricity;
            _cooling.Progress -= 3600;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (Input.GetMouseButtonUp(0))
            {
                _infoPanel.Show(this);
            }
        }

        public void Upgrate()
        {
            int id = _cooling.Index;
            Cooling newCooling = StaticStorage.GetLink<CoolingPack>("CoolingPack").GetCooling(id + 1);
            if (_coin.COIN >= newCooling.Price)
            {
                Clear();
                _coin.COIN -= newCooling.Price;
                Cooling = newCooling;
            }
        }
    }
}