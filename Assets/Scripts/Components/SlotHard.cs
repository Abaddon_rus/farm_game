﻿using System.Collections;
using UnityEngine;
using System;

namespace Game
{
    [Serializable]
    public class SlotHard : IUpdatable
    {

        [SerializeField]
        Hard _hard = new Hard();

        public SlotFarm _slotFarm;

        public int _numberSlot;

        public Coroutine _coroutine;

        public float COIN
        {
            set
            {
                if (_slotFarm.CRASH == false)
                {
                    _hard.Coins = Mathf.Clamp(_hard.Coins + value, 0, _hard.Volume);
                }
            }
            get
            {
                return _hard.Coins;
            }
        }

        public Hard HARD
        {
            set
            {
                if (_hard != null && _hard != value)
                {
                    _hard = value;
                    StaticStorage.UpdateCore.Add(this);
                }
            }
            get
            {
                return _hard;
            }
        }

        public void Full()
        {
            if (_hard.Volume != 0 && _hard.Coins == _hard.Volume)
            {
                _slotFarm._getCoinsIcon.SetActive(true);
                _slotFarm._mineCoinsIcon.gameObject.SetActive(false);
            }
        }

        public void Remove()
        {
            StaticStorage.UpdateCore.Remove(this);
            StaticStorage.GetLink<Inventory>("Inventory").Add(_hard);
            _hard = new Hard();
        }

        void IUpdatable.OnUpdate(float deltaTime)
        {
            if (_slotFarm.STOP == false)
            {
                _hard.Volume = _hard.Strength == 0 ? 0 : _hard.Volume;
                _hard.Progress += deltaTime;
                if (_hard.Progress > 3600)
                {
                    HourTurn();
                }
            }
        }

        void HourTurn()
        {
            _hard.Progress -= 3600;
            //_hard.PriceRepair = Mathf.Clamp((float)System.Math.Round(_hard.Price - _hard.Price * ((float)_hard.Strength / (float)_hard.MaxStrength), 2), 0.1f, _hard.Price);
            _hard.Strength = Mathf.Clamp((int)Mathf.Round(_hard.Strength - (1 + (_slotFarm._temperature._temperature - 25))), 0, int.MaxValue);
        }
    }
}
