﻿using System;
using UnityEngine;

namespace Game
{
    [Serializable]
    public class SlotCooler : IUpdatable
    {
        [SerializeField]
        Cooler _cooler = new Cooler();

        public SlotFarm _slotFarm;

        public int _numberSlot;

        public Cooler Cooler
        {
            set
            {
                if (_cooler != value)
                    _cooler = value;
                ChangeCoolers();
            }
            get
            {
                return _cooler;
            }
        }

        public void ChangeCoolers()
        {
            StaticStorage.UpdateCore.Add(this);
            _slotFarm.FARM.SlotsCards.Find(x => x._numberSlot == _numberSlot).GetHeanDissipation();
        }

        public void Remove()
        {
            StaticStorage.UpdateCore.Remove(this);
            StaticStorage.GetLink<Inventory>("Inventory").Add(_cooler);
            _cooler = new Cooler();
        }

        void IUpdatable.OnUpdate(float deltaTime)
        {
            if (_slotFarm.STOP == false)
            {
                _cooler.Efficiency = _cooler.Strength == 0 ? 0 : _cooler.Efficiency;
                _cooler.Progress += deltaTime;
                if (_cooler.Progress > 3600)
                {
                    HourTurn();
                }
            }
        }

        void HourTurn()
        {
            _cooler.Progress -= 3600;
            //_cooler.PriceRepair = Mathf.Clamp((float)Math.Round(_cooler.Price - _cooler.Price * ((float)_cooler.Strength / (float)_cooler.MaxStrength), 2), 0.1f, _cooler.Price);
            _cooler.Strength = Mathf.Clamp((int)Mathf.Round(_cooler.Strength - (1 + (_slotFarm._temperature._temperature - 25))), 0, int.MaxValue);
        }
    }
}
