﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteFarms : MonoBehaviour
{
    [SerializeField]
    GameObject _prefabSlot;

    [SerializeField]
    public Transform _top;

    [SerializeField]
    Transform _crashIcon;
    [SerializeField]
    Transform _getCoinIcon;

    List<SpriteRenderer> _sprites = new List<SpriteRenderer>();


    private void Awake()
    {
        _sprites.AddRange(GetComponentsInChildren<SpriteRenderer>());
    }

    public void AddSlot(int count, int slots)
    {
        for (int i = 0; i < count; i++)
        {
            GameObject pref = Instantiate(_prefabSlot, transform, false);
            pref.transform.localPosition = new Vector3(0, 0.243f + 0.44f * i + (slots * 0.44f), 0);
            _sprites.AddRange(pref.GetComponentsInChildren<SpriteRenderer>());
            _top.transform.localPosition += new Vector3(0, 0.44f, 0);
            _crashIcon.transform.localPosition += new Vector3(0, 0.44f, 0);
            _getCoinIcon.transform.localPosition += new Vector3(0, 0.44f, 0);
        }
    }

    public void StopFarm()
    {
        for (int i = 0; i < _sprites.Count; i++)
        {
            _sprites[i].color = new Color(0.5f, 0.5f, 1);
        }
    }

    public void StartFarm()
    {
        for (int i = 0; i < _sprites.Count; i++)
        {
            _sprites[i].color = new Color(1, 1, 1);
        }
    }

    public void CrashFarm()
    {
        for (int i = 0; i < _sprites.Count; i++)
        {
            _sprites[i].color = new Color(0.5f, 0, 0);
        }
    }

    public void RepairFarm()
    {
        for (int i = 0; i < _sprites.Count; i++)
        {
            _sprites[i].color = new Color(0.5f, 0.5f, 0.5f, 0.7f);
        }
    }
}
