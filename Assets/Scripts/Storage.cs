﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public static class PlantingComponents
    {
        public static VideoCard VideoCard = null;

        public static Hard Hard = null;

        public static Cooler Cooler = null;

        public static Farm Farm = null;

        public static Cooling Cooling = null;

        public static GameObject DragItem = null;

        public static int IdInInventory;

        public static float VolumeSound = 1;

        public static void Temp(Packs packs)
        {
            if (packs is VideoCard)
            {
                VideoCard = packs as VideoCard;
            }
            else if (packs is Cooler)
            {
                Cooler = packs as Cooler;
            }
            else if (packs is Hard)
            {
                Hard = packs as Hard;
            }
            else if (packs is Farm)
            {
                Farm = packs as Farm;
                StaticStorage.GetLink<Menu>("Menu").ClosePanel();
            }
            else if (packs is Cooling)
            {
                Cooling = packs as Cooling;
                StaticStorage.GetLink<Menu>("Menu").ClosePanel();
            }
        }

        public static void Clear()
        {
            VideoCard = null;
            Hard = null;
            Cooler = null;
            Farm = null;
            Cooling = null;
        }

        public static SlotFarm SlotFarm;
    }

    public static class StaticStorage
    {
        public static DataProfile Prifile;

        public static UpdateCore UpdateCore = new UpdateCore();

        static Dictionary<string, object> Links = new Dictionary<string, object>();

        public static void AddLink(string name, object classObject)
        {
            Links.Add(name, classObject);
        }

        public static T GetLink<T>(string name)
        {
            T result = default(T);
            object value;
            if (Links.TryGetValue(name, out value))
            {
                result = (T)value;
            }
            return result;
        }
    }

    public static class Extention
    {
        public static T Clone<T>(this T source) where T : class, new()
        {
            var json = JsonUtility.ToJson(source, true);
            T result = new T();
            JsonUtility.FromJsonOverwrite(json, result);

            return result;
        }
    }

    [Serializable]
    public class DataPacket : Packet
    {
        public float v1;
        public float v2;
        public float v3;
        public string s1;
    }
}