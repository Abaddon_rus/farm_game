﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    [CreateAssetMenu(fileName = "OtherPack", menuName = "Packs/OtherPack")]
    public class OtherPack : ScriptableObject
    {
        public List<Other> Other;

        public Other GetOther(string name) => Other.Find(i => i.Name == name);

        public void AddCountOther(string name, int count) => Other.Find(i => i.Name == name).Count += count;
    }

    [Serializable]
    public class Other
    {
        public string Name;
        public string Description;
        public Sprite Icon;
        public float Price;
        public int Count;
        public OtherQuery Query;
    }

    public enum OtherQuery
    {
        BuyExtinguisher,
        BuyInstruments,
        BuyEmergencyCooling
    }
}