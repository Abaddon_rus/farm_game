﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    [CreateAssetMenu(fileName = "Buff", menuName = "Buff/Buff")]
    public class Buff : ScriptableObject
    {
        public List<BuffWP> BuffWorkplace;

        public BuffWP GetBuff(string ID) => BuffWorkplace.Find(i => i.ID == ID);
    }

    [Serializable]
    public class BuffWP
    {
        public string ID;
        public string Name;
        public string Description;
        public float StartParametr;
        public float Parametr;
        public float Price;
        public int Cooldown;
        public Sprite Icon;
        public List<BuffParameter> Parameters;

        public BuffParameter GetParametrs(string lvl) => Parameters.Find(i => i.Lvl == lvl);
    }

    [Serializable]
    public class BuffParameter
    {
        public string Lvl;
        public BuffWPEnum Addiction;
        public int AddictionLvl;
        public int PlayerLvl;
    }

    public enum BuffWPEnum
    {
        Ничего = 0,
        TABLE = 1,
        CHAIR = 2,
        MONITOR = 3,
        KEYBOARD = 4,
        LuckyTask = 5,
        FlashRelax = 6,
        FastTask = 7,
        ExpensiveTask = 8,
        FlashTask = 9,
        FastRelax = 10,
        DoubleTask = 11,
        BestTask = 12
    }
}