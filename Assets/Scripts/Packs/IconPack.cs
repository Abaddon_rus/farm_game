﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    [CreateAssetMenu(fileName = "IconPack", menuName = "Packs/IconPack")]
    public class IconPack : ScriptableObject
    {
        public List<Icon> Icon;
    }

    [Serializable]
    public class Icon
    {
        public string Name;
        public Sprite Sprite;
    }
}