﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    [CreateAssetMenu(fileName = "CoolerPack", menuName = "Packs/CoolerPack")]
    public class CoolerPack : ScriptableObject
    {
        public List<Cooler> Cooler;

        public Cooler GetCooler(int Index) => Cooler.Find(i => i.Index == Index);
    }

    [Serializable]
    public class Cooler : Packs
    {
        public int MinFarmLvl = 0;
        public float Efficiency;
        public int Size;
    }
}
