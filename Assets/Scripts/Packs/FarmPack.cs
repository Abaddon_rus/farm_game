﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    [CreateAssetMenu(fileName = "FarmPack", menuName = "Packs/FarmPack")]
    public class FarmPack : ScriptableObject
    {
        public List<Farm> Farms;

        public Farm GetFarm(int Index) => Farms.Find(i => i.Index == Index);
    }

    [Serializable]
    public class Farm : Packs
    {
        public float HeatDissipation = 0;
        public float CriticalTemperature = 0;
        public Vector2Int PositionXY = new Vector2Int();
        public List<SlotCard> SlotsCards = new List<SlotCard>();
        public List<SlotCooler> SlotsCoolers = new List<SlotCooler>();
        public List<SlotHard> SlotsHards = new List<SlotHard>();
        public bool Crash = false;
        public bool Stop = true;
    }
}
