﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    [CreateAssetMenu(fileName = "CoolingPack", menuName = "Packs/CoolingPack")]
    public class CoolingPack : ScriptableObject
    {
        public List<Cooling> Cooling;

        public Cooling GetCooling(int Index) => Cooling.Find(i => i.Index == Index);
    }

    [Serializable]
    public class Cooling : Packs
    {
        public int MinFarmLvl = 0;
        public int NumberSlot;
        public float CoolingTemperature;
        public bool Stop = true;
    }
}