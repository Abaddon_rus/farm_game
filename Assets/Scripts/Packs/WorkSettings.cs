﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    [CreateAssetMenu(fileName = "WorkSettings", menuName = "Packs/WorkSettings")]
    public class WorkSettings : ScriptableObject
    {
        [Header("Имя задания")]
        public string[] Verb = new string[1];
        public string[] Noun = new string[1];
        public string[] Title = new string[1];

        [Header("Типы заказов")]
        public List<TypeTask> TypeTasks;

        [Header("Лицензии")]
        public List<LicenseType> LicenseType;

        public TypeTask GetTypeTask(string name) => TypeTasks.Find(i => i.Name == name);

        public string Name()
        {
            string name;

            name = Verb[UnityEngine.Random.Range(0, Verb.Length)] + " " + Noun[UnityEngine.Random.Range(0, Noun.Length)] + " " + Title[UnityEngine.Random.Range(0, Title.Length)];

            return name;
        }
    }

    [Serializable]
    public class TypeTask
    {
        public string Name;
        public int Count;
        public int CountRare;
        public float Chance;
        public float TimeRelax;
        public Vector2 Time;
        public Vector2 Price;
        public Sprite Ramka;

        public int GetTime()
        {
            return (int)UnityEngine.Random.Range(Time.x, Time.y);
        }
        public float GetPrice(int time)
        {
            return time * UnityEngine.Random.Range(Price.x, Price.y);
        }
    }

    [Serializable]
    public class LicenseType
    {
        public int Lvl;
        public float Price;
        public float PriceTask;
        public int TimeTask;
    }
}
