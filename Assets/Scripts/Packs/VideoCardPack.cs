﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    [CreateAssetMenu(fileName = "VideoCardPack", menuName = "Packs/VideoCardPack")]
    public class VideoCardPack : ScriptableObject
    {
        public List<VideoCard> Cards;

        public VideoCard GetVideoCard(int Index) => Cards.Find(i => i.Index == Index);
    }

    [Serializable]
    public class VideoCard : Packs
    {
        public int MinFarmLvl = 0;
        public float Income;
        public float Coeficient;
        public float Temperature;
        public float TemperatureMax;
        public float HeatDissipation;
    }
}