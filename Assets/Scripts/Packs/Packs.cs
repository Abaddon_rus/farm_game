﻿using UnityEngine;
using System;

namespace Game
{
    [Serializable]
    public class Packs
    {
        public bool Invisible = false;
        public int ID;
        public int Index;
        public string Name;
        public Sprite Icon;
        public float Price;
        public float PriceRepair;
        public int Strength;
        public int MaxStrength;
        public float Electricity;
        public float Progress;
        public int Level = 1;
        public int TimeEndRepair = 0;
        public int CountRepairs;
    }
}
