﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    [CreateAssetMenu(fileName = "HardPack", menuName = "Packs/HardPack")]
    public class HardPack : ScriptableObject
    {
        public List<Hard> Hard;

        public Hard GetHard(int Index) => Hard.Find(i => i.Index == Index);
    }

    [Serializable]
    public class Hard : Packs
    {
        public int MinFarmLvl = 0;
        public float Volume;
        public float Coins;
    }
}