﻿using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CustomButton : Button
{

    public override void OnPointerExit(PointerEventData eventData)
    {
        base.OnPointerExit(eventData);
        DoStateTransition(SelectionState.Normal, false);
    }
}
