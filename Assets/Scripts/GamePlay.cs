﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class GamePlay : MonoBehaviour, IUpdatable
    {
        public Canvas _canvas;

        [SerializeField]
        VideoCardPack _videoCardPack;
        [SerializeField]
        CoolerPack _coolerPack;
        [SerializeField]
        HardPack _hardPack;
        [SerializeField]
        FarmPack _farmPack;
        [SerializeField]
        CoolingPack _coolingPack;
        [SerializeField]
        IconPack _iconPack;
        [SerializeField]
        BuyFarm _buyFarm;
        [SerializeField]
        BuyCooling _buyCooling;
        [SerializeField]
        Scrollbar _scrollbar;

        public float _maxDistance = 4;
        private Vector3 _positionClick;
        public float _minBorder = -1;
        public float _maxBorder = 1;

        Menu _menu;
        InfoPanel _infoPanel;
        RepairPanel _repairPanel;
        Internet _internet;
        Electricity _electricity;

        private void Awake()
        {
            StaticStorage.AddLink("BuyCooling", _buyCooling);
            StaticStorage.AddLink("BuyFarm", _buyFarm);
            StaticStorage.AddLink("GamePlay", this);
            StaticStorage.AddLink("VideoCardPack", _videoCardPack);
            StaticStorage.AddLink("CoolerPack", _coolerPack);
            StaticStorage.AddLink("HardPack", _hardPack);
            StaticStorage.AddLink("FarmPack", _farmPack);
            StaticStorage.AddLink("CoolingPack", _coolingPack);
            StaticStorage.AddLink("IconPack", _iconPack);
            _scrollbar.onValueChanged.AddListener(ChangeSize);
            _scrollbar.value = 1;
        }

        private void Start()
        {
            _menu = StaticStorage.GetLink<Menu>("Menu");
            _infoPanel = StaticStorage.GetLink<InfoPanel>("InfoPanel");
            _repairPanel = StaticStorage.GetLink<RepairPanel>("RepairPanel");
            _internet = StaticStorage.GetLink<Internet>("Internet");
            _electricity = StaticStorage.GetLink<Electricity>("Electricity");
            StaticStorage.UpdateCore.Add(this);
        }

        private void ChangeSize(float size)
        {
            Camera.main.orthographicSize = Mathf.Clamp(size * (_maxDistance - 4), 0, _maxDistance) + 4;
        }

        public void ClosePanels()
        {
            _menu.ClosePanel();
            _infoPanel.ClosePanel();
            _repairPanel.ClosePanel();
            _internet.ClosePanel();
            _electricity.ClosePanel();
            _buyFarm.gameObject.SetActive(false);
            _buyCooling.gameObject.SetActive(false);
        }

        void IUpdatable.OnUpdate(float deltaTime)
        {
            if (Input.GetAxis("Mouse ScrollWheel") != 0)
            {
                _scrollbar.value = Mathf.Clamp(_scrollbar.value - (Input.GetAxis("Mouse ScrollWheel")) / 5, 0, 1);
            }
            if (Input.GetMouseButtonDown(2))
            {
                _positionClick = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            }
            else if(Input.GetMouseButton(2))
            {
                Vector3 vec = _positionClick - Camera.main.ScreenToWorldPoint(Input.mousePosition);
                Camera.main.transform.localPosition = new Vector3(Mathf.Clamp((Camera.main.transform.localPosition.x + vec.x), _minBorder * (2f + (1 - _scrollbar.value)), _maxBorder * (2f + (1 - _scrollbar.value))), Camera.main.transform.localPosition.y);
                Camera.main.transform.localPosition = new Vector3(Camera.main.transform.localPosition.x, Mathf.Clamp((Camera.main.transform.localPosition.y + vec.y), _minBorder * (2 + (1 - _scrollbar.value)) * 1.3f, 0));
                _positionClick = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            }
        }
    }
}
