﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class CoinsIncome : MonoBehaviour
    {

        [SerializeField]
        TextMeshProUGUI _textMeshProUGUI;

        [SerializeField]
        Button _button;

        float _coinsIncome;

        public float Income
        {
            set
            {
                _coinsIncome += value;
                _textMeshProUGUI.text = _coinsIncome.ToString();
            }
        }

        private void Awake()
        {
            StaticStorage.AddLink("CoinsIncome", this);
        }

        private void Start()
        {
            _button.onClick.AddListener(Zero);
        }

        private void Zero()
        {
            NetworkCore.Send("NullIncome", new DataPacket
            {
                v1 = 0
            }, (data) =>
            {
                NullIncom(Convert.ToBoolean(data));
            });
        }

        private void NullIncom(bool result)
        {
            if (result)
            {
                _coinsIncome = 0;
                _textMeshProUGUI.text = _coinsIncome.ToString();
            }
        }
    }
}