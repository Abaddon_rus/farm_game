﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Game
{
    public class ClickMiss : MonoBehaviour, IPointerClickHandler
    {
        GamePlay _gamePlay;

        private void Start()
        {
            _gamePlay = StaticStorage.GetLink<GamePlay>("GamePlay");
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            _gamePlay.ClosePanels();
            if (PlantingComponents.DragItem != null)
            {
                Destroy(PlantingComponents.DragItem);
                PlantingComponents.Clear();
            }
        }        
    }
}
